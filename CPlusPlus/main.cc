#include <iostream>
#include <cmath>

using namespace std;

/* ---------------------------------------------------------------------- */

class Private
{
private:
    int x, y, z;
public:
    Private ( ) : x (1), y (2), z (3) { }
};

class Spy
{
public:
    int x, y,  z;
};

void privateFields ()
{
    Private * p = new Private;
    Spy * s = (Spy *) p;
    cout << "x = " << s->x << endl;
    cout << "y = " << s->y << endl;
    cout << "z = " << s->z << endl;
}

/* ---------------------------------------------------------------------- */

class Base
{
protected:
    string name;
public:
    Base (string name0) : name (name0) { }
    virtual void info () { cout << "Base (name=" << name << ")" << endl; }
};

class Derived : public Base
{
private:
    int number;
public:
    Derived (string name0, int number0) : Base (name0), number (number0) { }
    void info () { cout << "Derived (name=" << name << ", number=" << number << ")" << endl; }
};

void baseAndDerived ()
{
    Base * a = new Base ("alpha");
    Base * b = new Derived ("beta", 7);
    a->info ();
    b->info ();
}

/* ---------------------------------------------------------------------- */

#if 0
struct BaseStruct
{
    string name;
};

void base_init (BaseStruct * self, string name0)
{
    self->name = name0;
}

BaseStruct * base_create (string name0)
{
    BaseStruct * self = new BaseStruct;
    base_init (self, name0);
    return self;
}

void base_info (BaseStruct * self)
{
    cout << "Base (name=" << self->name << ")" << endl;
}

struct DerivedStruct
{
    BaseStruct base;
    int number;
};

void derived_init (DerivedStruct * self, string name0, int number0)
{
    base_init (&self->base, name0);
    self->number = number0;
}

DerivedStruct * derived_create (string name0, int number0)
{
    DerivedStruct * self = new DerivedStruct;
    derived_init (self, name0, number0);
    return self;
}

void derived_info (DerivedStruct * self)
{
    cout << "Derived (name=" << self->base.name << ", number=" << self->number << ")" << endl;
}

void withoutVirtual ()
{
    BaseStruct * b = base_create ("first");
    DerivedStruct * t = derived_create ("second", 2);
    base_info (b);
    derived_info (t);
}
#endif

/* ---------------------------------------------------------------------- */

struct BaseStruct;
void base_info (BaseStruct * self);

typedef void (*BaseMethod) (BaseStruct *);

struct BaseStruct
{
    string name;
    BaseMethod info_method;
};

void base_init (BaseStruct * self, string name0)
{
    self->name = name0;
    self->info_method = base_info;
}

BaseStruct * base_create (string name0)
{
    BaseStruct * self = new BaseStruct;
    base_init (self, name0);
    return self;
}

void base_info (BaseStruct * self)
{
    cout << "Base (name=" << self->name << ")" << endl;
}

struct DerivedStruct
{
    BaseStruct base;
    int number;
};

void derived_info (DerivedStruct * self);

void derived_init (DerivedStruct * self, string name0, int number0)
{
    base_init (&self->base, name0);
    self->base.info_method = (BaseMethod) derived_info;
    self->number = number0;
}

DerivedStruct * derived_create (string name0, int number0)
{
    DerivedStruct * self = new DerivedStruct;
    derived_init (self, name0, number0);
    return self;
}

void derived_info (DerivedStruct * self)
{
    cout << "Derived (name=" << self->base.name << ", number=" << self->number << ")" << endl;
}

void withVirtual ()
{
    BaseStruct * b = base_create ("first");
    b->info_method (b);

    DerivedStruct * t = derived_create ("second", 2);
    t->base.info_method (& t->base);

    BaseStruct * v = & t->base;
    v->info_method (v);
}

/* ---------------------------------------------------------------------- */

class Basic
{
protected:
     int x, y;
public :
     virtual void print () { cout << "Basic (" << x << ", " << y << ")" << endl; }
     virtual int  distance () { return sqrt (x*x +y*y); }
     virtual void scale (int c) { x = x * c; y *=c; }
};

class Component : public Basic
{
protected:
     int z;
public :
     virtual void print () { cout << "Component (" << x << ", " << y << "," << z << ")" << endl; }
     virtual int  distance () { return sqrt (x*x +y*y + z*z); }
     virtual void move (int dx, int dy, int dz) { x += dx; y += dy; z += dz; }
};

/* ---------------------------------------------------------------------- */

int main()
{
    privateFields ();
    baseAndDerived ();
    // withoutVirtual ();
    withVirtual ();
    return 0;
}
