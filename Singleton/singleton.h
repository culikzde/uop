#ifndef SINGLETON_H
#define SINGLETON_H

class Singleton
{
private:
    int number;
public:
    int getNumber () { return number; }
    void setNumber (int n) { number = n; }

private:
    Singleton ();

private:
    static Singleton * obj;

public:
    static Singleton & instance ();
};

#endif // SINGLETON_H
