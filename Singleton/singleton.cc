#include <singleton.h>

#include <iostream>
using namespace std;

Singleton * Singleton::obj = NULL;

Singleton::Singleton()
{
    number = 0;
}

Singleton & Singleton::instance ()
{
    if (obj == NULL)
        obj = new Singleton ();
    return *obj;
}

int main ()
{
    Singleton & p = Singleton::instance ();
    p.setNumber (7);
    // Singleton s; // constructor is private
    return 0;
}
