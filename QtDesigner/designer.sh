#!/bin/sh

DESIGNER=designer-qt5
which $DESIGNER || DESIGNER=designer

insert_first () {
   local NAME=$1
   local NEW=$2
   eval VALUE=\"\$$NAME\"

   if test -z "$VALUE" ; then
      export $NAME="$NEW"
   else
      export $NAME="$NEW:$VALUE"
   fi
}

mkdir -p _output/plugins/designer
cp libconnectionplugin.so _output/plugins/designer

insert_first QT_PLUGIN_PATH _output/plugins

$DESIGNER $*
