#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include "factory.h"
#include "panel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void addTool (QString text, Factory * factory);

private:
    Ui::MainWindow *ui;
};
#endif // WINDOW_H
