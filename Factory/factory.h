#ifndef FACTORY_H
#define FACTORY_H

#include <QWidget>
#include <QPushButton>
#include <QTextEdit>
#include <QTreeWidget>

#include <QToolButton>

/* ---------------------------------------------------------------------- */

class Factory
{
public:
    virtual QWidget * createInstance () = 0;
};

class ButtonFactory : public Factory
{
public:
    QWidget * createInstance ()  { return new QPushButton ("Button"); }
};

class EditorFactory : public Factory
{
private:
    QString text;
public:
    QWidget * createInstance ()
    {
        QTextEdit * e = new QTextEdit;
        e->setText (text);
        return e;
    }
    EditorFactory (QString p_text) : text (p_text) { }
};

class TreeFactory : public Factory
{
public:
    QWidget * createInstance ()  { return new QTreeWidget; }
};

/* ---------------------------------------------------------------------- */

extern QHash <QString, Factory *> factories;

const QString tool_format = "application/x-shape";

/* ---------------------------------------------------------------------- */

class ToolButton : public QToolButton
{
private:
    QString name;
protected :
    void mousePressEvent (QMouseEvent *event);
public:
    ToolButton (QWidget * p_parent, QString p_name) :
        QToolButton (p_parent),
        name (p_name)
    {
        setText (p_name);
    }
};

#endif // FACTORY_H
