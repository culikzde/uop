#include "panel.h"
#include "factory.h"
#include <QDragEnterEvent>
#include <QMimeData>

#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

void Panel::dragEnterEvent (QDragEnterEvent *event)
{
    const QMimeData * data = event->mimeData ();
    if (data->hasFormat (tool_format))
    {
        event->accept();
    }
}

void Panel::dropEvent (QDropEvent *event)
{
    const QMimeData * data = event->mimeData ();
    if (data->hasFormat (tool_format))
    {
        QString name = data->data(tool_format);
        cout << "DROP " << name.toStdString () << endl;
        Factory * factory = factories [name];
        QWidget * widget = factory->createInstance ();

        QPoint pos = event->pos();
        widget->setParent (this);
        widget->move (pos);
        widget->setVisible(true);
    }
}

/* ---------------------------------------------------------------------- */

Panel::Panel(QWidget *parent) : QWidget(parent)
{
    setAcceptDrops (true);
}

/* ---------------------------------------------------------------------- */


