#include "factory.h"
#include <QDrag>
#include <QMimeData>

/* ---------------------------------------------------------------------- */

QHash <QString, Factory *> factories;

/* ---------------------------------------------------------------------- */

void ToolButton::mousePressEvent (QMouseEvent *event)
{
    QDrag * drag = new QDrag (this);
    QMimeData * data = new QMimeData ();
    data->setData (tool_format, name.toLatin1());
    drag->setMimeData (data);
    drag->start ();
}

/* ---------------------------------------------------------------------- */

