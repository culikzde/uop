#include "window.h"
#include "ui_factory.h"

#include <QApplication>
#include <QDrag>
#include <QMimeData>

/* ---------------------------------------------------------------------- */

void MainWindow::addTool (QString name, Factory * factory)
{
    factories [name] = factory;
    ToolButton * b = new ToolButton (this, name);
    ui->toolBar->addWidget (b);
}

/* ---------------------------------------------------------------------- */

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    addTool ("Button",    new ButtonFactory);
    addTool ("Edit",      new EditorFactory ("some text"));
    addTool ("Tree View", new TreeFactory);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* ---------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
