#ifndef PANEL_H
#define PANEL_H

#include <QWidget>

class Panel : public QWidget
{
    Q_OBJECT
public:
    explicit Panel (QWidget *parent = nullptr);

signals:
protected:
    void dragEnterEvent (QDragEnterEvent *event);
    void dropEvent (QDropEvent *event);
};

#endif // PANEL_H
