#ifndef QT_INVENTOR_H
#define QT_INVENTOR_H

#include <QMainWindow>
#include <QTreeWidgetItem>

#include <Inventor/nodes/SoSeparator.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow ( );

private:
    void displayBranch (QTreeWidgetItem * target, SoNode *node);
    void displayNode (SoNode *node);
    void displayLine (QString name, QString value);

    void simpleScene (SoSeparator * top);

private slots:
    void on_tree_itemActivated (QTreeWidgetItem *item, int column);
    void on_prop_itemChanged (QTreeWidgetItem *item, int column);

private:
    Ui::MainWindow *ui;
    SoNode * edit_node = nullptr;
};

#endif // QT_INVENTOR_H
