QT += core gui widgets

TARGET = qt-inventor.bin

HEADERS +=  qt-inventor.h
SOURCES += qt-inventor.cc
FORMS +=  qt-inventor.ui

# WSL, Ubuntu
# LIBS += -lCoin -lSoQt

# Hyper-V, Fedora 41
# INCLUDEPATH += -I /usr/include/Coin4
# LIBS += -lCoin -lSoQt

# MSys2
# Menu Edit / Preferences :
# tab  Kits / Qt Versions : ADD C:/Programs/MSys64/mingw64/bin/qmake.exe
# tab  Kits / Kits : ADD "Qt5 from MSys" SET compiler C, C++, Qt Version
DEFINES += COIN_DLL SOQT_DLL
LIBS += -L C:/Programs/MSys64/mingw64/bin -lCoin-80 -lSoQt



# Alma Linux
# INCLUDEPATH += -I /usr/include/Coin4 -I /tmp/appl/usr/include/Coin4
# LIBS += -L /tmp/appl/usr/lib64 -lCoin -lSoQt
# LIBS += -Wl,-rpath -Wl,/tmp/appl/usr/lib64
# nebo na radce LD_LIBRARY_PATH=/tmp/appl/usr/lib64 ./qt-inventor.bin



# Na AlmaLinuxu pridat SoQt a SoQt-devel
# dnf install rpm-build doxygen Coin4-devel
# rpmbuild --rebuild SoQt-1.6.0-15.fc40.src.rpm

# zdrojovy balicek https://archives.fedoraproject.org/pub/fedora/linux/releases/40/Everything/source/tree/Packages/s/SoQt-1.6.0-15.fc40.src.rpm
# vytvorene balicky http://kmlinux.fjfi.cvut.cz/~culikzde/alma-soqt/
# rozbalit SoQt SoQt-devel do /tmp/appl (napr. pomoci mc)



# INCLUDEPATH += `pkg-config SoQt --cflags`
# LIBS += `pkg-config SoQt --libs` -lSoQt

# CONFIG += link_pkgconfig
# PKGCONFIG += SoQt

# dnf install SoQt-devel
# apt install libsoqt520-dev


