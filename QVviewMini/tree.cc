
/* tree.cc */

#include "tree.h"
#include "view.h"

#include <QHeaderView>

#ifdef USE_MINI
   #include "code.h"
   #include "mini-parser.hpp"
#endif

#ifdef USE_CLANG
   #include "clang-common.h"
#endif

#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

Tree::Tree (Window * p_win) :
   QTreeWidget (p_win),
   win (p_win)
{
   header()->hide ();
   connect (this, &Tree::itemActivated, this, &Tree::onActivated);
   connect (this, &Tree::currentItemChanged, this,  &Tree::onCurrentItemChanged);
}

void Tree::onActivated (QTreeWidgetItem * node, int column)
{
   TreeItem * item = dynamic_cast < TreeItem * > (node);
   if (item != NULL)
   {
       QString fileName = QString::fromStdString (recallFileName (item->src_file));
       if (fileName != "")
          win->openEditor (fileName, item->src_line, item->src_col);
   }
}

void Tree::onCurrentItemChanged (QTreeWidgetItem * current, QTreeWidgetItem * previous)
{
   // cout << "currentItemChanged" << current->text (0).toStdString () << endl;
   PropertyTable * prop = win->getProp ();
   prop->clearTable ();

   TreeItem * item = dynamic_cast < TreeItem * > (current);
   if (item != NULL)
   {
      prop->addString ("file name", QString::fromStdString ((recallFileName (item->src_file))));
      prop->addInt ("src_file", item->src_file);
      prop->addInt ("src_line", item->src_line);
      prop->addInt ("src_col", item->src_col);

      if (item->qt_obj != NULL) prop->addString ("(qt_obj)", typeid (item->qt_obj).name ());
      if (item->gra_obj != NULL) prop->addString ("(gra_obj)", typeid (item->gra_obj).name ());
      #ifdef USE_MINI
      if (item->sim_obj != NULL) prop->addString ("(sim_obj)", typeid (item->sim_obj).name ());
      if (item->cmm_obj != NULL) prop->addString ("(cmm_obj)", typeid (item->cmm_obj).name ());
      #endif
      #ifdef USE_CLANG
      if (item->clang_obj != NULL) prop->addString ("(clang_obj)", typeid (item->clang_obj).name ());
      #endif

      win->displayProperties (item);
   }
}

/* ---------------------------------------------------------------------- */

TreeItem::TreeItem (QTreeWidget * parent, QString text) :
   QTreeWidgetItem (parent),
   src_file (0),
   src_line (0),
   src_col (0),
   qt_obj (NULL),
   gra_obj (NULL)
   #ifdef USE_MINI
     , sim_obj (NULL)
     , cmm_obj (NULL)
   #endif
   #ifdef USE_CLANG
      , clang_obj (NULL);
   #endif
{
   if (text != "")
      setText (0, text);
}

TreeItem::TreeItem (QTreeWidgetItem * parent, QString text) :
   QTreeWidgetItem (parent),
   src_file (0),
   src_line (0),
   src_col (0),
   qt_obj (NULL),
   gra_obj (NULL)
   #ifdef USE_MINI
     , sim_obj (NULL)
     , cmm_obj (NULL)
   #endif
   #ifdef USE_CLANG
      , clang_obj (NULL);
   #endif
{
   if (text != "")
      setText (0, text);
}

/* ---------------------------------------------------------------------- */
