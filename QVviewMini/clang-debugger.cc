
/* clang-debugger.cc */

#include "clang-debugger.h"

#include "lldb/API/LLDB.h"

#include <QTreeWidget>
#include <QHeaderView>

#include "view.h"

#include <unistd.h> // getpid

#include <iostream>
// #include <iomanip>
using namespace std;

/* ---------------------------------------------------------------------- */

template <class T>
QTreeWidgetItem * display (QTreeWidget * tree, T & v)
{
   QTreeWidgetItem * node = new QTreeWidgetItem (tree);
   lldb::SBStream stream = lldb::SBStream ();
   v.GetDescription (stream);
   QString txt = stream.GetData();
   txt = txt.trimmed ();
   node->setText (0, txt);
   return node;
}

template <class T>
QTreeWidgetItem * display (QTreeWidgetItem * branch, T & v)
{
   QTreeWidgetItem * node = new QTreeWidgetItem (branch);
   lldb::SBStream stream = lldb::SBStream ();
   v.GetDescription (stream);
   QString txt = stream.GetData();
   txt = txt.trimmed ();
   node->setText (0, txt);
   return node;
}

/* ---------------------------------------------------------------------- */

class DebugRegisters : public QTreeWidget
{
public:
   DebugRegisters (Window * p_win) : QTreeWidget (p_win)
   {
       header ()->hide ();
   }

   void displayRegisterGroup (QTreeWidgetItem * branch, lldb::SBValue & group)
   {
      int cnt = group.GetNumChildren ();
      for (int inx = 0; inx < cnt ; inx++)
      {
          lldb::SBValue value = group.GetChildAtIndex (inx);
          QString txt = QString (value.GetName ()) +  " = " + value.GetValue ();

          QTreeWidgetItem * node = new QTreeWidgetItem (branch);
          node->setText (0, txt);
      }
   }

   void displayRegisters (lldb::SBFrame & frame)
   {
      lldb::SBValueList group_list =  frame.GetRegisters ();
      int group_cnt = group_list.GetSize ();
      for (int group_inx = 0; group_inx < group_cnt ; group_inx++)
      {
          lldb::SBValue group = group_list.GetValueAtIndex (group_inx);
          QTreeWidgetItem * branch = new QTreeWidgetItem (this);
          branch->setText (0, group.GetName ());
          displayRegisterGroup (branch, group);
      }
   }
};

/* ---------------------------------------------------------------------- */

class DebugStack : public QTreeWidget
{
public:
   DebugStack (Window * p_win) : QTreeWidget (p_win)
   {
      header ()->hide ();
   }

   void displayVariables (QTreeWidgetItem * branch, lldb::SBFrame & frame)
   {
         lldb::SBValueList list =  frame.GetVariables (true, true, true, true);
         int cnt = list.GetSize ();
         for (int inx = 0; inx < cnt ; inx++)
         {
             lldb::SBValue value = list.GetValueAtIndex (inx);
             QString txt = QString (value.GetName ()) + " = " + value.GetValue ();

             QTreeWidgetItem * node = new QTreeWidgetItem (branch);
             node->setText (0, txt);
         }
   }

   void displayStack (lldb::SBThread & thread)
   {
      int cnt = thread.GetNumFrames();
      // if (cnt > 10) cnt = 10;
      for (int inx = cnt-1; inx >= 0 ; inx--)
      {
         lldb::SBFrame frame = thread.GetFrameAtIndex (inx);
         QTreeWidgetItem * branch = display (this, frame);
         displayVariables (branch, frame);
      }
   }
};

/* ---------------------------------------------------------------------- */

class DebugThreads : public QTreeWidget
{
public:
   DebugThreads (Window * p_win) : QTreeWidget (p_win)
   {
       header ()->hide ();
   }


   void displayThreads (lldb::SBProcess & process)
   {
       QTreeWidgetItem * branch = display (this, process);
       int cnt = process.GetNumThreads();
       for (int inx = 0; inx < cnt ; inx++)
       {
          lldb::SBThread thread = process.GetThreadAtIndex (0);
          display (branch, thread);
       }
       branch->setExpanded (true);
   }
};

/* ---------------------------------------------------------------------- */

class DebugView : QTreeWidget
{
public:
   DebugView (Window * p_win);
   void displayState (lldb::SBTarget & target, lldb::SBProcess & process);

private:
   Window * win;
   DebugThreads * thread_view;
   DebugRegisters * register_view;
   DebugStack * stack_view;

   void displayInstuctions (lldb::SBInstructionList list);
};

DebugView::DebugView (Window * p_win) :
   QTreeWidget (p_win),
   win (p_win)
{
    header ()->hide ();

    register_view = new DebugRegisters (win);
    stack_view = new DebugStack (win);
    thread_view = new DebugThreads (win);

    win->left_tools->addTab (thread_view, "Threads");

    win->left_tools->addTab (register_view, "Registers");
    win->left_tools->setCurrentWidget (register_view);

    win->right_tools->addTab (stack_view, "Stack");
    win->right_tools->setCurrentWidget (stack_view);

    win->tabs->addTab (this, "Instructions");
    win->tabs->setCurrentWidget (this);
}

void DebugView::displayInstuctions (lldb::SBInstructionList list)
{
   int cnt = list.GetSize ();
   // if (cnt > 4) cnt = 4;
   for (int inx = 0; inx < cnt ; inx++)
   {
       lldb::SBInstruction instr = list.GetInstructionAtIndex (inx);
       display (this, instr);
   }
}

void DebugView::displayState (lldb::SBTarget & target, lldb::SBProcess & process)
{
    clear ();
    thread_view->clear ();
    stack_view->clear ();
    register_view->clear ();

    thread_view->displayThreads (process);
    lldb::SBThread thread = process.GetThreadAtIndex (0);
    if (thread.IsValid ())
    {
        stack_view->displayStack (thread);

        lldb::SBFrame frame = thread.GetFrameAtIndex (0);
        register_view->displayRegisters (frame);

        lldb::SBFunction func = frame.GetFunction();
        if (func.IsValid ())
        {
            displayInstuctions (func.GetInstructions (target));
        }
        else
        {
            lldb::SBSymbol symbol = frame.GetSymbol ();
            if (symbol.IsValid ())
            {
               displayInstuctions (symbol.GetInstructions (target));
            }
        }
    }
}

//* ---------------------------------------------------------------------- */

template <class T>
void print (T & v)
{
   lldb::SBStream stream = lldb::SBStream ();
   v.GetDescription (stream);
   std::cout << stream.GetData();
}

template <class T>
void print (T & v, lldb::DescriptionLevel level)
{
   lldb::SBStream stream = lldb::SBStream ();
   v.GetDescription (stream, level);
   std::cout << stream.GetData();
}

/* ---------------------------------------------------------------------- */

void debug_with_lldb (Window * win, string fileName)
{
   DebugView * debug_view = new DebugView (win);

   cout << "point 1" << endl;
   lldb::SBDebugger::Initialize(); // important
   cout << "point 2" << endl;
   lldb::SBDebugger debugger (lldb::SBDebugger::Create (false));
   debugger.SetAsync (false);
   lldb::SBTarget target = debugger.CreateTargetWithFileAndArch (fileName.c_str (), LLDB_ARCH_DEFAULT);
   if (target.IsValid ())
   {
          cout << "point 3" << endl;
          // print (debugger);
          // cout << endl;
          // print (target, lldb::eDescriptionLevelFull);
          // print (target, lldb::eDescriptionLevelVerbose);
          // cout << endl;

          // lldb::SBBreakpoint main_bp = target.BreakpointCreateByName ("main", target.GetExecutable().GetFilename());
          lldb::SBBreakpoint main_bp = target.BreakpointCreateByName ("main");
          lldb::SBBreakpoint func_bp = target.BreakpointCreateByName ("func");
          lldb::SBBreakpoint click_bp = target.BreakpointCreateByName ("Example::method2");
          // print (main_bp)

          #if 0
          lldb::SBProcess process = target.LaunchSimple (NULL, NULL, NULL);
          #else
          cout << "MODULES " << target.GetNumModules() << endl;
          lldb::SBModule module = target.GetModuleAtIndex (0);
          {
             lldb::SBValueList list = module.FindGlobalVariables(target, "", 10);
             cout << "VARIABLES " << list.GetSize() << endl;
          }
          cout << "UNITS " << module.GetNumCompileUnits() << endl;
          {
             int cnt = module.GetNumSymbols();
             cout << "SYMBOLS " << cnt << endl;
             // for (int i = 0 ; i < cnt; i++)
             //    cout << "   symbol " << module.GetSymbolAtIndex(i).GetDisplayName() << endl;
          }
          {
             int cnt = module.GetNumSections ();
             cout << "SECTIONS " << cnt << endl;
             for (int i = 0 ; i < cnt; i++)
                 cout << "   section " << module.GetSectionAtIndex(i).GetName() << endl;
          }

          {
             // lldb::SBTypeList list = module.GetTypes ();
             lldb::SBTypeList list = module.GetTypes (lldb::eTypeClassClass);
             // lldb::SBTypeList list = module.FindTypes ("finch::CmmStatSect");
             int cnt = list.GetSize();
             cout << "TYPES " << cnt << endl;
             for (int i = 0 ; i < cnt; i++)
             {
                lldb::SBType type = list.GetTypeAtIndex (i);
                string name = type.GetName();
                // string name = type.GetDisplayTypeName();
                // if (name.substr (0, 7) == "finch::")
                if (name.find ("finch") != string::npos)
                {
                   cout << "   type " << name << endl;
                   {
                      int cnt = type.GetNumberOfFields ();
                      for (int i = 0 ; i < cnt; i++)
                      {
                          lldb::SBTypeMember m = type.GetFieldAtIndex (i);
                          cout << "      field " << m.GetName()
                               << " : " << m.GetType().GetDisplayTypeName()
                               << " @ " << m.GetOffsetInBytes ()<< endl;
                      }
                   }
                   if (0)
                   {
                      int cnt = type.GetNumberOfMemberFunctions ();
                      for (int i = 0 ; i < cnt; i++)
                      {
                          lldb::SBTypeMemberFunction m = type.GetMemberFunctionAtIndex (i);
                          cout << "      method " << m.GetName()
                               << " : " << m.GetType().GetDisplayTypeName()
                               << endl;
                      }
                   }
                   {
                      int cnt = type.GetNumberOfFields ();
                      for (int i = 0 ; i < cnt; i++)
                      {
                          lldb::SBTypeMember m = type.GetFieldAtIndex (i);
                          cout << "      field " << m.GetName()
                               << " : " << m.GetType().GetDisplayTypeName()
                               << " @ " << m.GetOffsetInBytes ()<< endl;
                      }
                   }
                }
             }
          }

          #if 0
          lldb::SBListener listener;
          lldb::SBError error;
          lldb::SBProcess process = target.AttachToProcessWithID (listener, getpid(), error);
          cout << error.GetCString() << endl;
          #endif
          #endif
          cout << "point 3.1" << endl;
          #if 0
          if (process.IsValid ())
          {
             cout << "point 4" << endl;

             lldb::StateType state = process.GetState ();
             cout << "point 5" << endl;
             if (state == lldb::eStateStopped)
             {
                cout << "point 6" << endl;
                debug_view->displayState (target, process);

                /*
                lldb::SBThread thread = process.GetThreadAtIndex (0);
                print (thread, lldb::eDescriptionLevelVerbose);
                thread.StepOver ();
                cout << "point 7" << endl;

                state = process.GetState ();
                while (state == lldb::eStateStopped)
                {
                    cout << "point 8" << endl;
                    // debug_view->displayState (target, process);
                    process.Continue ();
                }
                */

             }
          }
          #endif
   }
   lldb::SBDebugger::Terminate ();
   cout << "point 0" << endl;
}

/* ---------------------------------------------------------------------- */

// see lldb/examples/functions/main.cc

// dnf install lldb-devel
// apt install liblldb-dev

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
