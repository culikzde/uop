
/* clang-util.h */

#ifndef CLANG_UTIL_H
#define CLANG_UTIL_H

#include <string>
#include <vector>
// using namespace std;

/* ---------------------------------------------------------------------- */

using std::string;
using std::vector;

typedef vector <string> string_list;

inline void add_list (string_list & target, string_list list)
{
    target.insert (target.end (), list.begin (), list.end ());
}

/* ---------------------------------------------------------------------- */

string_list common_cflags ();
string_list common_libs ();

/* ---------------------------------------------------------------------- */

string_list gcc_options (string gcc = "");

string_list pkg_options (string pkg, string options, string pkg_config_path = "");
string_list pkg_cflags (string pkg, string pkg_config_path = "");
string_list pkg_libs (string pkg, string pkg_config_path = "");

/* ---------------------------------------------------------------------- */

const string pkg_gtk1 = "gtk+";
const string pkg_gtk2 = "gtk+-2.0";
const string pkg_gtk3 = "gtk+-3.0";
const string pkg_gtk4 = "gtk4";

const string pkg_qt3 = "qt-mt";
const string pkg_qt4 = "QtGui";
const string pkg_qt5 = "Qt5Widgets";
const string pkg_qt6 = "Qt6Widgets";

/*
const string pkg_qt4_moc = "QtGui";
const string opt_qt4_moc = "--variable=moc_location";
const string opt_qt4_uic = "--variable=uic_location";

const string pkg_qt5_moc = "Qt5";
const string opt_qt5_moc = "--variable=moc";
const string opt_qt5_qmake = "--variable=qmake";

const string pkg_qt6_moc = "Qt6";
const string opt_qt6_moc = "--variable=moc";
const string opt_qt6_qmake = "--variable=qmake";
*/

const string pkg_inventor = "SoQt";

/* ---------------------------------------------------------------------- */

int storeFileName (string file_name);
string recallFileName (int index);

void show_error (string msg);

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // CLANG_UTIL_H
