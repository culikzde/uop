#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

// typedef signed long long large;
// typedef unsigned long long ularge;

// typedef int middle;
// typedef unsigned int umiddle;

typedef int64_t large;
typedef int64_t ularge;

typedef int32_t middle;
typedef uint32_t umiddle;

#ifdef __x86_64__

#if 1
void asm_add_mul (ularge * target, ularge * a, ularge * b, ularge a_len, ularge b_len)
// target := target + a * b
// target length = a_len + b_len
{
    asm (

    // r9 ... target ... increment in a_loop ( --> rdi in b_loop )
    // r10 ... a ... increment in a_loop
    // r11 ... b ... ( --> rsi in b_loop ) (r11 is const)
    // r12 ... a_len ... decrement in a_loop
    // r13 ... b_len ... ( --> rcx in b_loop ) (r13 is const)
    // rdx:rax ... accumulator, rbx ... carry
    // r14 ... temporary value from a

    "movq %0, %%r9  ;"
    "movq %1, %%r10 ;"
    "movq %2, %%r11 ;"
    "movq %3, %%r12 ;"
    "movq %4, %%r13 ;"

    ".intel_syntax noprefix;"

    "a_loop : ;" // outer loop

    "mov r14, [r10] ;" // value from a
    "add r10, 8 ;" // move a pointer

    "mov rdi, r9 ;" // copy of target
    "add r9, 8 ;" // move target pointer (dalsi ukladani o rad vyse)

    "mov rsi, r11 ;" // b, right parameter

    "mov rbx, 0 ; " // carry
    "mov rcx, r13 ;" // b_len

    "b_loop : ;" // inner loop

    "mov rax, r14  ;" // value from a
    "mul qword ptr [rsi] ;" // dx:ax := value from a * value from b
    "add rsi, 8 ;" // move b pointer

    "add rax, rbx ;" // add carry
    "adc rdx, 0 ;"
    // (2^k-1)*(2^k-1)

    "mov [rdi], rax ; " // store result
    "add rdi, 8 ;" // result pointer
    "mov rbx, rdx ; " // next carry

    "dec rcx ;"
    "jnz b_loop ;" // repeat b_len times

    "mov [rdi], rbx ;" // store carry

    "dec r12 ;"
    "jnz b_loop ;" // repeat a_len times


    ".att_syntax;" // back to AT&T
    : // output
    : "g" (target), "g" (a), "g" (b), "g" (a_len), "g" (b_len) // input
    : "rax", "rbx", "rcx", "rdx", "rsi", "rdi", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "cc" // modified
    );
}
#endif

int asm_add_direct (ularge * a, ularge * b, ularge len)
// NEFUNGUJE S -_2
// a := a + b
// result = true => unsigned overflow
{
ularge result;
#if 1
    asm (
      ".intel_syntax noprefix;"

      "mov rdi, %1;" // pointer to destination and left operand
      "mov rsi, %2;" // pointer to right operand
      "mov rcx, %3;" // number of 64-bits words

      "mov rbx, 0;"  // carry
      "start: ;" // begin of loop

      "xor rdx, rdx;" // rdx := 0, next carry

      "mov rax, [rdi];" // left
      "add rax, [rsi];" // plus right

      "adc rdx, 0;"    // rdx := rdx + 0 + cf, rdx := carry from left plus right

      "add rax, rbx;"  // add old carry

      "adc rdx, 0;"    // rdx := complete next carry

      "mov rbx, rdx;"   // rbx := carry for following loop cycle

      "mov [rdi], rax;" // store value

      "add rsi, 8;"    // move left pointer
      "add rdi, 8;"    // move right pointer

      "dec rcx;"       // decrement counter
      "jnz start;"     // repeat if counter != 0

      "mov %0, rbx;"   // result is carry
      ".att_syntax;"   // back to AT&T

    : "=r" (result) // output
    : "r" (a), "r" (b), "r" (len) // input
    : "rax", "rbx", "rcx", "rdx", "rsi", "rdi", "cc" // modified
    );
#else
    asm
    (
        "movq %1, %%rdi ;"
        "movq %2, %%rsi ;"
        "movq %3, %%rcx ;"

        "movq $0, %%rbx ;"  // carry
        "start: ;" // begin of loop

        "xorq %%rdx, %%rdx ;" // rdx := 0, next carry

        "movq (%%rdi), %%rax ;" // left
        "addq (%%rsi), %%rax ;" // plus right

        "adcq $0, %%rdx ;"    // rdx := carry from left plus right

        "addq %%rbx, %%rax ;"  // add old carry

        "adcq $0, %%rdx ;"    // rdx := complete next carry

        "movq %%rdx, %%rbx ;"   // rbx := carry for following loop cycle

        "movq %%rax, (%%rdi) ;" // store value

        "addq $8, %%rsi ;"    // move left pointer
        "addq $8, %%rdi ;"    // move right pointer

        "decq %%rcx ;"       // decrement counter
        "jnz start ;"     // repeat if counter != 0

        "movq %%rbx, %0 ;"   // result is carry

      : "=g" (result) // output
      : "g" (a), "g" (b), "g" (len) // input
      : "rax", "rbx", "rcx", "rdx", "rsi", "rdi", "cc" // modified
    );
    // return result;
#endif
return result;
}

#endif

int main()
{
    assert (sizeof (large) == 2 * sizeof (middle));
    assert (sizeof (large) == 2 * sizeof (middle));

    assert (sizeof (large) == 8);
    assert (sizeof (ularge) == 8);

    if (1)
    {
       ularge a [4] = { 1, 2, 3, 4 };
       ularge b [4] = { 10, 20, 30, 40};

       // ularge a [4] = { 1,                   2,  3,                  4 };
       // ularge b [4] = { 0xffffffffffffffff, 20, 30, 0xffffffffffffffff };

       // ularge a [4] = { 1,                  0,                  0,                  0 };
       // ularge b [4] = { 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff };

       ularge t = asm_add (a, b, 4);
       printf("Result is %lu, %lu, %lu, %lu, %lu\n", a[0], a[1], a[2], a[3], t);
       printf("Result is %#010lx, %#010lx, %#010lx, %#010lx, %#010lx\n", a[0], a[1], a[2], a[3], t);
    }
    else
    {
        ularge a [4] = { 0, 2, 0, 0 };
        ularge b [4] = { 0, 3, 0, 0};
        ularge c [4+4] = { 0, 0, 0, 0,  0, 0, 0, 0};

        // asm_add_mul (c, a, b, 4, 4);

        int cnt = sizeof (c) / sizeof (c[0]);
        for (int i = 0; i < cnt; i++)
        {
            printf("%#010lx, %lu \n", c[i], c[i]);
        }

    }
   return 0;
}
