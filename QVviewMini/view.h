#ifndef VIEW_H
#define VIEW_H

// #include "precompiled.h"

#include <QMainWindow>

#include <QTabWidget>
#include <QSplitter>
#include <QVBoxLayout>

#include "edit.h"
#include "tree.h"
#include "prop-table.h"

#include "files.h"
#include "grep.h"

#include <string>
#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

int storeFileName (string file_name);

string recallFileName (int index);
int    recallFileIndex (string file_name);

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window (QWidget * parent = NULL);
    ~ Window ();

public :
    QMenu * fileMenu;
    QMenu * editMenu;
    QMenu * viewMenu;
    QMenu * runMenu;

    Tree * tree;
    Tree * classes;
    Tree * project;

    FileView * file_view;
    GrepView * grep_view;

    PropertyTable * prop;

    QTabWidget * left_tools;
    QTabWidget * right_tools;

    QWidget * middle_widget;
    QVBoxLayout * middle_layout;
    QTabWidget * tabs;

    QTextEdit * empty_edit;
    FindBox * find_box;
    GoToLineBox * line_box;

    QSplitter * vsplitter;
    QSplitter * hsplitter;

    QHash < QString, Edit * > sourceEditors;

    QTextEdit * info;

private:
    void readFile (Edit * edit, QString fileName);

    Edit * getEditor ();
    QString getEditorFileName (Edit * edit);
    QString getCurrentFileName ();
    QString getCurrentText ();

    void addMenuItem (QMenu * menu, QString title, QObject * obj, const char * slot, const char * shortcut = NULL);
    void addView (QString title, QWidget * widget);

public:
    Edit * openEditor (QString fileName, int line = 0, int col = 0);
    void removeEmptyEditor ();

    QTextEdit * getInfo () { return info; }
    PropertyTable * getProp () { return prop; }

public:
    void displayProperties (TreeItem * item);

public slots:
    void openFile ();
    void saveFile ();
    void saveFileAs ();
    void reloadFile ();
    void quit ();

    void indent ();
    void unindent ();

    void comment ();
    void uncomment ();

    void setBookmark ();
    void prevBookmark ();
    void nextBookmark ();
    void clearBookmarks ();

    void find ();
    void findPrev ();
    void findNext ();
    void replace ();
    void findSelected ();
    void findIncremental ();
    void goToLine ();
    void findInFiles ();

    void enlargeFont ();
    void shrinkFont ();

    void prevFunction ();
    void nextFunction ();

    void moveLinesUp ();
    void moveLinesDown ();

    void showDesigner ();
    void runMini ();

    void showClangDecl ();
    void runInterpreter ();
    void runCompiler ();
    void runDebugger ();
};

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // VIEW_H
