
/* clang-util.cc */

#include "clang-util.h"

#include <stdlib.h> // getenv
#include <stdio.h> // popen

#include <iostream> // getenv
using namespace std;

const char cr = '\r';
const char lf = '\n';

/* ---------------------------------------------------------------------- */

string process_query (FILE * f)
{
   string result = "";
   bool stop = false;

   while (! stop)
   {
      // read line
      char * buf = NULL;
      size_t size = 0;
      ssize_t len = getline (&buf, &size, f);
      if (len >= 0)
      {
         if (len > 0 && buf [len] == 0) len --;
         if (result != "")
            result += lf;
         result += string (buf, len);
         free (buf);
      }
      else
      {
         stop = true;
      }
   }

   return result;
}

string query (string command)
{
   string result = "";

   #ifdef __linux__
      FILE * f = popen (command.c_str (), "r");
      if (f == NULL)
      {
         show_error ("Cannot execute: " + command);
      }
      else
      {
         result = process_query (f);
         pclose (f);
      }
   #else
      show_error ("Cannot execute: " + command);
   #endif

   // info (command + " -> " + result);
   return result;
}

/* ---------------------------------------------------------------------- */

string get_token (const string txt, int & inx, int len)
{
   int i = inx;
   while (i < len && txt [i] <= ' ' && txt [i] != lf) i++;
   int start = i;
   while (i < len && txt [i] > ' ' && txt [i] != lf) i++;
   inx = i;
   // info ("get_token " + QuoteStr (txt.substr (start, inx-start)));
   return txt.substr (start, inx-start);
}

string get_rem (const string txt, int & inx, int len)
{
   int i = inx;
   while (i < len && txt [i] <= ' ' && txt [i] != lf) i++;
   int start = i;
   while (i < len && txt [i] != lf) i++;
   int stop = i;

   if (txt[i] == lf)
      i ++;
   inx = i;

   // info ("get_rem " + QuoteStr (txt.substr (start, stop-start)));
   return txt.substr (start, stop-start);
}

vector <string> gcc_options (string gcc)
{
   if (gcc == "")
      gcc = "gcc";

   vector <string> options;

   string cmd = "echo 'int main () { }' | " + gcc + " -v -x c++ -dD -E - 2>&1";
   string answer = query (cmd);

   // include directories

   {
      string txt = answer;

      string pattern = "#include <...> search starts here:";
      string::size_type start = txt.find (pattern);
      if (start != string::npos)
         start = start + pattern.length ();

      string::size_type stop = txt.find ("End of search list");

      if (start != string::npos && stop != string::npos)
      {
         txt = txt.substr (start, stop-start);

         int len = txt.length ();
         int inx = 0;

         while (inx < len)
         {
            string dir = get_rem (txt, inx, len);
            if (dir != "")
            {
               options.push_back ("-I" + dir);
            }
         }
      }
   }

   // conditional symbols

   if (0)
   {
      string txt = answer;
      int len = txt.length ();
      int inx = 0;

      while (inx < len)
      {
         string key = get_token (txt, inx, len);
         if (key == "#define")
         {
            string name = get_token (txt, inx, len);
            string value = get_rem (txt, inx, len);
            if (value == "")
               options.push_back ("-D" + name);
            else
               options.push_back ("-D" + name + "=" + value);
         }
         else
         {
            get_rem (txt, inx, len);
         }
      }
   }

   return options;
}

/* ---------------------------------------------------------------------- */

string_list pkg_options (string pkg, string options, string pkg_config_path)
{
   string cmd = "pkg-config " + pkg + " " + options;
   if (pkg_config_path != "")
   {
      string env = getenv ("PKG_CONFIG_PATH");
      if (env != "") pkg_config_path = pkg_config_path + ":" + env;
      cmd = "PKG_CONFIG_PATH=" + pkg_config_path + " " + cmd;
   }
   string txt = query (cmd);
   int len = txt.length ();

   string_list list;
   int inx = 0;
   while (inx < len)
   {
      while (inx < len && txt [inx] <= ' ') inx ++;
      int start = inx;
      while (inx < len && txt [inx] > ' ') inx ++;
      if (start < inx)
      list.push_back (txt.substr (start, inx-start));
   }

   return list;
}

string_list pkg_cflags (string pkg, string pkg_config_path)
{
   return pkg_options (pkg, "--cflags", pkg_config_path);
}

string_list pkg_libs (string pkg, string pkg_config_path)
{
   return pkg_options (pkg, "--libs", pkg_config_path);
}

/* ---------------------------------------------------------------------- */

string_list common_cflags ()
{
   // string_list list = gcc_options ("clang");
   string_list list = gcc_options ();
   list.push_back ("-fPIC");
   list.push_back ("-I.");
   add_list (list, pkg_cflags (pkg_qt5));
   return list;
}

string_list common_libs ()
{
   string_list list;
   add_list (list, pkg_libs (pkg_qt5));
   return list;
}

/* ---------------------------------------------------------------------- */

void show_error (string msg)
{
   cerr << msg << endl;
}

/* ---------------------------------------------------------------------- */
