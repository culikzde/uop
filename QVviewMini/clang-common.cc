
/* clang-common.cc */

#include "clang-common.h"
#include "clang-util.h"

/* ---------------------------------------------------------------------- */

#include "llvm/Config/llvm-config.h"

#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"

#include "clang/Frontend/ASTUnit.h"
#include "clang/AST/ASTContext.h"

#include "clang/AST/AST.h"
#include "llvm/Support/Casting.h"

#if LLVM_VERSION_MAJOR < 18
   typedef clang::ASTUnit * unit_t;
   typedef clang::ASTUnit * unit_param_t;
#else
   typedef std::unique_ptr<clang::ASTUnit> unit_t;
   typedef std::unique_ptr<clang::ASTUnit> & unit_param_t;
#endif

/* ---------------------------------------------------------------------- */

#include <iostream>
#include <vector>

using std::string;
using std::vector;

using std::cout;
using std::endl;

typedef vector <string> string_list;

/* ---------------------------------------------------------------------- */

#include "tree.h"
#include "code.h"
/* ---------------------------------------------------------------------- */

class QTreeWidgetItem;

void display_code (QTreeWidgetItem * branch, SimBasic * basic);

/* ---------------------------------------------------------------------- */

unit_t load_ast_unit (string_list options)
{
   int argc = options.size ();
   typedef const char * char_ptr;
   const char * * argv = new char_ptr [argc];
   for (int i = 0; i < argc; i++)
   {
      argv [i] = options [i].c_str ();
      cout << "ARG " << argv[i] << endl;
   }

   // http://stackoverflow.com/questions/29955255/get-ast-for-c-fragment-using-clang

   llvm::IntrusiveRefCntPtr<clang::DiagnosticOptions> diagOpts = new clang::DiagnosticOptions();
   clang::TextDiagnosticPrinter * diagClient = new clang::TextDiagnosticPrinter (llvm::errs(), &*diagOpts);

   llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> diagID (new clang::DiagnosticIDs ());
   llvm::IntrusiveRefCntPtr<clang::DiagnosticsEngine> diags = new clang::DiagnosticsEngine (diagID, &*diagOpts, diagClient);

   unit_t unit = clang::ASTUnit::LoadFromCommandLine (
       argv,
       argv + argc,
       std::make_shared <clang::PCHContainerOperations> (),
       diags,
       llvm::StringRef()
   );

   #if 0
   cout << "Point 1" << endl;
   if (unit == NULL)
      cout << "No unit" << endl;
   else
   {
   cout << "Point 2" << endl;
   clang::ASTContext & ctx = unit->getASTContext ();

      for (clang::DeclContext::decl_iterator I = ctx.getTranslationUnitDecl()->decls_begin (),
           E = ctx.getTranslationUnitDecl()->decls_end ();
           I != E;
           ++ I )
      {
         clang::Decl * decl = *I;
         string kind = decl->getDeclKindName ();
         string name = "";
         if (clang::NamedDecl * named_decl = llvm::dyn_cast <clang::NamedDecl> (decl))
         {
             name = named_decl->getQualifiedNameAsString ();
         }
         cout << "Declaration " << name << " : " << kind << endl;
      }
   }
   cout << "Last Point" << endl;
   #endif

   return unit;

}

/* ---------------------------------------------------------------------- */

class TreeScanner
{
private:
   QTreeWidget * tree;
   QTreeWidgetItem * branch;
   clang::SourceManager & source_manager;

public:
    TreeScanner (QTreeWidget * p_tree, clang::ASTContext & Ctx) :
       tree (p_tree),
       branch (NULL),
       source_manager (Ctx.getSourceManager ())
    {
       branch = new TreeItem (tree);
       branch->setText (0, "Top Level Declaration");

       ScanASTContext (Ctx);
    }

    void ScanASTContext (clang::ASTContext & Ctx)
    {
      for (clang::DeclContext::decl_iterator I = Ctx.getTranslationUnitDecl()->decls_begin (),
           E = Ctx.getTranslationUnitDecl()->decls_end ();
           I != E;
           ++ I )
      {
         ScanDecl (*I);
      }
    }

    void GetLocation (TreeItem * node, clang::Decl * D)
    {
       if (D != NULL)
       {
          clang::SourceLocation loc = D->getLocation ();
          if (loc.isValid () && loc.isFileID())
          {
             clang::PresumedLoc ploc = source_manager.getPresumedLoc (loc);
             if (ploc.isValid ())
             {

                 string file_name = ploc.getFilename( );
                 node->src_file = storeFileName (file_name);
                 node->src_line = ploc.getLine ();
                 node->src_col = ploc.getColumn();
              }
          }
       }
    }

    void ScanDecl (clang::Decl * D)
    {
       // see clang/include/clang/AST/DeclNodes.def
       // see clang/lib/AST/DeclPrinter.cpp
       assert (D != NULL);

       // string location = GetLocation (D);
       string kind = D->getDeclKindName ();

       string name = "";
       if (clang::NamedDecl * ND = llvm::dyn_cast <clang::NamedDecl> (D))
       {
          name = ND->getQualifiedNameAsString ();
       }

       TreeItem * node = new TreeItem (branch);
       node->setText (0, QString::fromStdString (name + " : " + kind));
       GetLocation (node, D);

       clang::DeclContext * DC = llvm::dyn_cast <clang::DeclContext> (D);
       if (DC != NULL)
       {
          QTreeWidgetItem * save_branch = branch;
          branch = node;

          for (clang::DeclContext::decl_iterator I = DC->decls_begin (), E = DC->decls_end (); I != E; ++I)
              ScanDecl (*I);

          branch = save_branch;
      }
    }

};

/* ---------------------------------------------------------------------- */

class CodeScanner
{
private:
   clang::SourceManager & source_manager;

public:
    CodeScanner (QTreeWidget * tree, clang::ASTContext & Ctx) :
       source_manager (Ctx.getSourceManager ())
    {
       SimNamespace * ns = new SimNamespace;
       ScanASTContext (ns, Ctx);

       tree->clear ();
       QTreeWidgetItem * root = tree->invisibleRootItem ();
       for (SimBasic * item : ns->item_list)
           display_code (root, item);
    }

    void ScanASTContext (SimScope * scope, clang::ASTContext & Ctx)
    {
      for (clang::DeclContext::decl_iterator I = Ctx.getTranslationUnitDecl()->decls_begin (),
           E = Ctx.getTranslationUnitDecl()->decls_end ();
           I != E;
           ++ I )
      {
         ScanDecl (scope, *I);
      }
    }

    void GetLocation (SimBasic * node, clang::Decl * D)
    {
       if (D != NULL)
       {
          clang::SourceLocation loc = D->getLocation ();
          if (loc.isValid () && loc.isFileID())
          {
             clang::PresumedLoc ploc = source_manager.getPresumedLoc (loc);
             if (ploc.isValid ())
             {
                 string file_name = ploc.getFilename( );
                 node->src_file = storeFileName (file_name);
                 node->src_line = ploc.getLine ();
                 node->src_col = ploc.getColumn();
              }
          }
       }
    }

    void ScanDecl (SimScope * scope, clang::Decl * D)
    {
       assert (D != NULL);

       // string location = GetLocation (D);
       clang::Decl::Kind kind = D->getKind ();

       string name = "";
       if (clang::NamedDecl * ND = llvm::dyn_cast <clang::NamedDecl> (D))
       {
          name = ND->getQualifiedNameAsString ();
       }

       SimBasic * basic = nullptr;

       if (kind == clang::Decl::Namespace)
       {
           basic = new SimNamespace;
       }
       else if (kind == clang::Decl::CXXRecord)
       {
           SimClass * cls = new SimClass;
           clang::CXXRecordDecl * C = dynamic_cast <clang::CXXRecordDecl *> (D);
           basic = cls;
       }
       /*
       else if (kind == clang::Decl::EnumType)
       {
           basic = new Enum;
       }
       */
       else if (kind == clang::Decl::Var)
       {
           basic = new SimVariable;
       }
       else if (kind == clang::Decl::Function)
       {
           SimFunction * func = new SimFunction;
           clang::FunctionDecl * F = dynamic_cast <clang::FunctionDecl *> (D);
           basic =  func;
       }

       if (basic != nullptr)
       {
          basic->name = name;
          GetLocation (basic, D);
          scope->add (basic);
       }

       SimScope * basic_scope = basic->conv_SimScope ();
       if (basic_scope != nullptr)
       {
          clang::DeclContext * DC = llvm::dyn_cast <clang::DeclContext> (D);
          if (DC != NULL)
          {
             for (clang::DeclContext::decl_iterator I = DC->decls_begin (), E = DC->decls_end (); I != E; ++I)
                 ScanDecl (basic_scope, *I);
          }
       }
    }
};

/* ---------------------------------------------------------------------- */

void display_code (QTreeWidgetItem * branch, SimBasic * basic)
{
    // cout << "DISPLAY " << basic->name << endl;
    TreeItem * node = new TreeItem;
    node->setText (0, QString::fromStdString (basic->name));
    node->src_file = basic->src_file;
    node->src_line = basic->src_line;
    node->src_col = basic->src_col;
    branch->addChild (node);

    SimScope * scope = basic->conv_SimScope ();
    if (scope != nullptr)
       for (SimBasic * item : scope->item_list)
          display_code (node, item);
}

/* ---------------------------------------------------------------------- */

void display_ast_unit (QTreeWidget * tree, unit_param_t unit)
{
    if (unit != NULL)
    {
       clang::ASTContext & ctx = unit->getASTContext ();
       TreeScanner (tree, ctx);
    }
}

/* ---------------------------------------------------------------------- */

void code_from_unit (QTreeWidget * classes, unit_param_t unit)
{
    if (unit != NULL)
    {
       clang::ASTContext & ctx = unit->getASTContext ();
       CodeScanner scanner (classes, ctx);
    }
}

/* ---------------------------------------------------------------------- */

void displayTranslationUnit (QTreeWidget * tree, QTreeWidget * classes, QString file_name_qstr)
{
    std::string file_name = file_name_qstr.toStdString ();

    string_list list;
    list.push_back ("compiler_name"); // neccessary for llvm 3.8
    list.push_back (file_name);

    add_list (list, common_cflags ());

    unit_t unit = load_ast_unit (list);

    display_ast_unit (tree, unit);
    code_from_unit (classes, unit);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
