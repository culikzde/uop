#ifndef TREE_H
#define TREE_H

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGraphicsItem>

class Window;
class PropertyTable;

#ifdef USE_MINI
   class SimBasic;
   class CmmDecl;
#endif

#ifdef USE_CMM
   #include "cmm_browser.hpp"
   USE_NAMESPACE
   /*
   #include "std.h"
   OPEN_NAMESPACE
      class CmmBasic;
      class CmmBrowser;
   CLOSE_NAMESPACE
   using NAMESPACE::CmmBasic;
   using NAMESPACE::CmmBrowser;
   */
#endif

#ifdef USE_CLANG
   class clang::Decl;
#endif

/* ---------------------------------------------------------------------- */

class Tree : public QTreeWidget
{
Q_OBJECT
public:
    Tree (Window * p_win = NULL);

private:
    Window * win;

public slots:
    void onActivated (QTreeWidgetItem * node, int column);
    void onCurrentItemChanged (QTreeWidgetItem * current, QTreeWidgetItem * previous);
};

/* ---------------------------------------------------------------------- */

// class CmmBasic;

class TreeItem : public QTreeWidgetItem
{
public:
    int src_file;
    int src_line;
    int src_col;

    QObject * qt_obj;
    QGraphicsItem * gra_obj;

    #ifdef USE_MINI
       SimBasic * sim_obj;
       CmmDecl * cmm_obj;
    #endif

    #ifdef USE_CLANG
       clang::Decl * clang_obj;
    #endif

public:
    TreeItem (QTreeWidget * parent, QString text = "");
    TreeItem (QTreeWidgetItem * parent = NULL, QString text = "");
};

/* ---------------------------------------------------------------------- */

#endif // TREE_H
