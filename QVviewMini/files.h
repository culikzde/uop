/* files.h */

#ifndef FILES_H
#define FILES_H

#include <QTreeView>

class QMenu;

/* ---------------------------------------------------------------------- */

class FileView : public QTreeView
{
   Q_OBJECT
   public:
      FileView (QWidget * win);
      void showPath (QString path);

   public slots:
      void onActivated (const QModelIndex & index);
      void onContextMenu (const QPoint & pos);
};

/* ---------------------------------------------------------------------- */

#endif // FILES_H
