/* files.cc */

#include "files.h"

#include <QMenu>

#include <QFileIconProvider>
#include <QMimeDatabase>
#include <QImageReader>

#include <QDir>
#include <QHeaderView>

/* ---------------------------------------------------------------------- */

#if QT_VERSION >= 0x040400
   #define USE_FILE_SYS_MODEL
#endif

#ifdef USE_FILE_SYS_MODEL
   #include <QFileSystemModel>
   typedef QFileSystemModel DirModel;
#else
   #include <QDirModel>
   typedef QDirModel DirModel;
#endif

/* ---------------------------------------------------------------------- */

class IconProvider : public QFileIconProvider
{
   public:
       IconProvider ();
       QIcon icon (const QFileInfo &info) const override;
   private:
       QMimeDatabase database;
       QList <QByteArray> formats;
};

IconProvider::IconProvider ()
{
   formats = QImageReader::supportedImageFormats();
}

QIcon IconProvider::icon (const QFileInfo & fileInfo) const
{
   QIcon result;
   QString ext = fileInfo.suffix (); // temporary variable, suffix () type is not known to c2py
   if (formats.contains (ext.toLatin1()))
   {
      result = QIcon (fileInfo.filePath ());
   }
   if (result.isNull ())
   {
      QMimeType info = database.mimeTypeForFile (fileInfo);
      result = QIcon::fromTheme (info.iconName ());
   }
   if (result.isNull ())
   {
      result =  QFileIconProvider::icon (fileInfo);
   }
   return result;
}

/* ---------------------------------------------------------------------- */

FileView::FileView (QWidget * win) :
   QTreeView (win)
   // last_menu (NULL)
{
   QString path = QDir::currentPath ();

   DirModel * model = new DirModel ();
   #ifdef USE_FILE_SYS_MODEL
      model->setFilter (model->filter() | QDir::Files);
      // model->setFilter (QDir::AllEntries);
      model->setRootPath (path);
      #if QT_VERSION >= QT_VERSION_CHECK (5,14,0)
          model->setOption (DirModel::DontWatchForChanges, false);
      #endif
   #endif

   model->setIconProvider (new IconProvider);

   setModel (model);
   setCurrentIndex (model->index (path));

   QHeaderView * hdr = header ();
   hdr->setSectionResizeMode (0, QHeaderView::ResizeToContents);
   hdr->setSectionResizeMode (1, QHeaderView::Fixed);
   hdr->hideSection (2); // file type
   hdr->hideSection (3); // time and date

   connect (this, SIGNAL (activated (QModelIndex)), this, SLOT (onActivated (QModelIndex)));

   setContextMenuPolicy (Qt::CustomContextMenu);
   connect (this, SIGNAL (customContextMenuRequested (const QPoint &)), this, SLOT (onContextMenu (QPoint)));
}

void FileView::onActivated (const QModelIndex & index)
{
   // if self.win != None :
   DirModel * m = dynamic_cast < DirModel *> (model ());
   QString fileName = m->filePath (index);
   // self.win.showStatus (fileName)
   // self.win.loadFile (fileName)
}

void FileView::onContextMenu (const QPoint & pos)
{
   QMenu * menu = new QMenu (this);

   QStringList dir_list;
   // dir_list << sys.path [0];
   dir_list += QIcon::themeSearchPaths();
   #if QT_VERSION >= 0x050b00
      dir_list += QIcon::fallbackSearchPaths();
   #endif

   int len = dir_list.size ();
   for (int inx = 0; inx < len ; inx ++)
   {
      QString path = dir_list [inx];
      QAction * act = menu->addAction (path);
      connect (act, & QAction::triggered , this, [=] { showPath (path); });
   }

   menu->exec (mapToGlobal (QPoint (pos)));
}

void FileView::showPath (QString path)
{
   // QString path = "";
   // if (last_menu != NULL)
   //     path = last_menu->activeAction()->text ();

   DirModel * m = dynamic_cast < DirModel *> (model ());
   QModelIndex inx = m->index (path);
   setCurrentIndex (inx);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
