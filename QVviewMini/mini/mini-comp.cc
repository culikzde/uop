
/* mini-comp.cc */

#include "mini-comp.h"

#ifdef USE_CLANG
   #undef NAMESPACE
   #include "llvm/Config/llvm-config.h"
   // #include "clang/AST/ASTContext.h"
   #include "clang/Frontend/CompilerInstance.h"
   #include "clang/Frontend/ASTUnit.h"
   #include "clang/Frontend/TextDiagnosticPrinter.h"
   #undef NAMESPACE
#endif

#ifdef USE_DEBUGGER
   #include "lldb/API/LLDB.h"
#endif

#include <map>
#include <unordered_map>
#include <iostream>
using namespace std;

/* --------------------------------------------------------------------- */

const int intInx = 1;
const int unsignedIntInx = 2;
const int longInx = 3;
const int unsignedLongInx = 4;
const int floatInx = 5;
const int doubleInx = 6;
const int unknownInx = 7;

SimSimpleType * intType;
SimSimpleType * unsignedIntType;
SimSimpleType * longType;
SimSimpleType * unsignedLongType;

SimSimpleType * floatType;
SimSimpleType * doubleType;

SimSimpleType * boolType;
SimSimpleType * charType;

SimNamedType * stringType;

/* --------------------------------------------------------------------- */

void init_types ()
{
   intType = new SimSimpleType;
   intType->type_int = true;

   unsignedIntType = new SimSimpleType;
   unsignedIntType->type_unsigned = true;
   unsignedIntType->type_int = true;

   longType = new SimSimpleType;
   longType->type_long = true;

   unsignedLongType = new SimSimpleType;
   unsignedLongType->type_unsigned = true;
   unsignedLongType->type_long = true;

   floatType = new SimSimpleType;
   floatType->type_float = true;

   doubleType = new SimSimpleType;
   doubleType->type_double = true;

   boolType = new SimSimpleType;
   boolType->type_bool = true;

   charType = new SimSimpleType;
   charType->type_char = true;

   stringType = new SimNamedType;
   stringType->type_name = "string";
}

/* --------------------------------------------------------------------- */

class MiniCompiler : public Parser
{
private:
   bool remember_constructor;
   bool remember_tilda;
   bool remember_destructor;
   bool remember_global;
   bool remember_leading;

protected:
   DisplayStack display;

public:
   SimNamespace * global_scope;

public:
   MiniCompiler ()
   {
      global_scope = new SimNamespace;
      global_scope->name = "";
      display.push (global_scope);

      remember_constructor = false;
      remember_tilda = false;
      remember_destructor = false;
      remember_global = false;
      remember_leading = false;

      init_types ();
   }

   void init_types () { }

   void selectColor (SimBasic * item, string color_table = "") { }
   void markDefn (SimBasic * item) { }
   void markOutline (SimBasic * item) { }
   void markUsage (SimBasic * item, CmmDecl * decl) { }

   void openCompletion (SimBasic * item, bool outside = false) { }
   void closeCompletion (SimBasic * item, bool outside = false) { }

   void storeLocation (CmmBasic * loc)
   {
      loc->src_file = getTokenFileInx ();
      loc->src_line = getTokenLine ();
      loc->src_col  = getTokenCol ();
   }

   SimNamespace * compile_program ()
   {
      while (! isEof ())
      {
         parse_declaration ();
      }
      return global_scope;
   }

   void enter (SimBasic * item)
   {
      // cout << "ENTER " << item->name << endl;
      SimScope * scope = display. top ();
      enterIntoScope (scope, item);
   }

   void enterIntoScope (SimScope * scope, SimBasic * item)
   {
      scope->add (item);

      CmmDecl * item_decl = item->cmm_decl;
      CmmDecl * scope_decl = scope->cmm_decl;
      if (item_decl != nullptr)
      {
         item_decl->item_context = scope;
         if (scope_decl != nullptr && scope_decl->item_qual != "")
         {
            item_decl->item_qual = scope_decl->item_qual + '.' + item->name;
         }
         else
         {
            item_decl->item_qual = item->name;
         }
      }
   }

   void openScope (SimScope * scope)
   {
      display.push (scope);
   }

   void closeScope ()
   {
      display.pop ();
   }

   SimBasic * searchScope (SimScope * scope, string name)
   {
      return scope->get (name);
   }

   SimBasic * lookup (string name)
   {
      SimBasic * result = nullptr;
      int inx = display.size () - 1;
      while (result == nullptr && inx >= 0)
      {
         SimScope * scope = display.get (inx);
         for (SimScope * item : scope->using_list)
         {
            if (result == nullptr)
            {
               result = searchScope (item, name);
            }
         }
         if (result == nullptr)
         {
            result = searchScope (scope, name);
         }
         inx = inx - 1;
      }
      // if (result != nullptr) cout << "LOOKUP " << name << endl;
      return result;
   }

   void on_expr (CmmExpr * expr) override
   {
   }

   /*
   void on_declarator (CmmDeclarator * declarator) override
   {
   }
   */

   void on_namespace (CmmNamespaceDecl * decl) override
   {
      SimNamespace * ns = new SimNamespace;
      ns->name = decl->name;
      ns->cmm_decl = decl;
      decl->sim_basic = ns;

      copy_basic_location (ns, decl);
      enter (ns);

      selectColor (ns, "namespaceColors");
      markDefn (ns);
   }

   void on_open_namespace (CmmNamespaceDecl * decl) override
   {
      SimNamespace * ns = decl->sim_basic->conv_SimNamespace ();
      openScope (ns);
      openCompletion (ns);
   }

   void on_close_namespace (CmmNamespaceDecl * decl) override
   {
      SimNamespace * ns = decl->sim_basic->conv_SimNamespace ();
      closeCompletion (ns);
      closeScope ();
   }

   void on_class (CmmClassDecl * decl) override
   {
      SimClass * cls = new SimClass;
      cls->name = decl->name;
      cls->cmm_decl = decl;
      decl->sim_basic = cls;

      copy_basic_location (cls, decl);
      enter (cls);

      selectColor (cls, "classColors");
      markDefn (cls);
   }

   void on_open_class (CmmClassDecl * decl) override
   {
      SimClass * cls = decl->sim_basic->conv_SimClass ();
      openScope (cls);
      openCompletion (cls);
   }

   void on_close_class (CmmClassDecl * decl) override
   {
      SimClass * cls = decl->sim_basic->conv_SimClass ();
      closeCompletion (cls);
      closeScope ();
   }

   void on_member_initializer (CmmMemberInitializer * initializer) override
   {
      SimBasic * item = lookup (initializer->simp_name->id);
      if (item != nullptr)
         initializer->simp_name->item_decl = item->cmm_decl;
   }

   void on_enum (CmmEnumDecl * decl) override
   {
      SimEnum * e = new SimEnum;
      e->name = decl->name;
      decl->sim_basic = e;

      copy_basic_location (e, decl);
      enter (e);

      selectColor (e, "enumColors");
      markDefn (e);
   }

   void on_open_enum (CmmEnumDecl * decl) override
   {
      SimEnum * e = decl->sim_basic->conv_SimEnum ();
      openScope (e);
      openCompletion (e);
   }

   void on_enum_item (CmmEnumItem * decl) override
   {
      SimVariable * item = new SimVariable;
      item->name = decl->name;
      decl->sim_basic = item;

      copy_basic_location (item, decl);
      enter (item);

      selectColor (item, "variableColors");
      markDefn (item);
   }

   void on_close_enum (CmmEnumDecl * decl) override
   {
      SimEnum * e = decl->sim_basic->conv_SimEnum ();
      closeCompletion (e);
      closeScope ();
   }

   #if 0
   void on_typedef (CmmTypedefDecl * typedef_decl) override
   {
      clear_declarator (typedef_decl->declarator);
      copy_declarator_name (typedef_decl, typedef_decl->declarator);
      copy_declarator_location (typedef_decl, typedef_decl->declarator);

      copy_basic_location (typedef_decl, typedef_decl);
      enter (typedef_decl);

      selectColor (typedef_decl, "typedefColors");
      markDefn (typedef_decl);
   }

   void on_using (CmmUsingDecl * using_declaration) override
   {
      if (using_declaration->a_namespace)
      {
         SimScope * decl = using_declaration->qual_name->item_decl->conv_CmmNamespaceDecl();
         if (decl != nullptr)
         {
            if (decl->kind != namespaceDecl)
               error ("SimNamespace name expected");
            display.top()->using_list.push_back (decl);
         }
      }
   }
   #endif

   void on_simple_declaration (CmmSimpleDecl * simple_decl) override
   {
      copy_location (simple_decl, simple_decl->type_spec);
   }

   CmmDeclarator * get_inner_declarator (CmmDeclarator * declarator)
   {
       CmmDeclarator * result = nullptr;
       CmmDeclaratorKind kind = declarator->kind;

       if (kind == nestedDeclarator)
          result = declarator->conv_CmmNestedDeclarator()->inner_declarator;
       else if (kind == pointerDeclarator)
          result = declarator->conv_CmmPointerDeclarator()->inner_declarator;
       else if (kind == referenceDeclarator)
          result = declarator->conv_CmmReferenceDeclarator()->inner_declarator;
       else if (kind == arrayDeclarator)
          result = declarator->conv_CmmArrayDeclarator()->inner_declarator;
       else if (kind == functionDeclarator)
          result = declarator->conv_CmmFunctionDeclarator()->inner_declarator;

      return result;
   }

   CmmDeclarator * get_basic_declarator (CmmDeclarator * declarator)
   {
      bool stop = false;
      while (! stop)
      {
         CmmDeclaratorKind kind = declarator->kind;

         if (kind == nestedDeclarator)
            declarator = declarator->conv_CmmNestedDeclarator()->inner_declarator;
         else if (kind == pointerDeclarator)
            declarator = declarator->conv_CmmPointerDeclarator()->inner_declarator;
         else if (kind == referenceDeclarator)
            declarator = declarator->conv_CmmReferenceDeclarator()->inner_declarator;
         else if (kind == arrayDeclarator)
            declarator = declarator->conv_CmmArrayDeclarator()->inner_declarator;
         else if (kind == functionDeclarator)
            declarator = declarator->conv_CmmFunctionDeclarator()->inner_declarator;
         else
            stop = true;
      }
      return declarator;
   }

   CmmFunctionDeclarator * get_function_specifier (CmmDeclarator * declarator)
   {
      bool stop = false;
      while (! stop)
      {
         CmmDeclaratorKind kind = declarator->kind;

         if (kind == nestedDeclarator)
            declarator = declarator->conv_CmmNestedDeclarator()->inner_declarator;
         else if (kind == pointerDeclarator)
            declarator = declarator->conv_CmmPointerDeclarator()->inner_declarator;
         else if (kind == referenceDeclarator)
            declarator = declarator->conv_CmmReferenceDeclarator()->inner_declarator;
         else if (kind == arrayDeclarator)
            declarator = declarator->conv_CmmArrayDeclarator()->inner_declarator;
         // NO else if (kind == functionDeclarator)
         // NO   declarator = declarator->conv_CmmFunctionDeclarator()->inner_declarator;
         else
            stop = true;
      }

      CmmFunctionDeclarator * result = nullptr;
      if (declarator->kind == functionDeclarator)
         result = declarator->conv_CmmFunctionDeclarator();
      return result;
   }

   void on_simple_declarator (CmmDeclarator * declarator) override
   {
      declarator = get_basic_declarator (declarator);
      if (declarator->kind == emptyDeclarator)
         error ("Empty declarator");
   }

   void on_simple_item (CmmSimpleDecl * simple_declaration) override
   {
      CmmSimpleItem * simple_item = simple_declaration->items [simple_declaration->items.size()-1];
      CmmExpr * type_spec = simple_declaration->type_spec;
      CmmDeclarator * declarator = simple_item->declarator;
      if ( declarator->kind != functionDeclarator)
      {
         type_spec->constructor_flag = false;
         type_spec->destructor_flag = false;
      }
      else if (declarator->conv_CmmFunctionDeclarator()->inner_declarator->kind == emptyDeclarator)
      {
         // clear constructor_flag when declaration only starts with class name
         type_spec->constructor_flag = false;
         type_spec->destructor_flag = false;
      }

      // is it function
      CmmFunctionDeclarator * func_spec = get_function_specifier (declarator);
      if (func_spec != nullptr)
      {
         if (all_value_parameters (func_spec))
         {
            // variable with constructor parameters
         }
         else
         {
            // fuction declaration
            simple_item->is_function = true;
            simple_item->func_spec = func_spec;
            simple_item->simple_decl = simple_declaration;
         }
      }
      else
      {
         // variable with initialization block
      }

      // name and source location
      copy_declarator_name (simple_item, declarator);
      copy_declarator_location (simple_item, declarator);

      CmmName * qual_name = nullptr;
      if (type_spec->constructor_flag || type_spec->destructor_flag)
      {
         qual_name = simple_declaration->type_spec->conv_CmmName();
      }
      else
      {
         qual_name = get_declarator_name (declarator);
      }

      // type
      SimType * init_type = simple_declaration->type_spec->sim_type;
      simple_item->sim_type = get_declarator_type (init_type, declarator);

      /*
      cout << "TYPE ";
      if (SimNamedType * t = dynamic_cast <SimNamedType *> (init_type))
      {
         cout << t->type_name;
         cout << " ... " << t->type_decl->name;
      }
      cout << endl;
      */

      // constructor
      if (type_spec->constructor_flag)
      {
         simple_item->is_constructor = true;
         simple_item->item_name = "(constructor)" + get_constructor_name (type_spec);
         copy_location (simple_item, type_spec);
      }

      // destructor
      if (type_spec->destructor_flag)
      {
         simple_item->is_destructor = true;
         simple_item->item_name = '~' + get_constructor_name (type_spec);
         copy_location (simple_item, type_spec);
      }

      SimBasic * basic = nullptr;
      SimVariable * var = nullptr;
      SimFunction * func = nullptr;

      if (simple_item->is_function)
      {
          func = new SimFunction;
          basic = func;
      }
      else
      {
          var = new SimVariable;
          basic = var;
      }

      basic->name = simple_item->item_name;
      basic->cmm_decl = simple_item;
      simple_item->sim_basic = basic;

      // enter identifier into scope
      copy_basic_location (basic, simple_item);
      enter (basic);

      // color and mark position in source editor
      if (simple_item->is_function)
         selectColor (basic, "functionColors");
      else
         selectColor (basic, "variableColors");
      markDefn (basic);

      // function parameters
      if (simple_item->is_function)
      {
         // func_spec = get_function_specifier (declarator);
         openScope (func);
         enter_parameters (func_spec);
         closeScope ();
      }
   }

   string get_constructor_name (CmmExpr * type_spec)
   {
      string result = "";
      CmmDecl * decl = type_spec->item_decl;
      if (decl != nullptr)
      {
         result = decl->item_name;
      }
      return result;
   }

   CmmName * get_declarator_name (CmmDeclarator * declarator)
   {
      CmmName * result = nullptr;
      if (declarator != nullptr)
      {
         declarator = get_basic_declarator (declarator);
         if (declarator->kind == basicDeclarator)
         {
            result = declarator->conv_CmmBasicDeclarator()->qual_name;
         }
      }
      return result;
   }

   void copy_declarator_name (CmmDecl * target, CmmDeclarator * declarator)
   {
      if (declarator != nullptr)
      {
         declarator = get_basic_declarator (declarator);
         if (declarator->kind == basicDeclarator)
         {
            target->item_name = get_short_name (declarator->conv_CmmBasicDeclarator()->qual_name);
         }
         else
         {
            target->item_name = "";
         }
      }
   }

   void copy_declarator_location (CmmDecl * target, CmmDeclarator * declarator)
   {
      if (declarator != nullptr)
      {
         declarator = get_basic_declarator (declarator);
         if (declarator->kind == basicDeclarator)
            copy_name_location (target, declarator->conv_CmmBasicDeclarator()->qual_name);
      }
   }

   void clear_declarator (CmmDeclarator * declarator)
   {
      if (declarator != nullptr)
      {
         declarator = get_basic_declarator (declarator);
         if (declarator->kind == basicDeclarator)
            clear_declaration (declarator->conv_CmmBasicDeclarator()->qual_name);
      }
   }

   void on_open_function (CmmSimpleDecl * simple_declaration) override
   {
      CmmSimpleItem * simple_item = simple_declaration->items [simple_declaration->items.size()-1];
      if (simple_item->is_function)
      {
         // function, not variable with initialization block
         SimFunction * func = simple_item->sim_basic->conv_SimFunction ();

         openScope (func);
         openCompletion (func, /* outside = */ true);
         markOutline (func);
         // scope for local declarations

         SimNamespace * local_scope = new SimNamespace;
         openScope (local_scope);
      }
   }

   void on_close_function (CmmSimpleDecl * simple_declaration) override
   {
      CmmSimpleItem * simple_item = simple_declaration->items [simple_declaration->items.size()-1];
      if (simple_item->is_function)
      {
         // function, not variable with initialization block
         SimFunction * func = simple_item->sim_basic->conv_SimFunction ();
         closeCompletion (func, /* outside = */ true);
         closeScope ();
         closeScope ();
      }
   }

   bool all_value_parameters (CmmFunctionDeclarator * func_spec)
   {
      bool result = false;
      if (func_spec->parameters->items.size () != 0)
      {
         result = true;
         for (CmmStat * param : func_spec->parameters->items)
         {
             if (param->kind != simpleStat)
             {
                result = false;
             }
         }
      }
      return result;
   }

   vector <CmmExpr*> value_parameters (CmmFunctionDeclarator * func_spec)
   {
      vector <CmmExpr*> result;
      for (CmmStat * param : func_spec->parameters->items)
      {
         if (param->kind == simpleStat)
         {
            result.push_back (param->conv_CmmSimpleStat()->inner_expr);
         }
      }
      return result;
   }

   void enter_parameters (CmmFunctionDeclarator * func_spec)
   {
      for (CmmStat * param : func_spec->parameters->items)
      {
         if (param->kind == simpleDecl)
         {
            CmmSimpleItem * simple_item = param->conv_CmmSimpleDecl()->items[0];
            clear_declarator (simple_item->declarator);
            copy_declarator_name (simple_item, simple_item->declarator);
            copy_declarator_location (simple_item, simple_item->declarator);

            SimVariable * var = new SimVariable;
            var->name = simple_item->item_name;
            var->cmm_decl = simple_item;
            simple_item->sim_basic = var;

            copy_basic_location (var, simple_item);
            enter (var);
            selectColor (var, "variableColors");
            markDefn (var);
         }
      }
   }

   void on_simple_statement (CmmSimpleStat * stat) override
   {
      copy_location (stat, stat->inner_expr);
   }

   void on_open_compound_statement (CmmCompoundStat * stat) override
   {
      // openLocalScope ();
   }

   void on_close_compound_statement (CmmCompoundStat * stat) override
   {
      // closeLocalScope ();
   }

   bool isScope (CmmDecl * obj)
   {
      return (obj != nullptr && (obj->kind == namespaceDecl || obj->kind == classDecl || obj->kind == enumDecl));
   }

   bool isTypeObject (CmmDecl * obj)
   {
      return (obj != nullptr && (obj->kind == typedefDecl || obj->kind == classDecl || obj->kind == enumDecl));
   }

   void on_base_name (CmmName * qual_name) override
   {
      // lookup declaration

      CmmDecl * decl = nullptr;
      SimBasic * basic = nullptr;
      if (qual_name->kind == globalName)
      {
         CmmGlobalName * global_name = qual_name->conv_CmmGlobalName ();
         basic = searchScope (global_scope, global_name->inner_name->conv_CmmSimpleName()->id);
      }
      else if (qual_name->kind == simpleName)
      {
         basic = lookup (qual_name->conv_CmmSimpleName()->id);
      }
      else if (qual_name->kind == compoundName)
      {
         CmmCompoundName * compound_name = qual_name->conv_CmmCompoundName();
         if (compound_name->right->kind == simpleName)
         {
            CmmDecl * left_decl = compound_name->left->item_decl;
            if (isScope (left_decl))
            {
               SimScope * basic_scope = left_decl->sim_basic->conv_SimScope ();
               if (basic_scope != nullptr)
               {
                  basic = searchScope (basic_scope, get_simple_name (compound_name));
                  if (basic != nullptr)
                     decl = basic->cmm_decl;
               }
            }
         }
      }
      /*
      else if (qual_name->kind == templateName)
      {
         decl = qual_name->conv_CmmTemplateName()->left->item_decl;
      }
      */

      if (basic != nullptr)
         decl = basic->cmm_decl;

      // class, enum or typedef => type
      if (isTypeObject (decl))
      {
         qual_name->type_flag = true;
      }

      // constructor or destructor on the beginning of member declaration
      if (remember_destructor)
      {
         if (decl == get_class ())
         {
            qual_name->destructor_flag = true;
         }
         remember_destructor = false;
         remember_constructor = false;
      }

      if (remember_constructor)
      {
         CmmClassDecl * cls = get_class ();
         if (decl == cls || (decl != nullptr && cls != nullptr && decl->item_qual == cls->item_qual))
         {
            qual_name->constructor_flag = true;
         }
         remember_constructor = false;
      }

      // store item_decl, name_owner
      qual_name->item_decl = decl;
   }

   bool is_expression (CmmExpr * expr) override
   {
      // type cannot be simple statement
      return ! expr->type_flag;
   }

   bool is_binary_expression (CmmExpr * expr) override
   {
      // type cannot be left operand in binary expression
      return ! expr->type_flag;
   }

   bool is_value_parameter (CmmExpr * expr) override
   {
      // parameters without type => value parameter
      return ! expr->type_flag;
   }

   #if 0
   void on_middle_bit_not_expr (CmmBitNotExpr * expr) override
   {
      if (remember_tilda)
      {
         if (isName ())
         {
            remember_destructor = true;
            remember_tilda = false;
         }
      }
   }
   #endif

   void on_bit_not_expr (CmmBitNotExpr * expr) override
   {
      expr->destructor_flag = expr->param->destructor_flag;
      expr->type_flag = expr->param->type_flag;
      expr->item_decl = expr->param->item_decl;
   }

   bool is_postfix_expression (CmmExpr * expr) override
   {
      bool result = true;
      if (expr->kind == subexprExpr)
      {
         // after expression in parenthesis, avoid functon call in (int) (double) v

         result = (!expr->type_flag);
      }
      else if (isSeparator ('('))
      {
         // class name at the beginning of member declaration => constructor

         result = ((!expr->constructor_flag) && (!expr->destructor_flag));
      }
      else if (isSeparator ('.'))
      {
         // class name . field name

         result = true;
      }
      else
      {
         result = (!expr->type_flag);
      }
      return result;
   }

   bool is_cast_expression (CmmExpr * expr) override
   {
      // is it type in parenthesis

      return (expr->kind == subexprExpr && expr->type_flag);
   }

   #if 0
   bool is_constructor (CmmExpr * expr) override
   {
      return (expr->constructor_flag || expr->destructor_flag);
   }
   #endif

   CmmClassDecl * get_class ()
   {
      return display.top()->cmm_decl->conv_CmmClassDecl();
   }

   void copy_basic_location (SimBasic * target, CmmBasic * source)
   {
      target->src_file = source->src_file;
      target->src_line = source->src_line;
      target->src_col = source->src_col;
   }

   void copy_location (CmmBasic * target, CmmBasic * source)
   {
      target->src_file = source->src_file;
      target->src_line = source->src_line;
      target->src_col = source->src_col;
      // target->src_pos = source->src_pos;
      // target->src_end = source->src_end;
   }

   void clear_declaration (CmmName * qual_name)
   {
      qual_name->item_decl = nullptr;
   }

   void copy_name_location (CmmDecl * target, CmmName * qual_name)
   {
      CmmName * source = qual_name;
      if (source != nullptr)
      {
         if (source->kind == compoundName)
         {
            source = source->conv_CmmCompoundName()->right;
         }
         copy_location (target, source);
      }
   }

   void copy_basic_name_location (SimBasic * target, CmmName * qual_name)
   {
      CmmName * source = qual_name;
      if (source != nullptr)
      {
         if (source->kind == compoundName)
         {
            source = source->conv_CmmCompoundName()->right;
         }
         copy_basic_location (target, source);
         #if 0
            source->item_ref = target;
         #endif
      }
   }

   string get_simple_name (CmmName * qual_name)
   {
      string result = "";
      if (qual_name->kind == simpleName)
      {
         result = qual_name->conv_CmmSimpleName()->id;
      }
      return result;
   }

   string get_short_name (CmmName * qual_name)
   {
      string result = "";
      if (qual_name == nullptr)
      {
      }
      else if (qual_name->kind == simpleName)
      {
         CmmSimpleName * simple = qual_name->conv_CmmSimpleName();
         if (simple != nullptr)
             result = simple->id;
      }
      else if (qual_name->kind == compoundName)
      {
         result = get_short_name (qual_name->conv_CmmCompoundName()->right);
      }
      else if (qual_name->kind == globalName)
      {
         result = get_short_name (qual_name->conv_CmmGlobalName()->inner_name);
      }
      else if (qual_name->kind == destructorName)
      {
         result = '~' + get_short_name (qual_name->conv_CmmDestructorName()->inner_name);
      }
      /*
      else if (qual_name->kind == templateName)
      {
         result = get_short_name (qual_name->conv_CmmTemplateName()->left);
      }
      */
      return result;
   }

   string get_name (CmmName * qual_name)
   {
      string result = "";
      if (qual_name->kind == simpleName)
      {
         result = qual_name->conv_CmmSimpleName()->id;
      }
      else if (qual_name->kind == compoundName)
      {
         CmmCompoundName * compound_name = qual_name->conv_CmmCompoundName();
         result = get_name (compound_name->left) + "::" + get_name (compound_name->right);
      }
      else if (qual_name->kind == globalName)
      {
         result = "::" + get_name (qual_name->conv_CmmGlobalName()->inner_name);
      }
      else if (qual_name->kind == destructorName)
      {
         result = '~' + get_name (qual_name->conv_CmmDestructorName()->inner_name);
      }
      /*
      else if (qual_name->kind == templateName)
      {
         result = get_name (qual_name->conv_CmmTemplateName()->left) + "< ??? >";
      }
      */
      if (result == "")
      {
         result = "???";
      }
      return result;
   }

   void on_type_name (CmmTypeName * expr) override
   {
      expr->type_flag = true;
      expr->sim_type = expr->qual_name->sim_type;
   }

   void on_const_specifier (CmmConstSpecifier * expr) override
   {
      expr->type_flag = true;
      #if 0
      expr->type = copy->copy (expr->param->sim_type);
      expr->type->type_const = true;
      #endif
   }

   void on_volatile_specifier (CmmVolatileSpecifier * expr) override
   {
      expr->type_flag = true;
   }

   void on_middle_decl_specifier (CmmDeclSpec * expr) override
   {
      remember_destructor = true;
   }

   void on_decl_specifier (CmmDeclSpec * expr) override
   {
      expr->type_flag = true;
      expr->constructor_flag = expr->param->constructor_flag;
      expr->destructor_flag = expr->param->destructor_flag;
   }

   void on_type_specifier (CmmTypeSpec * expr) override
   {
      expr->type_flag = true;
   }

   CmmDecl * get_type_decl (CmmName * obj)
   {
      CmmDecl * result = nullptr;
      CmmDecl * decl = obj->item_decl;
      if (decl != nullptr)
      {
         CmmKind kind = decl->kind;
         if (kind == classDecl || kind == enumDecl || kind == typedefDecl /* || dynamic_cast <Unknown * > (decl) */ )
         {
            result = decl;
         }
      }
      return result;
   }

   SimNamedType * get_named_type (CmmName * qual_name)
   {
      SimNamedType * result = new SimNamedType;
      result->type_name = get_name (qual_name);
      result->type_decl = get_type_decl (qual_name);
      return result;
   }

   SimType * get_type_spec (CmmTypeSpec * type_spec)
   {
      SimSimpleType * result = new SimSimpleType;

      if (type_spec->a_void)
      {
         result->type_void = true;
      }
      else if (type_spec->a_bool)
      {
         result->type_bool = true;
      }
      else if (type_spec->a_char)
      {
         result->type_char = true;
      }
      else if (type_spec->a_wchar)
      {
         result->type_wchar = true;
      }
      else if (type_spec->a_short)
      {
         result->type_short = true;
      }
      else if (type_spec->a_int)
      {
         result->type_int = true;
      }
      else if (type_spec->a_long)
      {
         result->type_long = true;
      }
      else if (type_spec->a_float)
      {
         result->type_float = true;
      }
      else if (type_spec->a_double)
      {
         result->type_double = true;
      }

      if (type_spec->a_signed)
      {
         result->type_signed = true;
      }
      else if (type_spec->a_unsigned)
      {
         result->type_unsigned = true;
      }

      return result;
   }

   SimType * get_declarator_type (SimType * init_type, CmmDeclarator * declarator)
   {
      SimType * result = init_type;
      while (declarator != nullptr)
      {
         SimType * last = result;
         CmmCvSpecifier * cv_spec = nullptr;
         if (declarator->kind == pointerDeclarator)
         {
            SimPointerType * compound = new SimPointerType;
            compound->type_from = last;
            result = compound;
            cv_spec = declarator->conv_CmmPointerDeclarator()->cv_spec;
         }
         if (declarator->kind == referenceDeclarator)
         {
            SimReferenceType * compound = new SimReferenceType;
            compound->type_from = last;
            result = compound;
            cv_spec = declarator->conv_CmmReferenceDeclarator()->cv_spec;
         }

         if (cv_spec != nullptr && cv_spec->cv_const)
         {
            result->type_const = true;
         }
         if (cv_spec != nullptr && cv_spec->cv_volatile)
         {
            result->type_volatile = true;
         }

         if (declarator->kind != emptyDeclarator)
         {
            last = result;
            if (declarator->kind == arrayDeclarator)
            {
               SimArrayType * compound = new SimArrayType;
               compound->type_from = last;
               compound->type_lim = declarator->conv_CmmArrayDeclarator()->lim;
               result = compound;
            }
            if (declarator->kind == functionDeclarator)
            {
               if (! all_value_parameters (declarator->conv_CmmFunctionDeclarator()))
               {
                  SimFunctionType * compound = new SimFunctionType;
                  compound->type_from = last;
                  result = compound;
               }
            }
         }

         declarator = get_inner_declarator (declarator);
      }
      return result;
   }

   void on_ident_expr (CmmName * expr) override
   {
      {
         CmmDecl * decl = expr->item_decl;
         if (decl != nullptr && decl->sim_basic != nullptr)
            markUsage (decl->sim_basic, decl);
      }

      if (expr->kind == simpleName)
      {
         // cout << "IDENT_EXPR " << get_simple_name (expr) << endl;
      }
      if (get_type_decl (expr) != nullptr)
      {
         expr->sim_type = get_named_type (expr);
      }
      if (expr->item_decl != nullptr && expr->item_decl->sim_type != nullptr)
      {
         expr->sim_type = expr->item_decl->sim_type;
      }
   }

   void on_field_expr (CmmFieldExpr * expr) override
   {
      copy_location (expr, expr->left);
      CmmExpr * left = expr->left;
      string right = expr->field_name;
   }

   void on_ptr_field_expr (CmmPtrFieldExpr * expr) override
   {
      copy_location (expr, expr->left);
      CmmExpr * left = expr->left;
      string right = expr->field_name;
   }

   void on_post_inc_expr (CmmPostIncExpr * expr) override
   {
      copy_location (expr, expr->left);
      expr->sim_type = expr->left->sim_type;
   }

   void on_post_dec_expr (CmmPostDecExpr * expr) override
   {
      copy_location (expr, expr->left);
      expr->sim_type = expr->left->sim_type;
   }

   void on_int_value (CmmIntValue * expr) override
   {
      expr->sim_type = intType;
      string text = expr->value;
   }

   void on_real_value (CmmRealValue * expr) override
   {
      expr->sim_type = doubleType;
   }

   void on_char_value (CmmCharValue * expr) override
   {
      expr->sim_type = charType;
   }

   void on_string_value (CmmStringValue * expr) override
   {
      expr->sim_type = stringType;
      string text = expr->value;
      #if 0
      for (CmmStringCont * item : expr->items)
      {
         text = text + item->value;
      }
      #endif
   }

   void on_this_expr (CmmThisExpr * expr) override
   {
   }

   void on_subexpr_expr (CmmSubexprExpr * expr) override
   {
      expr->sim_type = expr->param->sim_type;
      expr->type_flag = expr->param->type_flag;
   }

   void on_index_expr (CmmIndexExpr * expr) override
   {
      copy_location (expr, expr->left);
   }

   void on_call_expr (CmmCallExpr * expr) override
   {
      copy_location (expr, expr->left);
   }

   void on_modern_cast_expr (CmmModernCastExpr * expr) override
   {
   }

   void on_typeid_expr (CmmTypeIdExpr * expr) override
   {
   }

   void on_inc_expr (CmmIncExpr * expr) override
   {
   }

   void on_dec_expr (CmmDecExpr * expr) override
   {
   }

   void on_deref_expr (CmmDerefExpr * expr) override
   {
   }

   void on_addr_expr (CmmAddrExpr * expr) override
   {
   }

   void on_plus_expr (CmmPlusExpr * expr) override
   {
   }

   void on_minus_expr (CmmMinusExpr * expr) override
   {
   }

   void on_log_not_expr (CmmLogNotExpr * expr) override
   {
   }

   void on_sizeof_expr (CmmSizeofExpr * expr) override
   {
   }

   void on_allocation_expr (CmmNewExpr * expr) override
   {
   }

   void on_deallocation_expr (CmmDeleteExpr * expr) override
   {
   }

   void on_throw_expr (CmmThrowExpr * expr) override
   {
   }

   void on_binary_expr (CmmExpr * e) override
   {
      // CmmBinaryExpr * expr = dynamic_cast < CmmBinaryExpr * > (e);
      CmmBinaryExpr * expr = e->conv_CmmBinaryExpr ();
      if (expr != nullptr)
      {
         copy_location (expr, expr->left);
         CmmKind kind = expr->kind;

         if (kind == mulExpr || kind == divExpr || kind == modExpr ||
             kind == addExpr || kind == subExpr ||
             kind == shlExpr || kind == shrExpr ||
             kind == bitAndExpr || kind == bitOrExpr || kind == bitXorExpr)
         {
            // result type

            int a = type_number (expr->left);
            int b = type_number (expr->right);
            int inx = a;
            if (b > inx)
            {
               inx = b;
            }
            if (inx == doubleInx)
            {
               expr->sim_type = doubleType;
            }
            if (inx == floatInx)
            {
               expr->sim_type = floatType;
            }
            if (inx == unsignedLongInx)
            {
               expr->sim_type = unsignedLongType;
            }
            if (inx == longInx)
            {
               expr->sim_type = longType;
            }
            if (inx == unsignedIntInx)
            {
               expr->sim_type = unsignedIntType;
            }
            if (inx == intInx)
            {
               expr->sim_type = intType;
            }
         }
         else if (kind == ltExpr || kind == leExpr ||
                  kind == gtExpr || kind == geExpr ||
                  kind == eqExpr || kind == neExpr ||
                  kind == logAndExpr || kind ==  logOrExpr)
         {
            expr->sim_type = boolType;
         }
         else if (kind == assignExpr ||
                  kind == addAssignExpr || kind == subAssignExpr ||
                  kind == mulAssignExpr || kind == divAssignExpr || kind == modAssignExpr ||
                  kind == shlAssignExpr || kind == shrAssignExpr ||
                  kind == andAssignExpr || kind == orAssignExpr || kind == xorAssignExpr )
         {
         }
      }
   }

   int type_number (CmmExpr * expr)
   {
      int inx = unknownInx;
      if (expr->sim_type != nullptr)
      {
         SimSimpleType * type = expr->sim_type->conv_SimSimpleType ();
         if (type != nullptr)
         {
            if (type->type_double)
            {
               inx = doubleInx;
            }
            else if (type->type_float)
            {
               inx = floatInx;
            }
            else if (type->type_long)
            {
               if (type->type_unsigned)
               {
                  inx = unsignedLongInx;
               }
               else
               {
                  inx = longInx;
               }
            }
            else if (type->type_int || type->type_short || type->type_wchar || type->type_char || type->type_bool)
            {
               if (type->type_unsigned)
               {
                  inx = unsignedIntInx;
               }
               else
               {
                  inx = intInx;
               }
            }
         }
      }
      return inx;
   }
};

/* --------------------------------------------------------------------- */

SimScope * compile_program (string fileName)
{
   SimScope * result = nullptr;
   try
   {
      MiniCompiler comp;
      comp.open (fileName);

      #if 0
      while (! comp.isEof ())
      {
          comp.info ("token = " + comp.tokenToString (comp.token));
          comp.nextToken ();
      }
      #endif

      result = comp.compile_program ();
      comp.close ();
   }
   catch (LexerException)
   {
   }
   return result;
}
