
#include "precompiled.h"

#include "mini-instr.h"

#include "llvm/AsmParser/Parser.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/Verifier.h"
#include "llvm/ExecutionEngine/GenericValue.h"

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Target/TargetMachine.h"

#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/Orc/JITTargetMachineBuilder.h"
#include "llvm/ADT/PointerIntPair.h"

// #include "llvm/MC/TargetRegistry.h"
#include "llvm/Support/TargetRegistry.h"

#include "llvm/Support/CodeGen.h"
#include "llvm/Support/TargetSelect.h"
// #include "llvm/TargetParser/Host.h"
#include "llvm/Support/SmallVectorMemoryBuffer.h"

#include "llvm/Transforms/IPO/PassManagerBuilder.h"

#include "mini-parser.hpp"
#include "code.h"

#include <optional> // std::nullopt

#include <sys/stat.h> // mkdir
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

/* ---------------------------------------------------------------------- */

void print_module (llvm::Module * module)
{
    std::string code;
    llvm::raw_string_ostream stream (code);
    stream << * module;
    stream.flush ();
    cout << code << endl;
}

/* ---------------------------------------------------------------------- */

// http://stackoverflow.com/questions/34822212/generate-machine-code-directly-via-llvm-api
// http://stackoverflow.com/questions/64286153/set-llvm-assembly-output-syntax-with-c-api

void print_asm (llvm::Module *M)
{
#if 1
    llvm::InitializeNativeTarget ();
    llvm::InitializeNativeTargetAsmPrinter ();

    auto TargetTriple = llvm::sys::getDefaultTargetTriple();
    M->setTargetTriple(TargetTriple);

    std::string Error;
    const llvm::Target *target = llvm::TargetRegistry::lookupTarget(TargetTriple, Error);
    auto cpu = llvm::sys::getHostCPUName();
    llvm::SubtargetFeatures Features;
    llvm::StringMap<bool> HostFeatures;
    if (llvm::sys::getHostCPUFeatures(HostFeatures))
        for (auto &F : HostFeatures)
            Features.AddFeature(F.first(), F.second);
    auto features = Features.getString();

    llvm::TargetOptions Options;
    std::unique_ptr<llvm::TargetMachine> TM {
       target->createTargetMachine(
               TargetTriple,
               cpu,
               features,
               Options,
               llvm::Reloc::PIC_)
               // llvm::None, /* std::nullopt,*/
               // llvm::CodeGenOpt::None)
    };

    // llvm::PassManagerBuilder PMB;
    // PMB.OptLevel = 2;
    // auto PM = llvm::ModulePassManager ();
    llvm::legacy::PassManager PM;
    // PMB.populateModulePassManager (PM);

    M->setDataLayout(TM->createDataLayout());
    TM->addPassesToEmitFile (PM,
                             (llvm::raw_pwrite_stream &) llvm::outs(),
                             (llvm::raw_pwrite_stream *) (&llvm::outs()),
                             llvm::CodeGenFileType::CGFT_AssemblyFile,
                             true,
                             nullptr);
    PM.run(*M);
#endif
}

/* ---------------------------------------------------------------------- */

// http://stackoverflow.com/questions/48775661/llvm-how-to-construct-llvmvalue-from-a-stdstring
// http://stackoverflow.com/questions/67451800/using-jit-in-llvm-executionengine-create-error-c

void example1 ()
{
    cout << "example1" << endl;

    // llvm::Initialize ();
    llvm::InitializeNativeTarget ();
    llvm::InitializeNativeTargetAsmPrinter ();

    // target triple = "unknown-unknown-unknown"
    // target datalayout = ""
    string code = R"...(
        define double @"fpadd"(double %".1", double %".2")
        {
              entry:
              %"res" = fadd double %".1", %".2"
              ret double %"res"
        }
    )...";

    llvm::LLVMContext ctx;
    llvm::SMDiagnostic diag;
    std::unique_ptr <llvm::Module> module = parseAssemblyString (code, diag, ctx);

    // cout << "Point 1" << endl;
    cout << diag.getMessage().str() << endl;

    // cout << "Point 2" << endl;
    std::string err;
    llvm::EngineBuilder builder (std::move (module));
    // builder.setEngineKind(llvm::EngineKind::JIT).setErrorStr(&err);
    llvm::ExecutionEngine* engine = builder.create();
    // cout << "Point 3" << endl;
    // engine->finalizeObject ();
    // engine->runStaticConstructorsDestructors (false);

    uint64_t func_addr = engine->getFunctionAddress ("fpadd");
    typedef double (* func_type) (double, double);
    func_type func;
    func = (func_type) func_addr;

    // cout << "Point 3" << endl;
    double result = func (1.0, 3.5);
    cout << "RESULT fpadd (...) = " << result << endl;
}

// http://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl03.html
// http://mukulrathi.com/create-your-own-programming-language/llvm-ir-cpp-api-tutorial/
// http://eli.thegreenplace.net/2017/adventures-in-jit-compilation-part-3-llvm/
// http://stackoverflow.com/questions/33960249/how-to-store-the-generated-llvm-ir-code-of-a-llvmmodule-to-a-string

// http://gist.github.com/pavanky/e1ce654b1a0d1abd4727a47cb1645093

/* ---------------------------------------------------------------------- */

void example2 ()
{
    cout << "example2" << endl;

    llvm::LLVMContext ctx;
    llvm::IRBuilder builder (ctx);
    std::unique_ptr <llvm::Module> module = std::make_unique <llvm::Module> ("MyModule", ctx);

    llvm::Type * int_type = builder.getInt32Ty ();
    llvm::Type * result_type = int_type;
    llvm::Type * parameter_types [2] = { int_type, int_type };
    llvm::ArrayRef < llvm::Type * > parameters (parameter_types, 2);

    llvm::FunctionType * func_type = llvm::FunctionType::get (result_type, parameters, /* IsVarArgs = */ false); // (llvm::IntType (32), vector <> {llvm::IntType (32), llvm::IntType (32)});
    llvm::Function * func = llvm::Function::Create (func_type, llvm::Function::ExternalLinkage, "fce", *module);

    llvm::BasicBlock * bb_entry = llvm::BasicBlock::Create(ctx, "bb_entry", func);
    builder.SetInsertPoint (bb_entry);

    // auto stackint = builder.alloca (llvm::IntType (32));
    // builder.store (llvm::Constant (stackint->type->pointee, 123), stackint);
    // myint = builder.load (stackint);
    auto addinstr = builder.CreateAdd (func->getArg(0), func->getArg(1));
    // auto mulinstr = builder.CreateMul (addinstr, llvm::ConstantInt::get (int_type, 123));
    // auto pred = builder.CreateICmpSLT (addinstr, mulinstr);
    builder.CreateRet (addinstr);

    /*
    auto bb_block =  llvm::BasicBlock::Create(ctx, "bb_block", func);
    builder.SetInsertPoint (bb_block);

    auto bb_exit = llvm::BasicBlock::Create(ctx, "bb_exit", func);
    auto pred = builder.trunc (addinstr, llvm::IntType (1));

    builder.Createcbranch (pred, bb_block, bb_exit);
    builder.SetInsertPoint (bb_exit);
    builder.CreateRet (myint);
    */

    std::string code;
    llvm::raw_string_ostream stream (code);
    stream << *module;
    stream.flush ();
    cout << code << endl;

    #if 1
    {
       llvm::InitializeNativeTarget ();
       llvm::InitializeNativeTargetAsmPrinter ();

       llvm::EngineBuilder builder (std::move (module));
       std::string err;
       builder.setEngineKind(llvm::EngineKind::JIT).setErrorStr(&err);
       llvm::ExecutionEngine * engine = builder.create();
       cout << "Point 2 " << err << endl;
       engine->finalizeObject ();
       engine->runStaticConstructorsDestructors (false);

       uint64_t func_addr = engine->getFunctionAddress ("fce");
       typedef int (* func_type) (int, int);
       func_type func;
       func = (func_type) func_addr;

       // cout << "Point 3" << endl;
       int result = func (10, 20);
       cout << "RESULT fce (...) = " << result << endl;
    }
    #else
    {
        llvm::InitializeNativeTarget ();
        llvm::InitializeNativeTargetAsmPrinter ();

        cout << "Point 1 " << func->getNumOperands() << endl;
        llvm::EngineBuilder builder (std::move (module));
        llvm::ExecutionEngine* engine = builder.create();
        engine->finalizeObject ();
        engine->runStaticConstructorsDestructors (false);

        uint64_t func_addr = engine->getFunctionAddress ("fce");
        cout << "Point 2 " << func_addr << endl;
        llvm::Function * func_ref  = engine->FindFunctionNamed ("fce");
        cout << "Point 3 " << func_ref << " " << func_ref->getNumOperands()<<  endl;
        if (func_ref != nullptr)
        {
           llvm::GenericValue a;
           a.IntVal = 10;
           a.UIntPairVal.first = 10;
           a.UIntPairVal.second = 10;

           llvm::GenericValue b;
           // b.IntVal = 20;

           llvm::GenericValue args_array [2] = { a, b };
           llvm::ArrayRef <llvm::GenericValue> args (args_array, 2);

           llvm::GenericValue result = engine->runFunction (func_ref, args);
           cout << "RESULT fce (...) = " << result.IntVal.getLimitedValue () << endl;
        }
    }
    #endif
}

/* ---------------------------------------------------------------------- */

void example3 ()
{
    cout << "example3" << endl;

    llvm::InitializeNativeTarget ();
    llvm::InitializeNativeTargetAsmPrinter ();
    auto t1 = time (nullptr);

    llvm::LLVMContext ctx;
    llvm::IRBuilder < > builder (ctx);
    // llvm::Module * module = new llvm::Module ("MyModule", ctx);
    std::unique_ptr <llvm::Module> module = std::make_unique <llvm::Module> ("MyModule", ctx);

    // llvm::FunctionType * func_type = llvm::FunctionType::get (result_type, parameters, /* IsVarArgs = */ false); // (llvm::IntType (32), vector <> {llvm::IntType (32), llvm::IntType (32)});
    // llvm::Function * func = llvm::Function::Create (func_type, llvm::Function::ExternalLinkage, "fce", *module);

    llvm::IntegerType * int_type = llvm::Type::getInt32Ty (ctx);
    llvm::PointerType * int_ptr_type = int_type->getPointerTo ();
    typedef llvm::ArrayRef <llvm::Type *>  param_array;
    param_array param_types { int_ptr_type, int_type };
    llvm::FunctionType * func_type = llvm::FunctionType::get (int_type, param_types, false);
    // llvm::FunctionType * func_type = llvm::FunctionType::get (int_type, param_array { int_type, int_type }, false);
    llvm::Function * func = llvm::Function::Create (func_type, llvm::Function::ExternalLinkage, "sum", &*module);

    llvm::BasicBlock * bb_entry = llvm::BasicBlock::Create (ctx, "bb_entry", func);
    llvm::BasicBlock * bb_loop  = llvm::BasicBlock::Create (ctx, "bb_loop", func);
    llvm::BasicBlock * bb_leave = llvm::BasicBlock::Create (ctx, "bb_leave", func);

    builder.SetInsertPoint (bb_entry);
    builder.CreateBr (bb_loop);

    builder.SetInsertPoint (bb_loop);
    llvm::PHINode * index = builder.CreatePHI (int_type, 1, "index");
    index->addIncoming (llvm::ConstantInt::get (int_type, 0), bb_entry);

    llvm::PHINode * accum = builder.CreatePHI (int_type, 1, "accum");
    accum->addIncoming (llvm::ConstantInt::get (int_type, 0), bb_entry);

    typedef llvm::ArrayRef <llvm::Value *> value_array;
    auto ptr = builder.CreateGEP (int_type, func->getArg (0), value_array {index});
    auto value = builder.CreateLoad (int_type, ptr);
    auto added = builder.CreateAdd (accum, value);
    accum->addIncoming (added, bb_loop);
    auto indexp1 = builder.CreateAdd (index, llvm::ConstantInt::get (int_type, 1));
    index->addIncoming (indexp1, bb_loop);
    auto cond = builder.CreateICmpSLT (indexp1, func->getArg (1));
    builder.CreateCondBr (cond, bb_loop, bb_leave);

    builder.SetInsertPoint (bb_leave);
    builder.CreateRet (added);

    auto t2 = time (nullptr);
    std::string code;
    llvm::raw_string_ostream stream (code);
    stream << *module;
    stream.flush ();
    cout << code << endl;
    cout << "-- generate IR: " << t2 - t1 << endl;

    auto t3 = time (nullptr);
    llvm::SMDiagnostic diag;
    auto llmod = llvm::parseAssemblyString (code, diag, ctx);
    auto t4 = time (nullptr);
    cout << "-- parse assembly: " << t4 - t3 << endl;

    print_module (&*llmod);

    #if 0
    auto pmb = llvm::PassManagerBuilder ();
    pmb.OptLevel = 2;
    auto pm = llvm::ModulePassManager ();
    pmb.populate (pm);

    auto t5 = time (nullptr);
    pm->run (llmod);
    auto t6 = time (nullptr);
    cout << "-- optimize: " << t6 - t5 << endl;

    auto t7 = time (nullptr);
    auto target_machine = llvm::Target->from_default_triple ()->create_target_machine ();
    llvm::create_mcjit_compiler (llmod, target_machine); //ee
    ee->finalize_object ();
    cfptr = ee->get_function_address ("sum");
    auto t8 = time (nullptr);
    cout << "-- JIT compile: " << t8 - t7 << endl;

    print (target_machine->emit_assembly (llmod));
    cfunc = ctypes->CFUNCTYPE (ctypes->c_int, ctypes->POINTER (ctypes->c_int), ctypes->c_int) (cfptr);
    auto A = np->arange (10, dtype=np->int32);
    auto res = cfunc (A->ctypes->data_as (ctypes->POINTER (ctypes->c_int)), A->size);
    cout << res << " " << A->sum () << endl;;
    #endif

    {
       llvm::InitializeNativeTarget ();
       llvm::InitializeNativeTargetAsmPrinter ();

       llvm::EngineBuilder builder (std::move (module));
       std::string err;
       builder.setEngineKind(llvm::EngineKind::JIT).setErrorStr(&err);
       llvm::ExecutionEngine * engine = builder.create ();
       cout << "Point 2 " << err << endl;
       engine->finalizeObject ();
       engine->runStaticConstructorsDestructors (false);

       uint64_t func_addr = engine->getFunctionAddress ("sum");
       typedef int (* func_type) (int *, int);
       func_type func = (func_type) func_addr;

       cout << "Point 3 "<< (void*) func_addr << endl;
       int data [3] = { 10, 20, 30 };
       int result = func (data, 3);
       cout << "RESULT fce (...) = " << result << endl;
    }
}

/* ---------------------------------------------------------------------- */

void example4 ()
{
    cout << "example4" << endl;
    using namespace llvm;
    // http://gist.github.com/seven1m/2ca74265cca9ef6f493ef1de87e9252d

    auto context = std::make_unique<LLVMContext>();
    IRBuilder<> builder(*context);

    auto module = std::make_unique<Module>("hello", *context);

    // build a 'main' function
    auto i32 = builder.getInt32Ty();
    auto prototype = llvm::FunctionType::get(i32, false);
    llvm::Function *main_fn = llvm::Function::Create(prototype, llvm::Function::ExternalLinkage, "main", module.get());
    BasicBlock *body = BasicBlock::Create(*context, "body", main_fn);
    builder.SetInsertPoint(body);

    // use libc's printf function
    auto i8p = builder.getInt8PtrTy();
    auto printf_prototype = llvm::FunctionType::get(i8p, true);
    auto printf_fn = llvm::Function::Create(printf_prototype, llvm::Function::ExternalLinkage, "printf", module.get());

    // call printf with our string
    auto format_str = builder.CreateGlobalStringPtr("hello world\n");
    builder.CreateCall(printf_fn, { format_str });

    // return 1 from main
    auto ret = ConstantInt::get(i32, 1);
    builder.CreateRet(ret);

    // if you want to print the LLVM IR:
    module->print(llvm::outs(), nullptr);

    // execute it!
    ExecutionEngine *executionEngine = EngineBuilder(std::move(module)).setEngineKind(llvm::EngineKind::Interpreter).create();
    llvm::Function *main = executionEngine->FindFunctionNamed(StringRef("main"));
    auto result = executionEngine->runFunction(main, {});

    // return the result
    cout << "RESULT = " << result.IntVal.getLimitedValue () << endl;
}

/* ---------------------------------------------------------------------- */

#if 0
llvm::LLVMContext ctx;
llvm::IRBuilder < > builder (ctx);
std::unique_ptr <llvm::Module> module (new llvm::Module ("my_module", ctx));
// llvm::Module * module = new llvm::Module ("my_module", ctx);
#endif

class InstructionsModule
{
public:
   llvm::LLVMContext ctx;
   llvm::IRBuilder < > builder;
   std::unique_ptr <llvm::Module> module;

   InstructionsModule () :
      builder (ctx),
      module (new llvm::Module ("my_module", ctx))
   { }

    llvm::Type * code_type (SimType * t0)
    {
        llvm::Type * result = nullptr;
        if (t0 != nullptr)
        {
           if (SimSimpleType * t = t0->conv_SimSimpleType ())
           {
               if (t->type_int)
               {
                   result = builder.getInt32Ty ();
               }
               if (t->type_void)
               {
                   cout << "VOID" << endl;
                   result = builder.getVoidTy ();
               }
           }
           if (SimArrayType * t = t0->conv_SimArrayType ())
           {
               #if 0
               int cnt = t->lim->value;
               if (cnt != nullptr)
               {
                   llvm::Type * elem = code_type (t->type_from);
                   result = llvm::VectorType::get (elem, int (cnt));
               }
               #endif
           }
           #if 0
           if (PointerType * t = dynamic_cast <PointerType * > (t0))
           {
               result = code_type (t->type_from)->as_pointer ();
           }
           if (ReferenceType * t = dynamic_cast <ReferenceType * > (t0))
           {
               result = code_type (t->type_from)->as_pointer ();
           }
           #endif
        }
        if (result == nullptr)
        {
           result = builder.getInt32Ty (); // !?
        }
        return result;
    }

    // llvm::Value * first_variable = nullptr;

    void code_global_variable (CmmSimpleItem * decl)
    {
        llvm::Type * t = code_type (decl->sim_type);
        decl->code_value =
           new llvm::GlobalVariable
               (*module,
                t,
                /* isConstant */ false,
                llvm::GlobalValue::CommonLinkage,
                /* Initializer */ nullptr,
                decl->item_name);

        // if (first_variable == nullptr)
        //   first_variable = decl->code_value;
    }

    void code_local_variable (CmmSimpleItem * decl)
    {
        llvm::Type * t = code_type (decl->sim_type);
        decl->code = builder.CreateAlloca (t, 0, nullptr, decl->item_name);
        decl->local_variable = true; // !?
    }

    llvm::Function * func = nullptr;

    void code_function (CmmSimpleItem * decl)
    {
        SimType * t = decl->sim_type;
        SimFunctionType * f = t->conv_SimFunctionType ();

        llvm::Type * result_type;
        if (f == nullptr)
        {
            result_type = builder.getVoidTy ();
        }
        else
        {
            result_type = code_type (f->type_from);
        }

        CmmTypeSpec * type_spec = decl->simple_decl->type_spec->conv_CmmTypeSpec ();
        if (type_spec != nullptr && type_spec->a_void)
            result_type = builder.getVoidTy ();

        CmmStatSect * parameters = nullptr;
        if (decl->func_spec != nullptr)
            parameters = decl->func_spec->parameters;

        vector <llvm::Type *> param_types;
        #if 0
        for (SimType * p : f->parameter_types)
            param_types.push_back (code_type (p));
        #endif
        if (parameters != nullptr)
            for (CmmStat * param : parameters->items)
            {
                param_types.push_back (code_type (param->sim_type));
            }

        llvm::FunctionType * func_type = llvm::FunctionType::get (result_type, param_types, false);
        /* llvm::Function * */ func = llvm::Function::Create (func_type, llvm::Function::ExternalLinkage, 0, decl->item_name, &*module);

        decl->code_value = func;
        #if 1
        if (parameters != nullptr)
        {
           int inx = 0;
           for (CmmStat * param : parameters->items)
           {
               if (param->kind == simpleDecl)
               {
                  CmmSimpleItem * simple_item = param->conv_CmmSimpleDecl()->items[0];
                  simple_item->code_value = func->getArg (inx);
                  func->getArg(inx)->setName (simple_item->item_name);
               }
               inx = inx + 1;
           }
        }
        #endif


        llvm::BasicBlock * entry_block = llvm::BasicBlock::Create (ctx, "bb_entry", func);
        builder.SetInsertPoint (entry_block);

        // llvm::Value * value = llvm::ConstantInt::get (builder.getInt32Ty (), 42);
        // builder.CreateLoad (builder.getInt32Ty (), value, "something");

        code_stat (decl->simple_decl->body);

        builder.CreateRetVoid ();
    }


    void code_if_stat (CmmIfStat * stat)
    {
        code_expr (stat->cond);
        llvm::Value * cond = stat->cond->code_value;

        llvm::BasicBlock * then_block = llvm::BasicBlock::Create (ctx, "then_block", func);
        llvm::BasicBlock * else_block = nullptr;
        if (stat->else_stat != nullptr)
            else_block = llvm::BasicBlock::Create (ctx, "else_block", func);
        llvm::BasicBlock * stop_block = llvm::BasicBlock::Create (ctx, "stop_block", func);

        if (stat->else_stat == nullptr)
        {
            builder.CreateCondBr (cond, then_block, stop_block);
        }
        else
        {
            builder.CreateCondBr (cond, then_block, else_block);
        }

        builder.SetInsertPoint (then_block);
        code_stat (stat->then_stat);
        builder.CreateBr (stop_block);

        if (stat->else_stat == nullptr)
        {
            builder.CreateBr (stop_block);
        }
        else
        {
            builder.SetInsertPoint (else_block);
            code_stat (stat->else_stat);
            builder.CreateBr (stop_block);
        }
        builder.SetInsertPoint (stop_block);
    }

    void code_while_stat (CmmWhileStat * stat)
    {
        llvm::BasicBlock * start_block = llvm::BasicBlock::Create (ctx, "start_block", func);
        llvm::BasicBlock * cont_block = llvm::BasicBlock::Create (ctx, "cont_block", func);
        llvm::BasicBlock * leave_block = llvm::BasicBlock::Create (ctx, "leave_block", func);

        builder.SetInsertPoint (start_block);
        code_expr (stat->cond);
        builder.CreateCondBr (stat->cond->code_value, cont_block, leave_block);
        builder.SetInsertPoint (cont_block);
        code_stat (stat->body);
        builder.CreateBr (start_block);
        builder.SetInsertPoint (leave_block);
    }

    void code_for_stat (CmmForStat * stat)
    {
        code_expr (stat->from_expr);
        if (stat->iter_expr != nullptr)
        {
            code_expr (stat->iter_expr);
            code_stat (stat->body);
        }
        else
        {
            llvm::BasicBlock * start_block = llvm::BasicBlock::Create (ctx, "start_block", func);
            llvm::BasicBlock * cont_block = llvm::BasicBlock::Create (ctx, "cont_block", func);
            llvm::BasicBlock * stop_block = llvm::BasicBlock::Create (ctx, "stop_block", func);
            builder.SetInsertPoint (start_block);
            code_expr (stat->cond_expr);
            builder.CreateCondBr (stat->cond_expr->code_value, cont_block, stop_block);
            builder.SetInsertPoint (cont_block);
            code_stat (stat->body);
            code_expr (stat->step_expr);
            builder.CreateBr (start_block);
            builder.SetInsertPoint (stop_block);
        }
    }

    void code_and_or (CmmBinaryExpr * expr)
    {
        CmmExpr * left = expr->left;
        CmmExpr * right = expr->right;
        code_expr (left);

        llvm::BasicBlock * cont_block = llvm::BasicBlock::Create (ctx, "cont_block", func);
        llvm::BasicBlock * stop_block = llvm::BasicBlock::Create (ctx, "stop_block", func);

        builder.SetInsertPoint (cont_block);
        if (expr->kind == logAndExpr)
            builder.CreateCondBr (left->code_value, cont_block, stop_block);
        if (expr->kind == logOrExpr)
            builder.CreateCondBr (left->code_value, stop_block, cont_block);
        builder.SetInsertPoint (cont_block);
        code_expr (right);
        builder.SetInsertPoint (stop_block);
    }

    void code_stat (CmmStat * stat)
    {
        if (stat->kind == ifStat)
        {
            code_if_stat (stat->conv_CmmIfStat ());
        }
        else if (stat->kind == whileStat)
        {
            code_while_stat (stat->conv_CmmWhileStat ());
        }
        else if (stat->kind == forStat)
        {
            code_for_stat (stat->conv_CmmForStat ());
        }
        else if (stat->kind == compoundStat)
        {
            CmmStatSect * body = stat->conv_CmmCompoundStat ()->body;
            if (body != nullptr)
               for (CmmStat * item : body->items)
                   code_stat (item);
        }
        else if (stat->kind == simpleStat)
        {
            CmmExpr * expr = stat->conv_CmmSimpleStat ()->inner_expr;
            code_expr (expr);
            // stat->code_value = llvm::ConstantInt::get (builder.getInt32Ty (), 42); // expr->code;
        }
        else if (stat->kind == simpleDecl)
        {
            for (CmmStat * item : stat->conv_CmmSimpleDecl ()->items)
                if (CmmSimpleItem  * simple_item = item->conv_CmmSimpleItem ())
                   code_local_variable (simple_item);
        }
    }

    llvm::Instruction * expr_instr (CmmExpr * expr)
    {
        return builder.CreateLoad (expr->code_value);
    }

    void code_assign (CmmBinaryExpr * expr)
    {
        CmmExpr * left = expr->left;
        CmmExpr * right = expr->right;
        code_expr (right);
        if (left->kind != indexExpr)
        {
            code_ref (left);
            cout << "STORE " << left->code_value << ", " << right->code_value << endl;
            // llvm::Instruction * instr = expr_instr (left);
            builder.CreateStore (right->code_value, left->code_value);
        }
        else
        {
            #if 0
            array = left->left;
            index = left->param;
            code_ref (array);
            temp = builder.load (array->code);
            code_expr (index);
            expr->code = builder.insert_element (temp, right->code, index->code);
            #endif
        }
    }

    void code_ref (CmmExpr * expr)
    {
        expr->code_value = nullptr;
        CmmDecl * decl = expr->item_decl;
        if (decl == nullptr)
        {
           cout << "STORE TO NULL DECL" << endl;
           expr->code_value = builder.CreateAlloca (builder.getInt32Ty (), 0, nullptr, "unknown_target");
        }
        else if (decl->code_value != nullptr)
        {
           cout << "STORE TO VALID CODE_VALUE" << endl;
           expr->code_value = decl->code_value;
        }
        else if (decl->code != nullptr)
        {
           cout << "STORE TO LOADED DECL CODE" << endl;
           if (decl->local_variable)
              expr->code_value = decl->code; // !?
          else
              expr->code_value = builder.CreateLoad (decl->code);
        }
        else
        {
           cout << "STORE TO DECL WITHOUT CODE and CODE_VALUE" << endl;
           expr->code_value = builder.CreateAlloca (builder.getInt32Ty (), 0, nullptr, "unknown_target");
        }
        /*
        else
        {
            cout << "STORE TO SIMPLE NAME " << expr->conv_CmmSimpleName ()->id << " " << decl->code_value << endl;
            expr->code_value = decl->code_value;
        }
        */
    }

    void code_expr (CmmExpr * expr)
    {
        if (expr == nullptr) return;
        expr->code_value = nullptr;
        CmmKind kind = expr->kind;
        #if 0
        if (kind == simpleStat)
        {
            code_stat (expr);
            expr->code = expr->inner_expr->code;
        }
        else
        #endif
        if (kind == simpleName)
        {
            CmmDecl * decl = expr->item_decl;
            expr->code_value = decl->code_value;
            cout << "SIMPLE NAME " << expr->conv_CmmSimpleName ()->id << " " << decl->code_value << endl;

            if (decl->local_variable)
                expr->code_value = builder.CreateLoad (decl->code);

            if (expr->code_value == nullptr) // !?
                // expr->code_value = first_variable;
                expr->code_value = builder.CreateAlloca (builder.getInt32Ty (), 0, nullptr, "unknown_variable");

            #if 0
            if (decl == nullptr || decl->code == nullptr)
            {
                // expr->code = builder.CreateAlloca (builder.getInt32Ty (), 0, nullptr, "unknown");
            }
            else if (decl->sim_type != nullptr && decl->sim_type->from () != nullptr)
            {
                expr->code = builder.CreateLoad (code_type (decl->sim_type), decl->code_value, decl->item_name);
            }
            else
            {
                expr->code_value = decl->code_value;
            }
            #endif
        }

        else if (kind == intValue)
        {
            CmmIntValue * literal = expr->conv_CmmIntValue ();
            string text = literal->value;
            // int number = std::stoi (literal->value);
            // llvm::APInt apint (number, 32, /* isSigned */ true);
            llvm::APInt apint (32, text, /* radix */ 10);
            expr->code_value = llvm::ConstantInt::get (builder.getInt32Ty (), apint);
        }
        #if 0
        else if (kind == stringValue)
        {
        }
        else if (kind == subexprExpr)
        {
            code_expr (expr->conv_CmmSubexprExpr ()-> param);
        }
        else if (kind == indexExpr)
        {
            CmmExpr * left = expr->left;
            CmmExpr * right = expr->param;
            code_ref (left);
            auto temp = builder.CreateLoad (left->code);
            code_expr (right);
            expr->code = builder.extract_element (temp, right->code);
        }
        else if (kind == callExpr)
        {
            left = expr->left;
            args = tuple <> {};
            for (item : expr->param_list->items)
            {
                code_expr (item);
                args = args + tuple <> {item->code};
            }
            code_ref (left);
            temp = builder.load (left->code);
            expr->code = builder.call (temp, args);
        }
        #endif
        else if (kind == assignExpr)
        {
            code_assign (expr->conv_CmmAssignExpr ());
        }
        if (kind == logAndExpr || kind == logOrExpr)
        {
            code_and_or (expr->conv_CmmBinaryExpr ());
        }
        else if (kind == addExpr ||
                 kind == subExpr ||
                 kind == mulExpr ||
                 kind == divExpr ||
                 kind == modExpr ||
                 kind == shlExpr ||
                 kind == shrExpr ||
                 kind == bitAndExpr ||
                 kind == bitOrExpr ||
                 kind == bitXorExpr)
        {
            CmmBinaryExpr * bin_expr = expr->conv_CmmBinaryExpr ();
            CmmExpr * left = bin_expr->left;
            CmmExpr * right = bin_expr->right;
            code_expr (left);
            code_expr (right);

            if (kind == addExpr)
            {
                expr->code_value = builder.CreateAdd (left->code_value, right->code_value);
            }
            else if (kind == subExpr)
            {
                expr->code_value = builder.CreateSub (left->code_value, right->code_value);
            }
            else if (kind == mulExpr)
            {
                expr->code_value = builder.CreateMul (left->code_value, right->code_value, "multiply");
            }
            else if (kind == divExpr)
            {
                expr->code_value = builder.CreateSDiv (left->code_value, right->code_value);
            }
            else if (kind == modExpr)
            {
                expr->code_value = builder.CreateSRem (left->code_value, right->code_value);
            }
            else if (kind == shlExpr)
            {
                expr->code_value = builder.CreateShl (left->code_value, right->code_value);
            }
            else if (kind == shrExpr)
            {
                expr->code_value = builder.CreateAShr (left->code_value, right->code_value);
            }
            else if (kind == bitAndExpr)
            {
                expr->code_value = builder.CreateAnd (left->code_value, right->code_value);
            }
            else if (kind == bitOrExpr)
            {
                expr->code_value = builder.CreateOr (left->code_value, right->code_value);
            }
            else if (kind == bitXorExpr)
            {
                expr->code_value = builder.CreateXor (left->code_value, right->code_value);
            }
        }
        else if (kind == ltExpr ||
                 kind == leExpr ||
                 kind == gtExpr ||
                 kind == geExpr ||
                 kind == eqExpr ||
                 kind == neExpr )
        {
            CmmBinaryExpr * bin_expr = expr->conv_CmmBinaryExpr ();
            CmmExpr * left = bin_expr->left;
            CmmExpr * right = bin_expr->right;
            code_expr (left);
            code_expr (right);

            llvm::CmpInst::Predicate pred;
            if (kind == ltExpr)
            {
                pred = llvm::CmpInst::ICMP_SLT;
            }
            else if (kind == leExpr)
            {
                pred = llvm::CmpInst::ICMP_SLE;
            }
            else if (kind == gtExpr)
            {
                pred = llvm::CmpInst::ICMP_SGT;
            }
            else if (kind == geExpr)
            {
                pred = llvm::CmpInst::ICMP_SGE;
            }
            else if (kind == eqExpr)
            {
                pred = llvm::CmpInst::ICMP_EQ;
            }
            else if (kind == neExpr)
            {
                pred = llvm::CmpInst::ICMP_NE;
            }
            expr->code_value = builder.CreateCmp (pred, left->code_value, right->code_value);
        }
        #if 0
        else if (kind == minusExpr)
        {
            param = expr->param;
            code_expr (param);
            expr->code = builder.neg (param->code);
        }
        else if (kind == bitNotExpr)
        {
            param = expr->param;
            code_expr (param);
            expr->code = builder.not_ (param->code);
        }
        #endif

        if (expr->code_value == nullptr)
        {
            // expr->code_value = llvm::ConstantInt::get (builder.getInt32Ty (), 42);
            // expr->code_value = builder.CreateLoad (builder.getInt32Ty (), first_variable);
            // expr->code_value = first_variable;
            // expr->code_value = builder.CreateAlloca (builder.getInt32Ty (), 0, nullptr, "unknown_expression");
        }
        // expr->code = builder.CreateLoad (builder.getInt32Ty (), expr->code_value);
    }

    void code_program (SimScope * global_scope)
    {
        llvm::InitializeNativeTarget ();
        llvm::InitializeNativeTargetAsmParser ();
        llvm::InitializeNativeTargetAsmPrinter ();

        // llvm::InitializeAllTargets ();
        // llvm::InitializeAllAsmParsers ();
        // llvm::InitializeAllAsmPrinters ();

        // module = new llvm::Module ("my_module", ctx);

        if (global_scope != nullptr)
        {
           for (SimBasic * basic : global_scope->item_list)
           {
               CmmDecl * decl = basic->cmm_decl;
               if (CmmSimpleItem * simple_item = decl->conv_CmmSimpleItem ())
               {
                        if (simple_item->is_function)
                        {
                            cout << "FUNCTION " << simple_item->item_name << endl;
                            code_function (simple_item);
                        }
                        else
                        {
                            cout << "VARIABLE " << simple_item->item_name << endl;
                            code_global_variable (simple_item);
                        }
               }

               #if 0
               if (CmmSimpleDecl * simple_decl = decl->conv_CmmSimpleDecl ())
               {
                   for (CmmSimpleItem * simple_item : simple_decl->items)
                   {
                        if (simple_item->is_function)
                        {
                            cout << "FUNCTION " << simple_item->item_name << endl;
                            code_function (simple_item);
                        }
                        else
                        {
                            cout << "VARIABLE " << simple_item->item_name << endl;
                            code_global_variable (simple_item);
                        }
                   }
               }
               #endif
            }
        }

        print_module (&*module);
        // cout << "ASM" << endl;
        // print_asm (&*module);
        #if 0
        strmod = str (module);
        llmod = llvm::parse_assembly (strmod);
        pmb = llvm::create_pass_manager_builder ();
        pmb->opt_level = 2;
        pm = llvm::create_module_pass_manager ();
        pmb->populate (pm);
        pm->run (llmod);
        target = llvm::Target->from_default_triple ();
        machine = target->create_target_machine ();llvm::create_mcjit_compiler (llmod, machine)ee
        ee->finalize_object ();
        print (machine->emit_assembly (llmod));
        obj = machine->emit_object (llmod);
        fileName = compiler->win->outputFileName ("instr.o");
        FILE * f = open (fileName, "wb");
        f->write (obj);
        f->close ();
        #endif

        if (0)
        {
            llvm::EngineBuilder builder (std::move (module));
            // std::unique_ptr <llvm::Module> owner (module);
            // llvm::EngineBuilder builder (std::move (owner));
            std::string err;
            builder.setEngineKind(llvm::EngineKind::JIT).setErrorStr(&err);
            llvm::ExecutionEngine * engine = builder.create();
            cout << "Engine " << err << endl;
            engine->finalizeObject ();
            engine->runStaticConstructorsDestructors (false);
        }
        if (0)
        {
           // http://stackoverflow.com/questions/37267769/how-can-i-figure-out-why-a-call-to-llvmtargetmachineemittofile-fails-when-called

           // string cpu = "x86_64";
           string cpu = "";
           string feature = "";
           string output_file = "_output/output.asm"; // !?
           mkdir ("_output", 0700);

           auto triple = LLVMGetDefaultTargetTriple();
           // LLVMInitializeNativeTarget ();
           auto target = LLVMGetFirstTarget();
           auto opt_level = LLVMCodeGenOptLevel::LLVMCodeGenLevelNone;
           auto reloc_mode = LLVMRelocMode::LLVMRelocDefault;
           auto code_model = LLVMCodeModel::LLVMCodeModelDefault;
           auto target_machine = LLVMCreateTargetMachine (target, triple, cpu.c_str (), feature.c_str (), opt_level, reloc_mode, code_model);
           // auto file_type = LLVMCodeGenFileType::LLVMObjectFile;
           auto file_type = LLVMCodeGenFileType::LLVMAssemblyFile;

           char * error_str = nullptr;
           auto res = LLVMTargetMachineEmitToFile
                      (target_machine,
                       reinterpret_cast <LLVMModuleRef> (&*module),
                       (char *) output_file.c_str (),
                       file_type,
                       &error_str);
            cout << "File " << error_str << endl;
            ifstream f (output_file);
            if (f.good ())
            {
                ostringstream buffer;
                buffer << f.rdbuf();
                string text = buffer.str ();
                f.close ();
                cout << "FILE " << text;
            }
            cout.flush ();
        }

    }
};

/* ---------------------------------------------------------------------- */

void instruction_test (SimScope * global_scope)
{
   // example1 ();
   // example2 ();

   InstructionsModule inst;
   inst.code_program (global_scope);
}
