
/* clang-jit.cc */

#include "clang-jit.h"
// #include "clang-util.h"

#include <iostream> // important when compiled code is using iostream
#include <unistd.h> // extern char **environ;

/* ---------------------------------------------------------------------- */

#undef emit
#include "llvm/Config/llvm-config.h"

#define LLVM(a,b) ( ((a) << 8) + (b))
#define LLVM_VER LLVM (LLVM_VERSION_MAJOR, LLVM_VERSION_MINOR)

/* ---------------------------------------------------------------------- */

#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "llvm/IR/Module.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"
#include "clang/Frontend/Utils.h"
#include "llvm/ExecutionEngine/MCJIT.h"

#include "dlfcn.h"

/* ---------------------------------------------------------------------- */

#if LLVM_VERSION_MAJOR == 9
   using llvm::make_unique;
#endif

/* ---------------------------------------------------------------------- */

int execute (// const char * name,
             std::vector <const char *> options,
             string_list libraries,
             int new_argc,
             const char * * new_argv,
             char * const * new_envp)
{
   int Res = 255;

   int argc = options.size ();
   const char * * argv = & options [0];
   llvm::ArrayRef <const char *> array_ref (argv, argv + argc);
   llvm::IntrusiveRefCntPtr<clang::DiagnosticOptions> diagOpts = new clang::DiagnosticOptions();
   clang::TextDiagnosticPrinter * diagClient = new clang::TextDiagnosticPrinter(llvm::errs(), &*diagOpts);
   llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> diagID (new clang::DiagnosticIDs ());
   llvm::IntrusiveRefCntPtr<clang::DiagnosticsEngine> diags = new clang::DiagnosticsEngine (diagID, &*diagOpts, diagClient);

   #if LLVM_VER <= LLVM (14,0)
      std::unique_ptr<clang::CompilerInvocation> CI = clang::createInvocationFromCommandLine (array_ref);
   #else
      std::unique_ptr<clang::CompilerInvocation> CI = clang::createInvocation (array_ref);
   #endif

   clang::CompilerInstance Clang;
   Clang.setInvocation (std::move (CI));

   Clang.createDiagnostics ();
   if (!Clang.hasDiagnostics ())
      return Res;

   std::unique_ptr<clang::CodeGenAction> Act (new clang::EmitLLVMOnlyAction());
   if (!Clang.ExecuteAction (*Act))
       return Res;


   llvm::InitializeNativeTarget();
   llvm::InitializeNativeTargetAsmParser(); // added
   llvm::InitializeNativeTargetAsmPrinter();

   llvm::LLVMContext * Ctx = Act->takeLLVMContext();
   std::cout << "CONTEXT " << Ctx << std::endl;

   std::unique_ptr<llvm::Module> Module = Act->takeModule();

   if (Module) {
      std::cout << "MODULE" << std::endl;

      llvm::Function *EntryFn = Module->getFunction("main");
      if (!EntryFn) {
         llvm::errs() << "main function not found in module.\n";
         return 255;
   }

   std::vector<std::string> Args;
   if (new_argc == 0)
   {
      Args.push_back (Module->getModuleIdentifier());
   }
   else
   {
       for (int i = 0; i < new_argc; i++)
          Args.push_back (new_argv [i]);
   }

   std::string Error;
   llvm::ExecutionEngine * EE =
      llvm::EngineBuilder (std::move (Module))
        .setEngineKind(llvm::EngineKind::Either)
        .setErrorStr(&Error)
        .create();

   if (!EE) { llvm::errs() << "unable to make execution engine: " << Error << "\n"; }

   int cnt = libraries.size ();
   for (int inx = 0; inx < cnt; inx++)
   {
      std::string library = libraries [inx];
      // see <llvm-3.8>/tools/lli/lli.cpp
      void* lib_handle = dlopen (library.c_str(), RTLD_NOW | RTLD_GLOBAL);
      if(lib_handle == NULL)
        llvm::errs() << "library not loaded: " << library << "\n";
   }

   EE->finalizeObject();

   Res =  EE->runFunctionAsMain (EntryFn, Args, new_envp);

   std::cout << "DONE" << std::endl;
   }

   // Shutdown.
   // llvm::llvm_shutdown();

   return Res;
}

/* ---------------------------------------------------------------------- */

int compileAndRun (string_list options,
                   string_list libraries,
                   string_list new_argumets,
                   string_list new_environment)
{
    options.insert (options.begin (), "interpreter_name");
    // options.push_back ("-fsyntax-only");

    /* options as char pointers */
    std::vector <const char *> options_cstr;

    int opt_cnt = options.size ();
    for (int i = 0; i < opt_cnt; i++)
    {
        options_cstr.push_back (options[i].c_str ());
        std::cout << "OPT " << options[i].c_str () << std::endl;
    }

    /* argumets as char pointers */
    int new_argc = new_argumets.size ();
    const char * * new_argv = new const char * [new_argc];
    for (int i = 0; i < new_argc; i++)
    {
       new_argv [i] = new_argumets [i].c_str();
    }

    /* environment as char pointer */
    char * * new_envp = environ;

    int env_cnt = new_environment.size ();
    if (env_cnt > 0)
    {
       typedef char * CStr;
       new_envp = new CStr [env_cnt+1];
       for (int i = 0; i < env_cnt; i++)
       {
           new_envp [i] = (char *) new_environment [i].c_str();
       }
       new_envp [env_cnt] = NULL;
    }

    int result = execute (options_cstr,
                          libraries,
                          new_argc,
                          new_argv,
                          new_envp);

    delete [] new_argv;
    if (env_cnt > 0) delete [] new_envp;

    return result;
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
