#!/usr/bin/env python

from __future__ import print_function

import os, sys, importlib

from util import import_qt_modules, use_pyside2, use_pyqt5
import_qt_modules (globals ())


from input import fileNameToIndex
from env import CodePlugin

from lexer     import Lexer
from grammar   import Grammar, Rule, Expression, Alternative, Ebnf, Nonterminal, Terminal
from symbols import initSymbols
from grammar import Grammar

from tohtml    import ToHtml
from tolout    import ToLout
from toparser  import ToParser
from toproduct import ToProduct

from cparser   import CParser
from cproduct  import CProduct

# --------------------------------------------------------------------------

class GrammarPlugin (CodePlugin) :

   def __init__ (self, main_window) :
       super (GrammarPlugin, self).__init__ (main_window)

       menu = self.win.addTopMenu (self.mod.title)
       menu.aboutToShow.connect (self.onShowGramMenu)
       self.menu = menu

       act = QAction ("to &Python Parser", self.win)
       act.triggered.connect (self.toParser)
       act.need_grammar = True
       act.setShortcut ("Ctrl+F5")
       menu.addAction (act)

       act = QAction ("to &C++ Parser", self.win)
       act.triggered.connect (self.toCppParser)
       act.need_grammar = True
       act.setShortcut ("Ctrl+F6")
       menu.addAction (act)

       act = QAction ("to &HTML", self.win)
       act.triggered.connect (self.toHtml)
       act.need_grammar = True
       menu.addAction (act)

       act = QAction ("to &Lout", self.win)
       act.triggered.connect (self.toLout)
       act.need_grammar = True
       menu.addAction (act)
   def onShowGramMenu (self) :
       is_gram = self.win.hasExtension (self.win.getEditor(), ".g")
       for act in self.menu.actions () :
           if getattr (act, "need_grammar", False) :
              act.setEnabled (is_gram)

   def openGrammar (self, editor) :
       fileName = editor.getFileName ()
       source = str (editor.toPlainText ())
       grammar = Grammar ()
       grammar.openString (source)
       grammar.setFileName (fileName)

       self.sourceEdit = self.win.inputFile (fileName)
       self.win.initProject (editor)
       self.win.info.clearOutput ()

       return grammar

   # Html

   def toHtml (self) :
       e = self.win.getEditor ()
       if self.win.hasExtension (e, ".g") :
          outputFileName = self.win.outputFileName (e.getFileName (), ".html")

          grammar = self.openGrammar (e)
          grammar.parseRules ()

          product = ToHtml ()
          product.open (outputFileName)
          product.htmlFromRules (grammar)
          product.close ()

          self.win.reloadFile (outputFileName)
          self.win.joinProject (outputFileName)
          self.win.showHtml (outputFileName)
          print ("O.K.")

   # Lout

   def toLout (self) :
       e = self.win.getEditor ()
       if self.win.hasExtension (e, ".g") :
          outputFileName = self.win.outputFileName (e.getFileName (), ".lout")

          grammar = self.openGrammar (e)
          grammar.parseRules ()

          product = ToLout ()
          product.open (outputFileName)
          product.loutFromRules (grammar)
          product.close ()

          self.win.reloadFile (outputFileName)
          self.win.joinProject (outputFileName)

          # create PDF or PostScript file
          self.win.showLout (outputFileName)

          print ("O.K.")

   def toParser (self) :
       e = self.win.getEditor ()
       if self.win.hasExtension (e, ".g") :
          fileName = e.getFileName ()
          self.start (fileName)
          self.grammarFileName = fileName
          self.buildParser (rebuild = True) # toparser.py

   def toCppParser (self) :
       e = self.win.getEditor ()
       if self.win.hasExtension (e, ".g") :
          fileName = e.getFileName ()
          self.start (fileName)
          self.grammarFileName = fileName
          self.buildCParser (rebuild = True) # cparser.py

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
