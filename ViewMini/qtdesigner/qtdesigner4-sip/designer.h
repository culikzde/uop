
/* designer.h */

#ifndef DESIGNER_H
#define DESIGNER_H

#include <QtDesigner/QtDesigner>
#include <QtDesigner/QDesignerComponents>

#include "internals/qdesigner_integration_p.h"

/* ---------------------------------------------------------------------- */

class LocalDesignerIntegration : public qdesigner_internal::QDesignerIntegration
{
   public:
      explicit LocalDesignerIntegration(QDesignerFormEditorInterface * core, QObject *parent = 0) :
         qdesigner_internal::QDesignerIntegration (core, parent)
      {
          setSlotNavigationEnabled (true);
      }
};

/* ---------------------------------------------------------------------- */

class Designer : public QObject
{
   private:
      QDesignerFormEditorInterface * core;
      QDesignerFormWindowInterface * form;
      LocalDesignerIntegration * integration;
      QWidget * resource_editor;
      QWidget * signal_slot_editor;

   public:

      Designer (QWidget * window = NULL) :
         QObject (window),
         core (NULL),
         form (NULL),
         integration (NULL),
         resource_editor (NULL),
         signal_slot_editor (NULL)
      {
          core = QDesignerComponents::createFormEditor (window);

          core->setWidgetBox (QDesignerComponents::createWidgetBox (core, window));
          core->setObjectInspector (QDesignerComponents::createObjectInspector (core, window));

          core->setPropertyEditor (QDesignerComponents::createPropertyEditor (core, window));
          core->setActionEditor (QDesignerComponents::createActionEditor (core, window));

          resource_editor = QDesignerComponents::createResourceEditor (core, window);
          signal_slot_editor = QDesignerComponents::createSignalSlotEditor (core, window);

          // create propertyEditor before QDesignerIntegration
          // otherwise changes in propertyEditor are not displayed on designed form

          // create QDesignerIntegration before createFormWindow
          // otherwise propertyEditor and objectInspector are not updated when user selects designed widget

          // new qdesigner_internal::QDesignerIntegration (core, window);
          integration = new LocalDesignerIntegration (core, window);
          qdesigner_internal::QDesignerIntegration::initializePlugins (core);

          // connect (integration, SIGNAL(navigateToSlot(QString, QString, QStringList)),
          //         this, SLOT(navigateToSlot(QString, QString, QStringList)));

          form = core->formWindowManager()->createFormWindow();

          core->setTopLevel (window);
          core->formWindowManager()->setActiveFormWindow (form);

          /*
          objectInspector = core->objectInspector();
          widgetBox = core->widgetBox ();

          propertyEditor = core->propertyEditor ();
          actionEditor = core->actionEditor ();
          */
      }

      QWidget * getForm () { return form; }

      QWidget * widgetBox () { return core->widgetBox (); }
      QWidget * objectInspector () { return core->objectInspector (); }

      QWidget * propertyEditor () { return core->propertyEditor(); }
      QWidget * actionEditor () { return core->actionEditor(); }

      QWidget * resourceEditor () { return resource_editor; }
      QWidget * signalSlotEditor () { return signal_slot_editor; }

      void open (QString fileName)
      {
         QFile file (fileName);
         file.open (QFile::ReadOnly);
         form->setContents (&file);
      }

      void connectNavigation (QObject * target)
      {
          connect (integration, SIGNAL(navigateToSlot(QString, QString, QStringList)),
                   target, SLOT(navigateToSlot(QString, QString, QStringList)));
      }
};

/* ---------------------------------------------------------------------- */

#endif
