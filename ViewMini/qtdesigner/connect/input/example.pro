QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

SOURCES += example.cc

HEADERS += example.h

FORMS += example.ui
