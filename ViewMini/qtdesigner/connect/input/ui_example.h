/********************************************************************************
** Form generated from reading UI file 'example.ui'
**
** Created by: Qt User Interface Compiler version 5.15.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXAMPLE_H
#define UI_EXAMPLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_1;
    QHBoxLayout *horizontalLayout_1;
    QPushButton *leftButton;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *firstButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *upButton;
    QPushButton *downButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *lastButton;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *rightButton;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *leftVerticalLayout;
    QLabel *leftLabel;
    QListWidget *leftList;
    QVBoxLayout *bottomVerticalLayout;
    QLabel *bottomLabel;
    QListWidget *bottomList;
    QVBoxLayout *rightVerticalLayout;
    QLabel *rightLabel;
    QListWidget *rightList;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *addButton;
    QSpacerItem *horizontalSpacer_3;
    QDialogButtonBox *buttonBox;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_1 = new QVBoxLayout(centralwidget);
        verticalLayout_1->setObjectName(QString::fromUtf8("verticalLayout_1"));
        horizontalLayout_1 = new QHBoxLayout();
        horizontalLayout_1->setObjectName(QString::fromUtf8("horizontalLayout_1"));
        leftButton = new QPushButton(centralwidget);
        leftButton->setObjectName(QString::fromUtf8("leftButton"));

        horizontalLayout_1->addWidget(leftButton);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_1->addItem(horizontalSpacer_4);

        firstButton = new QPushButton(centralwidget);
        firstButton->setObjectName(QString::fromUtf8("firstButton"));

        horizontalLayout_1->addWidget(firstButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_1->addItem(horizontalSpacer);

        upButton = new QPushButton(centralwidget);
        upButton->setObjectName(QString::fromUtf8("upButton"));

        horizontalLayout_1->addWidget(upButton);

        downButton = new QPushButton(centralwidget);
        downButton->setObjectName(QString::fromUtf8("downButton"));

        horizontalLayout_1->addWidget(downButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_1->addItem(horizontalSpacer_2);

        lastButton = new QPushButton(centralwidget);
        lastButton->setObjectName(QString::fromUtf8("lastButton"));

        horizontalLayout_1->addWidget(lastButton);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_1->addItem(horizontalSpacer_5);

        rightButton = new QPushButton(centralwidget);
        rightButton->setObjectName(QString::fromUtf8("rightButton"));

        horizontalLayout_1->addWidget(rightButton);


        verticalLayout_1->addLayout(horizontalLayout_1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        leftVerticalLayout = new QVBoxLayout();
        leftVerticalLayout->setObjectName(QString::fromUtf8("leftVerticalLayout"));
        leftLabel = new QLabel(centralwidget);
        leftLabel->setObjectName(QString::fromUtf8("leftLabel"));
        leftLabel->setAlignment(Qt::AlignCenter);

        leftVerticalLayout->addWidget(leftLabel);

        leftList = new QListWidget(centralwidget);
        leftList->setObjectName(QString::fromUtf8("leftList"));

        leftVerticalLayout->addWidget(leftList);


        horizontalLayout_2->addLayout(leftVerticalLayout);

        bottomVerticalLayout = new QVBoxLayout();
        bottomVerticalLayout->setObjectName(QString::fromUtf8("bottomVerticalLayout"));
        bottomLabel = new QLabel(centralwidget);
        bottomLabel->setObjectName(QString::fromUtf8("bottomLabel"));
        bottomLabel->setAlignment(Qt::AlignCenter);

        bottomVerticalLayout->addWidget(bottomLabel);

        bottomList = new QListWidget(centralwidget);
        bottomList->setObjectName(QString::fromUtf8("bottomList"));

        bottomVerticalLayout->addWidget(bottomList);


        horizontalLayout_2->addLayout(bottomVerticalLayout);

        rightVerticalLayout = new QVBoxLayout();
        rightVerticalLayout->setObjectName(QString::fromUtf8("rightVerticalLayout"));
        rightLabel = new QLabel(centralwidget);
        rightLabel->setObjectName(QString::fromUtf8("rightLabel"));
        rightLabel->setAlignment(Qt::AlignCenter);

        rightVerticalLayout->addWidget(rightLabel);

        rightList = new QListWidget(centralwidget);
        rightList->setObjectName(QString::fromUtf8("rightList"));

        rightVerticalLayout->addWidget(rightList);


        horizontalLayout_2->addLayout(rightVerticalLayout);


        verticalLayout_1->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        addButton = new QPushButton(centralwidget);
        addButton->setObjectName(QString::fromUtf8("addButton"));

        horizontalLayout_3->addWidget(addButton);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        buttonBox = new QDialogButtonBox(centralwidget);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout_3->addWidget(buttonBox);


        verticalLayout_1->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Tools", nullptr));
        leftButton->setText(QCoreApplication::translate("MainWindow", "Left", nullptr));
        firstButton->setText(QCoreApplication::translate("MainWindow", "First", nullptr));
        upButton->setText(QCoreApplication::translate("MainWindow", "Up", nullptr));
        downButton->setText(QCoreApplication::translate("MainWindow", "Down", nullptr));
        lastButton->setText(QCoreApplication::translate("MainWindow", "Last", nullptr));
        rightButton->setText(QCoreApplication::translate("MainWindow", "Right", nullptr));
        leftLabel->setText(QCoreApplication::translate("MainWindow", "Left", nullptr));
        bottomLabel->setText(QCoreApplication::translate("MainWindow", "Bottom", nullptr));
        rightLabel->setText(QCoreApplication::translate("MainWindow", "Right", nullptr));
        addButton->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXAMPLE_H
