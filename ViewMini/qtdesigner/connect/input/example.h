#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    // Q_OBJECT

public:
    MainWindow (QWidget * parent = nullptr);
    ~MainWindow ();

private slots:
    void on_firstButton_clicked();

    void on_firstButton_pressed();

    void on_firstButton_released();

    void on_firstButton_toggled(bool checked);

private:
    Ui::MainWindow * ui;
};

#endif // EXAMPLE_H
