#!/bin/sh

DESIGNER="designer6"
which $DESIGNER || DESIGNER="designer-qt6"
which $DESIGNER || DESIGNER="/usr/lib/qt6/bin/designer"

insert_first () {
   local NAME=$1
   local NEW=$2
   eval VALUE=\"\$$NAME\"

   if test -z "$VALUE" ; then
      export $NAME="$NEW"
   else
      export $NAME="$NEW:$VALUE"
   fi
}

insert_first QT_PLUGIN_PATH _output/plugins

$DESIGNER $*
