#!/bin/env python

from PyQt6.QtCore import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *

import sys, importlib

sys.path.insert (1, ".")

import button

app = QApplication (sys.argv)
win = button.Button ()
win.show ()
app.exec ()
