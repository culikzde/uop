#!/bin/sh

sip-build --verbose --target-dir=. --no-make # --qmake=/usr/bin/qmake6

cd _build && make && cd .. || exit 1
cp _build/button/libbutton.so button.so || exit 1

python run.py

# pacman -S sip (python-pyqt6-sip) python-pyqt6 pyqt-builder
# conflict with sip4 python-sip4

# apt install build-essential (python3-pyqt6) sip-tools python3-dev

# dnf install python3-qt5-devel PyQt-builder (sip6) python3-devel 
