
/* button.h */

#ifndef BUTTON_H
#define BUTTON_H

#include <QPushButton>

/* ---------------------------------------------------------------------- */

class Button : public QPushButton {
   public:
      Button (QWidget * parent = 0) : QPushButton (parent) { setText ("My Button"); }
};

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // BUTTON_H
