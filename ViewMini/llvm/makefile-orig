CXX = clang

LLVM_CONFIG = llvm-config

LLVM_CFLAGS = $(shell $(LLVM_CONFIG) --cxxflags)
LLVM_LDFLAGS = $(shell $(LLVM_CONFIG) --ldflags)
LLVM_LIBS = $(shell $(LLVM_CONFIG) --libs)

# Debian 11: apt-get install libclang-cpp-dev
# Fedora: dnf install clang-devel llvm-devel

CXXFLAGS = -g -O0 -w
CXXFLAGS += $(LLVM_CFLAGS)

LDFLAGS += -rdynamic
LDFLAGS += $(LLVM_LDFLAGS)

LDLIBS += -lstdc++
LDLIBS += $(LLVM_LIBS)

LDLIBS += -ldl

# ---------------------------------------------------------------------------

# LIBRARIES

# FEDORA_14 = 1
# FEDORA_21 = 1
# FEDORA_22 = 1
# FEDORA_27 = 1
# FEDORA_29 = 1
# FEDORA_30 = 1
# FEDORA_31 = 1
FEDORA_34 = 1

# DEBIAN_9 = 1
# DEBIAN_10 = 1

ifdef DEBIAN_9
   LDLIBS += -lm
endif

ifdef FEDORA_14
   LLVM_LDFLAGS += -lLLVM-2.7
endif

ifdef FEDORA_21
   LLVM_LDFLAGS += -lLLVM-3.4
endif

ifdef FEDORA_22
   LLVM_LDFLAGS += -lLLVM-3.5
endif

ifdef FEDORA_29
  LLVM_LDFLAGS += -L /usr/lib
  LLVM_LDFLAGS += -lclang
endif

ifdef FEDORA_30
  LLVM_LDFLAGS += -lclang-cpp
endif

ifdef FEDORA_31
  LLVM_LDFLAGS += -lclang-cpp
endif

ifdef FEDORA_34
  LLVM_LDFLAGS += -lclang-cpp
endif

ifdef DEBIAN_9
  LLVM_LDFLAGS += -lLLVM
endif

# ---------------------------------------------------------------------------

# see <clang-source>/examples/clang-interpreter/Makefile
ifdef FEDORA_14

LLVM_LIBS += -lclangFrontend
LLVM_LIBS += -lclangDriver
LLVM_LIBS += -lclangCodeGen
LLVM_LIBS += -lclangSema
LLVM_LIBS += -lclangChecker
LLVM_LIBS += -lclangAnalysis
LLVM_LIBS += -lclangRewrite
LLVM_LIBS += -lclangAST
LLVM_LIBS += -lclangParse 
LLVM_LIBS += -lclangLex
LLVM_LIBS += -lclangBasic 

endif

# ---------------------------------------------------------------------------

ifdef FEDORA_21
  ADD_CLANG_LIBS = 1
endif

ifdef FEDORA_22
  ADD_CLANG_LIBS = 1
endif

ifdef FEDORA_27
  ADD_CLANG_LIBS = 1
endif

ifdef FEDORA_29
  ADD_CLANG_LIBS = 1
endif

ifdef DEBIAN_9
  ADD_CLANG_LIBS = 1
endif

ifdef DEBIAN_10
  ADD_CLANG_LIBS = 1
endif

ifdef ADD_CLANG_LIBS

LLVM_LIBS += -lclangCodeGen
LLVM_LIBS += -lclangDriver
LLVM_LIBS += -lclangFrontend
LLVM_LIBS += -lclangSerialization
LLVM_LIBS += -lclangTooling
LLVM_LIBS += -lclangParse # clangParse before clangSema
LLVM_LIBS += -lclangSema
LLVM_LIBS += -lclangStaticAnalyzerFrontend
LLVM_LIBS += -lclangStaticAnalyzerCheckers
LLVM_LIBS += -lclangStaticAnalyzerCore
LLVM_LIBS += -lclangAnalysis
LLVM_LIBS += -lclangIndex

ifndef FEDORA_21
LLVM_LIBS += -lclangRewrite
endif

LLVM_LIBS += -lclangRewriteFrontend
LLVM_LIBS += -lclangAST
LLVM_LIBS += -lclangEdit # clangEdit before clangLex
LLVM_LIBS += -lclangLex
LLVM_LIBS += -lclangBasic # last, important

endif

# ---------------------------------------------------------------------------

# COMPILE

all: interpreter.bin

SOURCES = interpreter-main.cc interpreter.h interpreter.cc interpreter-14-test.cc interpreter-11-alt.cc interpreter-11.cc interpreter-7.cc interpreter-3.cc interpreter-2.cc

interpreter.bin: interpreter-main.o $(SOURCES)
	$(LINK.cc) $< $(LDLIBS) -o $@

interpreter.o:  $(SOURCES)

# RUN

run: interpreter.bin
	./interpreter.bin `python gcc_options.py` sample.cc

run-gdb: interpreter.bin
	gdb ex r --args ./interpreter.bin `python gcc_options.py` sample.cc
	# gdb ex r --args ./interpreter.bin -fPIC `python gcc_options.py` `pkg-config Qt5Widgets --cflags --libs` sample-qt.cc

run-qt4: interpreter.bin
	./interpreter.bin -fPIC `python gcc_options.py` `pkg-config QtGui --cflags --libs` sample-qt.cc

run-qt5: interpreter.bin
	# ./interpreter.bin -fPIC -D __PIC__ `python gcc_options.py` `pkg-config Qt5Widgets --cflags --libs` sample-qt.cc
	./interpreter.bin -fPIC `python gcc_options.py` `pkg-config Qt5Widgets --cflags --libs` sample-qt.cc

QT6_OPTS =  -I /usr/include/qt6
QT6_OPTS += -I /usr/include/qt6/QtCore
QT6_OPTS += -I /usr/include/qt6/QtWidgets
QT6_OPTS += -lQt6Widgets

run-qt6: interpreter.bin
	# ./interpreter.bin -fPIC `python gcc_options.py` --std=c++17 $(QT6_OPTS) sample-qt.cc
	./interpreter.bin -fPIC `python gcc_options.py` --std=c++17 `pkg-config Qt6Widgets --cflags --libs` sample-qt.cc

run-qt5-dbg: interpreter.bin
	gdb ex r --args ./interpreter.bin -fPIC `python gcc_options.py` `pkg-config Qt5Widgets --cflags --libs` sample-qt.cc

# run-gtk1: interpreter.bin
# 	./interpreter.bin `python gcc_options.py` `pkg-config gtk+ --cflags --libs` sample-gtk.cc

run-gtk2: interpreter.bin
	./interpreter.bin `python gcc_options.py` `pkg-config gtk+-2.0 --cflags --libs` sample-gtk.cc

run-gtk3: interpreter.bin
	./interpreter.bin `python gcc_options.py` `pkg-config gtk+-3.0 --cflags --libs` sample-gtk.cc

# run-gtk4: interpreter.bin
# 	./interpreter.bin `python gcc_options.py` `pkg-config gtk4 --cflags --libs` sample-gtk.cc

# CLEAN

CLEANFILES = *.o *.bin

clean:
	- @ test -z "$(CLEANFILES)" || rm -f $(CLEANFILES)
