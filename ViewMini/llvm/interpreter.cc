
/* interpreter.cc */

#include "interpreter.h"

/* ---------------------------------------------------------------------- */

#include <iostream> // important when compiled code is using iostream

/* ---------------------------------------------------------------------- */

// #include <string.h> // strdup
#include <unistd.h> // extern char **environ;
#undef emit

/* ---------------------------------------------------------------------- */

#include "llvm/Config/llvm-config.h"

// Fedora 14
// #define  LLVM_VERSION_MAJOR 2
// #define  LLVM_VERSION_MINOR 7

/* ---------------------------------------------------------------------- */


#if LLVM_VERSION_MAJOR >= 9

   #include "interpreter-11-alt.cc"
   // #include "interpreter-11.cc"
   // clang 9, 10, 11, 12, 13, 14
   // Debian 11 ...
   // Fedora 31 ...

#elif LLVM_VERSION_MAJOR >= 7

   #include "interpreter-7.cc"
   // clang 7, 8
   // Debian 10
   // Fedora 29, 30

#elif LLVM_VERSION_MAJOR > 3 || LLVM_VERSION_MAJOR == 3 && LLVM_VERSION_MINOR >= 6

   #include "interpreter-3.cc"
   // clang 3.6 = 3.7 = 3.8 =~= 4 = 5 = 6
   // Debian 9
   // Fedora 23 ... 28

#else

   #include "interpreter-2.cc"
   // clang 2.7, 2.8, 3.0, 3.1, 3.2, 3.4, 3.5
   // Fedora 14 ... 22

#endif

/* ---------------------------------------------------------------------- */

#if 0

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

void handler (int sig)
{
  void *array [10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace (array, 10);

  // print out all the frames to stderr
  fprintf (stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd (array, size, STDERR_FILENO);
  exit (1);
}

#endif

/* ---------------------------------------------------------------------- */

#if 0

#include <iostream>
#include <stdexcept> // class std::runtime_error
// #include <dlfcn.h>
using namespace std;

void Interpreter::info (string msg)
{
   cout << msg << endl;
}

void Interpreter::error (string msg)
{
   cerr << msg << endl;
   // throw new std::runtime_error (msg);
}

#endif

/* ---------------------------------------------------------------------- */

int Interpreter::compileAndRun (std::vector <std::string> options,
                                std::vector <std::string> libraries,
                                std::vector <std::string> new_argumets,
                                std::vector <std::string> new_environment)
{
    // signal(SIGSEGV, handler);

    std::vector <const char *> options_cstr;

    int opt_cnt = options.size ();
    for (int i = 0; i < opt_cnt; i++)
    {
        // std::cout << "OPT " << options[i] << std::endl;
        options_cstr.push_back (options[i].c_str ());
    }

    // for (int i = 0; i < opt_cnt+1; i++)
    //     std::cout << "OPTS " << options_cstr[i] << std::endl;

    /*
    // DO NOT USE, c_str () value does not exit after loop
    for (std::string opt : options)
        options_cstr.push_back (opt.c_str ());
    */

    int new_argc = new_argumets.size ();
    const char * * new_argv = new const char * [new_argc];
    for (int i = 0; i < new_argc; i++)
       new_argv [i] = new_argumets [i].c_str();

    char * * new_envp = environ ;
    std::string env_str = ""; // keep local variable

    int env_cnt = new_environment.size ();
    if (env_cnt > 0)
    {
       typedef char * CStr;
       new_envp = new CStr [env_cnt+1];
       for (int i = 0; i < env_cnt; i++)
       {
           new_envp [i] = (char *) new_environment [i].c_str();
           std::cout << "ENV " << new_envp [i] << std::endl;
       }
       new_envp [env_cnt] = NULL;
    }

    int result = execute ("interpreter_name",
                          options_cstr,
                          libraries,
                          new_argc,
                          new_argv,
                          new_envp);

    delete [] new_argv;
    if (env_cnt > 0) delete [] new_envp;

    return result;
}

// see: clang/examples/clang-interpreter/main.cpp
// see: clang/tools/driver/cc1_main.cpp

/* ---------------------------------------------------------------------- */
