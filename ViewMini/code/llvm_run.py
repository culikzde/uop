#!/usr/bin/env python

from __future__ import print_function

import os, sys

try :
   import llvmlite.binding as llvm
   import llvmlite.ir as ir
   import ctypes
   use_llvmlite = True
except :
   use_llvmlite = False

sourceFileName = "examples/simple.cc"
irFileName = "_output/simple.ll"

cmd = "clang -S -emit-llvm " + sourceFileName + " -o " + irFileName
os.system (cmd)

with open (irFileName) as f :
   llvm_ir = f.read ()

# llvmlite/examples/ll_fpadd.py

llvm.initialize ()
llvm.initialize_native_target ()
llvm.initialize_native_asmparser ()
llvm.initialize_native_asmprinter ()

# Create a target machine representing the host
target = llvm.Target.from_default_triple ()
target_machine = target.create_target_machine ()
# And an execution engine with an empty backing module
backing_mod = llvm.parse_assembly ("")
engine = llvm.create_mcjit_compiler (backing_mod, target_machine)

"""
engine.add_object_file(llvm.ObjectFileRef.from_data(function_bytes))
func_ptr = engine.get_function_address(cpython_name)
# https://stackoverflow.com/questions/71193857/calling-numba-generated-pycfunctionwithkeywords-from-python
"""

# Create a LLVM module object from the IR
mod = llvm.parse_assembly (llvm_ir)
mod.verify ()
# Now add the module and make sure it is ready for execution
engine.add_module (mod)
engine.finalize_object ()
engine.run_static_constructors ()

# Look up the function pointer (a Python int)
func_ptr = engine.get_function_address ("main")

# Run the function via ctypes
cfunc = ctypes.CFUNCTYPE (ctypes.c_int, ctypes.c_int, ctypes.c_void_p) (func_ptr)
res = cfunc (1, None)
print ("RESULT main =", res)

# --------------------------------------------------------------------------

# ArchLinux: pacman -S python-llvmlite
# Fedora 34: dnf install python3-llvmlite
# Debian 10 : apt-get install python3-llvmlite

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

# --------------------------------------------------------------------------

