#include <gtkmm.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>

#include <iostream>
using namespace std;

class MyWindow : public Gtk::Window
{
   public :
      MyWindow ()
      {
         set_title ("Hello World");
         resize (200, 50);
         set_border_width (10);

         button.set_label ("Click Here");
         button.signal_clicked().connect (sigc::mem_fun (*this, &MyWindow::on_button_clicked));
         add (button);
         button.show ();
      }

      void on_button_clicked ()
      {
         cout << "Click" << endl;
         set_title ("Click");
         button.set_label ("Click");
      }

      Gtk::Button button;
};

int main (int argc, char * * argv)
{
   Gtk::Main app (argc, argv);
   MyWindow win;
   Gtk::Main::run (win);
}

// old-compile: -P gtkmm-2.4 -lstdc++ -fPIC
// Fedora: dnf install gtkmm24-devel

// compile: -P gtkmm-3.0 -lstdc++ -fPIC
// Fedora: dnf install gtkmm30-devel

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
