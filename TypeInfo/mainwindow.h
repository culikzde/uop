#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private:
    Ui::MainWindow *ui;

    QObject * display_obj = nullptr;
    void addText (QString name, QString value);
    void displayObject(QObject * obj);

private:
    QString nazev_var = "Nejaky nazev";
public:
    QString getNazev () { return nazev_var; }
    void setNazev (QString s) { nazev_var = s; }

    Q_PROPERTY (QString nazev READ getNazev WRITE setNazev)

private slots:
    void on_table_cellChanged(int row, int column);
};
#endif // MAINWINDOW_H
