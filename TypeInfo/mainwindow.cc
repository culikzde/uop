#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QMetaObject>
#include <QMetaProperty>

#include <QTableWidget>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->table->setColumnCount (2);
    ui->table->setHorizontalHeaderLabels (QStringList () << "name" << "value");
    ui->table->horizontalHeader()->setStretchLastSection (true);

    displayObject (ui->pushButton);
    // displayObject (this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addText (QString name, QString value)
{
    int cnt = ui->table->rowCount();
    ui->table->setRowCount (cnt+1);

    QTableWidgetItem * item1 = new QTableWidgetItem;
    item1->setText (name);
    ui->table->setItem (cnt, 0, item1);

    QTableWidgetItem * item2 = new QTableWidgetItem;
    item2->setText (value);
    ui->table->setItem (cnt, 1, item2);
}

void MainWindow::displayObject(QObject * obj)
{
    display_obj = nullptr;

    // Class cls = obj.getClass ();
    // Type t = obj.getType ();
    const QMetaObject * cls = obj->metaObject();

    ui->table->setRowCount(0);
    addText ("(class name)", cls->className ());
    if (cls->superClass() != nullptr) addText ("(super class)", cls->superClass()->className ());

    int cnt = cls->propertyCount();
    for (int inx = 0; inx < cnt; inx++)
    {
        QMetaProperty p = cls->property (inx);
        QVariant v = p.read (obj);
        addText (p.name(), v.toString());
    }

    display_obj = obj;
}

void MainWindow::on_table_cellChanged(int row, int column)
{
    if (column == 1 && display_obj != nullptr)
    {
        QTableWidgetItem * item1 = ui->table->item (row, 0);
        QTableWidgetItem * item2 = ui->table->item (row, 1);
        QString name = item1->text ();
        QString value = item2->text ();
        // setWindowTitle (name + " := " + value);

        QObject * obj = display_obj;
        const QMetaObject * cls = obj->metaObject();

        int cnt = cls->propertyCount();
        for (int inx = 0; inx < cnt; inx++)
        {
            QMetaProperty p = cls->property (inx);
            if (p.name () == name)
            {
                QVariant v = value;
                if (p.type() == QVariant::Int)
                {
                    // setWindowTitle ("int");
                    v = v.toInt();
                }
                p.write (obj, v);
            }
        }

    }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

