#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QTextEdit>
#include <QFileInfo>

/* ---------------------------------------------------------------------- */

class TreeItem : public QTreeWidgetItem
{
public:
    QString path;
};

/* ---------------------------------------------------------------------- */

class Visitor
{
protected:
    virtual void visit (QFileInfo & info) = 0;
private:
    void processBranch (QTreeWidgetItem * branch);
public:
    void process (QTreeWidget * tree);
};

class FileSizeVisitor : public Visitor
{
private:
    int cnt;
    long long sum;
    QTextEdit * output;
    void print (QString s);
protected:
    void visit (QFileInfo & info);
public:
     FileSizeVisitor (QTreeWidget * tree, QTextEdit * edit);
};

/* ---------------------------------------------------------------------- */

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void showDir (QTreeWidgetItem * target, QString path);

private:
    Ui::MainWindow *ui;
};

/* ---------------------------------------------------------------------- */

#endif // MAINWINDOW_H
