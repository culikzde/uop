#include "visitor.h"
#include "ui_visitor.h"

#include <QApplication>
#include <QDir>

/* ---------------------------------------------------------------------- */

void Visitor::processBranch (QTreeWidgetItem * branch)
{
    TreeItem * item = dynamic_cast < TreeItem * > (branch);
    if (item != nullptr)
    {
        QFileInfo info (item->path);
        if (info.isFile())
           visit (info);
    }

    int cnt = branch->childCount();
    for (int i = 0; i < cnt; i++)
    {
        processBranch (branch->child (i));
    }
}

void Visitor::process (QTreeWidget * tree)
{
    processBranch (tree->invisibleRootItem ());
}

/* ---------------------------------------------------------------------- */

void FileSizeVisitor::print (QString s)
{
    output->append (s);
}

void FileSizeVisitor::visit (QFileInfo &info)
{
    print ("file " + info.fileName());
    cnt ++;
    sum = sum + info.size();
}

QString fmt (long long n)
{
    QString s = QString::number (n);

    QString result = "";
    int cnt = 0;

    for (int i = s.length(); i >= 0; i--)
    {
        if (cnt > 1 && cnt % 3 == 1) result = ' ' + result;
        result = s[i] + result;
        cnt ++;
    }

    return result;
}

FileSizeVisitor::FileSizeVisitor (QTreeWidget * tree, QTextEdit * edit)
{
    output = edit;
    cnt = 0;
    sum = 0;
    process (tree);
    print (QString::number (cnt) + " files, " + fmt (sum) + " bytes");
}

/* ---------------------------------------------------------------------- */

void MainWindow::showDir (QTreeWidgetItem * target, QString path)
{
    QDir dir (path);

    TreeItem * branch = new TreeItem;
    branch->path = dir.absolutePath ();
    branch->setText (0, dir.dirName());
    branch->setToolTip (0, branch->path);
    branch->setForeground(0, QColor ("red"));
    branch->setIcon(0, QIcon::fromTheme ("folder"));
    target->addChild (branch);

    QDir::Filters filter = QDir::AllEntries | QDir::NoDotAndDotDot;
    QDir::SortFlags sort = QDir::Name;

    for (QFileInfo info : dir.entryInfoList (filter, sort))
    {
       if (info.isDir())
       {
          showDir (branch, info.absoluteFilePath());
       }
       else
       {
          TreeItem * node = new TreeItem;
          node->path = info.absoluteFilePath();
          node->setText (0, info.fileName());
          node->setToolTip (0, node->path);
          node->setForeground (0, QColor ("blue"));
          node->setIcon (0, QIcon::fromTheme ("document"));
          branch->addChild (node);
       }
    }
}

/* ---------------------------------------------------------------------- */

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QDir dir = QDir ("..");
    showDir (ui->treeWidget->invisibleRootItem(), dir.canonicalPath());
    FileSizeVisitor (ui->treeWidget, ui->textEdit);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* ---------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}


