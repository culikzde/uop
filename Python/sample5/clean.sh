test -f Makefile && make clean
rm Makefile
rm sample.sbf
rm sample.exp
rm sipAPIsample.h
rm sipsamplecmodule.cpp
rm sipsamplecmodule.o
rm sipsampleSample.cpp
rm sipsampleSample.o
rm sample.o
rm sample.so

rm -rf _build
rm sample.*.so
rm sample.cpython-310-x86_64-linux-gnu.so

