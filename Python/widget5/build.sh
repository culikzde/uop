#!/bin/sh

use_sip6=false

if test -f /etc/arch-release ; then
   use_sip6=true
fi

if grep 36 /etc/fedora-release 2>/dev/null >/dev/null ; then
   use_sip6=true
fi

if $use_sip6 ; then

   sip-build --verbose --target-dir=. --no-make
   ( cd _build && make )
   cp _build/button/libbutton.so button.so
   # required file pyproject.toml
   python run.py

   # pacman -S sip (python-pyqt5-sip) python-pyqt5 pyqt-builder
   # conflict with sip4 python-sip4

   # dnf install python3-qt5-devel PyQt-builder (sip6) python3-devel 

else

   test -f Makefile && make clean
   python3 configure.py || exit 1

   # Debian 11 problem
   sed -i 's/$(LINK) $(LFLAGS) -o $(TARGET) $(OFILES) $(LIBS)/$(LINK) -o $(TARGET) $(OFILES) $(LFLAGS) $(LIBS)/' Makefile

   make || exit 1
   python3 run.py

   # dnf install python3-sip-devel python3-qt5-devel qt5-tools-devel
   # apt-get install build-essential pkg-config python3-pyqt5 python3-sip-dev pyqt5-dev qttools5-dev
   # pacman -S sip4 python-sip4 (python-pyqt5-sip) python-pyqt5 pkgconf

fi
