#include "properties.h"
#include "ui_properties.h"

#include <QApplication>
#include <QMetaProperty>

void MainWindow::showBranch (QTreeWidgetItem *target, QObject *obj)
{
    TreeItem * branch = new TreeItem ();
    target->addChild (branch);

    const QMetaObject * cls = obj->metaObject();
    branch->setText (0, obj->objectName() + " : " + cls->className ());
    branch->obj = obj;

    for ( QObject * t : obj->children() )
        showBranch (branch, t);
}

QTableWidgetItem * MainWindow::addLine (QString name, QString value)
{
    ui->tableWidget->setHorizontalHeaderLabels (QStringList () << "Name" << "Value");

    int inx = ui->tableWidget->rowCount ();
    ui->tableWidget->setRowCount (inx +1);

    QTableWidgetItem * cell = new QTableWidgetItem ();
    cell ->setText (name);
    ui->tableWidget->setItem(inx, 0, cell);

    cell = new QTableWidgetItem ();
    cell ->setText (value);
    ui->tableWidget->setItem(inx, 1, cell);

    return cell;
}

void MainWindow::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    TreeItem * node = dynamic_cast < TreeItem * > (item);
    if (node != nullptr)
    {
        ui->tableWidget->clear ();
        ui->tableWidget->setColumnCount(2);
        ui->tableWidget->setRowCount(0);

        QObject * obj = node->obj;
        const QMetaObject * cls = obj->metaObject();
        addLine ("class name", cls->className());

        int cnt = cls->propertyCount();
        for (int i = 0; i < cnt; i++)
        {
            QMetaProperty prop = cls->property (i);
            QVariant v = prop.read (obj);
            QTableWidgetItem * cell = addLine (prop.name(), v.toString());
            if (v.type() == QVariant::Icon)
                cell->setData (Qt::DecorationRole, v);
        }
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowIcon (QIcon::fromTheme("weather-clear"));

    QObject * p = ui->treeWidget;
    while (p->parent() != nullptr)
        p = p->parent ();

    showBranch (ui->treeWidget->invisibleRootItem(), p);
    ui->treeWidget->expandAll ();
}

MainWindow::~MainWindow()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

