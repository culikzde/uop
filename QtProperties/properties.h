#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QTableWidget>

class TreeItem : public QTreeWidgetItem
{
   public:
      QObject * obj;

      TreeItem () : obj (nullptr) { }
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_treeWidget_itemDoubleClicked (QTreeWidgetItem *item, int column);

    void showBranch (QTreeWidgetItem * target, QObject * obj);
    QTableWidgetItem * addLine (QString name, QString value);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
