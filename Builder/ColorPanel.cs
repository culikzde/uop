﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public partial class ColorPanel : ToolStripButton // UserControl
    {
        private Color color;

        public ColorPanel (Color c, String name = "")
        {
            InitializeComponent ();
            color = c;

            // BackColor = c;
            // ToolTip tooltip = new ToolTip ();
            // tooltip.SetToolTip (this, name);

            const int size = 32;
            Bitmap img = new Bitmap (size, size);
            Graphics g = Graphics.FromImage (img);
            g.FillEllipse (new SolidBrush (color), 1, 1, size - 2, size - 2);

            this.Image = img;
            // this.Text = name;
            this.ToolTipText = name;

            this.MouseDown += ColorPanel_MouseDown;
        }

        private void ColorPanel_MouseDown (object sender, MouseEventArgs e)
        {
            DoDragDrop (color, DragDropEffects.Move);
        }
    }
}
