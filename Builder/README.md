Builder
=======

Toolbar icons
```csharp
using System.ComponentModel; // TypeDescriptor

AttributeCollection attrCol = TypeDescriptor.GetAttributes (ComponentType);
ToolboxBitmapAttribute imageAttr = attrCol [typeof (ToolboxBitmapAttribute)] as ToolboxBitmapAttribute;
icon = imageAttr.GetImage (ComponentType);
```

Drag and drop
```csharp
DoDragDrop (factory_object, DragDropEffects.Copy);
DoDragDrop (color, DragDropEffects.Move);
```

```csharp
private void designer_DragEnter (object sender, DragEventArgs e)
{
    if (e.Data.GetDataPresent (typeof (Color)))
        e.Effect = DragDropEffects.Move;
    else if (e.Data.GetDataPresent ("Builder.Factory"))  // Namespace.Class
        e.Effect = DragDropEffects.Copy;
    else
        e.Effect = DragDropEffects.None;
}

private void designer_DragDrop (object sender, DragEventArgs e)
{
    if (e.Data.GetDataPresent ("Builder.Factory"))
    {
        Factory factory = e.Data.GetData ("Builder.Factory") as Factory;
        Point location = designer.PointToClient (new Point (e.X, e.Y));
        createObject (factory, null, location);
    }
}

private void treeView_DragEnter (object sender, DragEventArgs e)
{
    if (e.Data.GetDataPresent ("Builder.Factory"))
        e.Effect = DragDropEffects.Copy;
}

private void treeView_DragDrop (object sender, DragEventArgs e)
{
    if (e.Data.GetDataPresent ("Builder.Factory"))
    {
        Factory factory = e.Data.GetData ("Builder.Factory") as Factory;
        Point pt = treeView.PointToClient (new Point (e.X, e.Y));
        TreeNode target = treeView.GetNodeAt (pt);
        createObject (factory, target, new Point (0, 0));
    }
}
```

![Builder.png](Pictures/Builder.png)

Custom attribute
```csharp
    public class ConnectAttribute : Attribute
    {
        public string ConnectName { get; set; }

        public ConnectAttribute (string name)
        {
            ConnectName = name;
        }
    }
```

```csharp
    public class Record
    {
        [Connect ("TreeName")]
        public string RecordName { get; set; }

        [Connect ("TreeColor")]
        public Color RecordColor { get; set; }

        public static Color BasicColor () { return Color.Orange; }
    }
```

Find property with custom attribute.

Call method when property is changed.

```csharp
    public class MyNode : TreeNode
    {
        private PropertyDescriptor NameProp = null;
        private PropertyDescriptor ColorProp = null;

        private PropertyDescriptor findProperty (object obj, string connect_name)
        {
            // search property with Connection attribute and connection_name, store property name
            string property_name = "";
            Type type = obj.GetType ();
            foreach (PropertyInfo info in type.GetProperties ())
            {
                object [] attrs = info.GetCustomAttributes (typeof(ConnectAttribute), true);
                foreach (ConnectAttribute attr in attrs)
                   if (attr != null && attr.ConnectName == connect_name)
                       property_name = info.Name;
            }

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties (obj);
            PropertyDescriptor result = null;

            // find property with connection Attribute
            if (property_name != "")
                result = properties.Find (property_name, false);

            return result;
        }

        public void connect ()
        {
            object obj = getData ();

            NameProp = findProperty (obj, "TreeName");
            ColorProp = findProperty (obj, "TreeColor");

            if (NameProp != null)
                NameProp.AddValueChanged (obj, CopyName);
            if (ColorProp != null)
                ColorProp.AddValueChanged (obj, CopyColor);
        }

        private void CopyName (object sender, EventArgs e)
        {
            object obj = getData ();
            if (NameProp != null)
                this.Text = NameProp.GetValue (obj).ToString ();
        }

        private void CopyColor (object sender, EventArgs e)
        {
            object obj = getData ();
            if (ColorProp != null)
                this.BackColor = (Color)ColorProp.GetValue (obj);
        }
    }
```
