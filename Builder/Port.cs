﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public partial class Port : UserControl
    {
        private Control comp;
        private int relx, rely;
        // public bool resize;

        public Port (Control c, int x, int y)
        {
            InitializeComponent ();
            comp = c;
            relx = x;
            rely = y;
            place ();
        }

        private int X0, Y0;
        private bool click;

        private void MyPort_MouseDown (object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void MyPort_MouseMove (object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Left)
                {
                    this.Left += e.X - X0;
                    this.Top += e.Y - Y0;
                    correct ();
                }
            }
        }

        private void MyPort_MouseUp (object sender, MouseEventArgs e)
        {
            click = false;
        }


        public void place ()
        {
            Left = comp.Left + relx * (comp.Width - Width);
            Top = comp.Top + rely * (comp.Height - Height);
            Parent = comp.Parent;
            BringToFront();
        }

        public void correct ()
        {
            comp.Left = Left - relx * (comp.Width - Width);
            comp.Top = Top - rely * (comp.Height - Height);

            foreach (Control c in Parent.Controls)
            {
                Port p = c as Port;
                if (p != null && p.comp == this.comp /* && p != this */)
                    p.place ();
            }
        }

        /*
        private void InitializeComponent ()
        {
            this.SuspendLayout();
            // 
            // Port
            // 
            this.BackColor = System.Drawing.Color.Lime;
            this.Name = "Port";
            this.Size = new System.Drawing.Size(10, 10);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyPort_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MyPort_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MyPort_MouseUp);
            this.ResumeLayout(false);
        }
        */

    }
}
