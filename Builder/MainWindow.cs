﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Builder
{
    public partial class MainWindow : Form
    {
        public MainWindow ()
        {
            InitializeComponent ();

            createColorPanels (new List<String> { "red", "green", "blue", "yellow", "orange", "lime", "cornflowerblue" });

            createComponent (typeof (Record), typeof (RecordView));
            createComponent (typeof (Field), typeof (FieldView));
            createComponent (typeof (Button));
            createComponent (typeof (Panel));
            createComponent (typeof (TreeView));
            createComponent (typeof (ListView));
            createComponent (typeof (TextBox));
            createComponent (typeof (DataGrid));

            designer.AllowDrop = true;
            treeView.AllowDrop = true;
        }

        private void createColorPanels (List<String> colorNames)
        {
            foreach (var colorName in colorNames)
            {
                Color color = Color.FromName (colorName);
                ColorPanel p = new ColorPanel (color, colorName);
                // int n = colorPage.Controls.Count;
                // p.Left = n * p.Width + (n + 1) * p.Left;
                // colorPage.Controls.Add (p);
                colorToolbar.Items.Add (p);
            }
        }

        private void createComponent (Type type0, Type view0 = null)
        {
            Factory f = new Factory (type0, view0);

            int n = componentPage.Controls.Count;
            f.Left = f.Left + n * (f.Width + f.Left);
            componentPage.Controls.Add (f);
        }

        private void designer_DragEnter (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent (typeof (Color)))
                e.Effect = DragDropEffects.Move;
            // else if (e.Data.GetDataPresent (DataFormats.Text))
            //    e.Effect = DragDropEffects.Copy;
            // else if (e.Data.GetDataPresent ("System.Windows.Forms.Button"))
            //     e.Effect = DragDropEffects.Copy;
            else if (e.Data.GetDataPresent ("Builder.Factory"))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void designer_DragDrop (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent ("Builder.Factory"))
            {
                Factory factory = e.Data.GetData ("Builder.Factory") as Factory;
                Point location = designer.PointToClient (new Point (e.X, e.Y));
                createObject (factory, null, location);
            }
        }

        private void treeView_DragEnter (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent ("Builder.Factory"))
                e.Effect = DragDropEffects.Copy;
        }

        private void treeView_DragDrop (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent ("Builder.Factory"))
            {
                Factory factory = e.Data.GetData ("Builder.Factory") as Factory;
                Point pt = treeView.PointToClient (new Point (e.X, e.Y));
                TreeNode target = treeView.GetNodeAt (pt);
                createObject (factory, target, new Point (0, 0));
            }
        }

        private void createObject (Factory factory, TreeNode target, Point location)
        {
            Object obj = factory.createInstance ();

            MyNode node = new MyNode ();
            node.Text = obj.ToString ();
            node.Tag = obj;
            node.connect ();

            if (target == null)
                treeView.Nodes.Add (node);
            else
                target.Nodes.Add (node);

            if (obj is Control)
            {
                Control ctl = obj as Control;
                if (location != null)
                {
                    ctl.Left = location.X;
                    ctl.Top = location.Y;
                }

                new Adapter (ctl, this);

                designer.Controls.Add (ctl);
            }

        }

        private void treeView_NodeMouseClick (object sender, TreeNodeMouseClickEventArgs e)
        {
            SelectObject (e.Node.Tag);
        }

        public void SelectObject (object obj)
        {
            if (obj is Common)
                obj = (obj as Common).recallData();

            // obj = new ObjectWrapper (obj);
            propGrid.SelectedObject = obj;
        }

        private void readData (TextWriter stream)
        {

        }

        private void writeData (TextWriter stream)
        {
            foreach (TreeNode node in treeView.Nodes)
            {
                object obj = node.Tag;
                if (obj is Common)
                    obj = (obj as Common).recallData ();

                XmlSerializer writer = new XmlSerializer (obj.GetType ());
                writer.Serialize (stream, obj);
            }
        }

        private void open_Click (object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog () == DialogResult.OK)
            {
                StreamReader stream = new StreamReader (openFileDialog.FileName);
                // XmlSerializer reader = new XmlSerializer (typeof (MyObject));
                // obj = (MyObject)reader.Deserialize (stream);
                stream.Close ();

                // AddObject (obj);
            }
        }

        private void save_Click (object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog () == DialogResult.OK)
            {
                StreamWriter stream = new StreamWriter (saveFileDialog.FileName);
                // XmlSerializer writer = new XmlSerializer (typeof (MyObject));
                // writer.Serialize (stream, obj);
                writeData (stream);
                stream.Close ();
            }
        }

        private void viewXml_Click (object sender, EventArgs e)
        {
            StringWriter stream = new StringWriter ();
            writeData (stream);
            info.Text = stream.ToString ();
        }

        private void viewData_Click (object sender, EventArgs e)
        {
            new DataWindow ().Show ();
        }

        private void quit_Click (object sender, EventArgs e)
        {
            Close ();
        }

        private void addStructure (TreeNodeCollection target, object obj)
        {
           TreeNode node = new TreeNode ();
           node.Tag = obj;
           string typ = obj.GetType().ToString();
           
           if (obj is Control)
           {
              Control c = obj as Control;
              node.Text = c.Name + " : " + typ;
              
              foreach (Control item in c.Controls)
                 addStructure (node.Nodes, item);
           }
           else
           {
              node.Text = obj.ToString () + " : " + typ;
           }
           
           target.Add (node);
        }
        
        void structureTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
           object obj = e.Node.Tag;
           Type typ = obj.GetType ();
           foreach (PropertyInfo prop in typ.GetProperties ())
           {
              string name = prop.Name;
              object value = null;
              if (prop.CanRead)
              {
                 value = prop.GetValue (obj, null); /* second parameter required from .Net 4.5 */
              }
              object [] line = new object [] {name, value};
              reflectionGrid.Rows.Add (line);
           }
           
       
        }
        
        private void reflection_Click (object sender, EventArgs e)
        {
           leftTabs.SelectedTab = structureTab;
           rightTabs.SelectedTab = reflectionTab;
           
           structureTree.Nodes.Clear ();
           addStructure (structureTree.Nodes, this);
        }
        
        
        
        
        
        

    }
}
