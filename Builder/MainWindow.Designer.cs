﻿namespace Builder
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           this.mainMenu = new System.Windows.Forms.MenuStrip();
           this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
           this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
           this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
           this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
           this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
           this.viewDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
           this.viewXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
           this.toolMenu = new System.Windows.Forms.ToolStripMenuItem();
           this.reflectionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
           this.statusBar = new System.Windows.Forms.StatusStrip();
           this.toolBar = new System.Windows.Forms.ToolStrip();
           this.splitContainer1 = new System.Windows.Forms.SplitContainer();
           this.splitContainer2 = new System.Windows.Forms.SplitContainer();
           this.leftTabs = new System.Windows.Forms.TabControl();
           this.treeTab = new System.Windows.Forms.TabPage();
           this.treeView = new System.Windows.Forms.TreeView();
           this.structureTab = new System.Windows.Forms.TabPage();
           this.structureTree = new System.Windows.Forms.TreeView();
           this.splitContainer3 = new System.Windows.Forms.SplitContainer();
           this.designer = new System.Windows.Forms.Panel();
           this.rightTabs = new System.Windows.Forms.TabControl();
           this.propertyTab = new System.Windows.Forms.TabPage();
           this.propGrid = new System.Windows.Forms.PropertyGrid();
           this.reflectionTab = new System.Windows.Forms.TabPage();
           this.palette = new System.Windows.Forms.TabControl();
           this.componentPage = new System.Windows.Forms.TabPage();
           this.colorPage = new System.Windows.Forms.TabPage();
           this.colorToolbar = new System.Windows.Forms.ToolStrip();
           this.info = new System.Windows.Forms.TextBox();
           this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
           this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
           this.reflectionGrid = new System.Windows.Forms.DataGridView();
           this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
           this.ValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
           this.mainMenu.SuspendLayout();
           ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
           this.splitContainer1.Panel1.SuspendLayout();
           this.splitContainer1.Panel2.SuspendLayout();
           this.splitContainer1.SuspendLayout();
           ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
           this.splitContainer2.Panel1.SuspendLayout();
           this.splitContainer2.Panel2.SuspendLayout();
           this.splitContainer2.SuspendLayout();
           this.leftTabs.SuspendLayout();
           this.treeTab.SuspendLayout();
           this.structureTab.SuspendLayout();
           ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
           this.splitContainer3.Panel1.SuspendLayout();
           this.splitContainer3.Panel2.SuspendLayout();
           this.splitContainer3.SuspendLayout();
           this.rightTabs.SuspendLayout();
           this.propertyTab.SuspendLayout();
           this.reflectionTab.SuspendLayout();
           this.palette.SuspendLayout();
           this.colorPage.SuspendLayout();
           ((System.ComponentModel.ISupportInitialize)(this.reflectionGrid)).BeginInit();
           this.SuspendLayout();
           // 
           // mainMenu
           // 
           this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
         this.fileMenu,
         this.viewMenu,
         this.toolMenu});
           this.mainMenu.Location = new System.Drawing.Point(0, 0);
           this.mainMenu.Name = "mainMenu";
           this.mainMenu.Size = new System.Drawing.Size(1264, 24);
           this.mainMenu.TabIndex = 0;
           this.mainMenu.Text = "menuStrip1";
           // 
           // fileMenu
           // 
           this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
         this.openMenuItem,
         this.saveMenuItem,
         this.quitMenuItem});
           this.fileMenu.Name = "fileMenu";
           this.fileMenu.Size = new System.Drawing.Size(37, 20);
           this.fileMenu.Text = "&File";
           // 
           // openMenuItem
           // 
           this.openMenuItem.Name = "openMenuItem";
           this.openMenuItem.Size = new System.Drawing.Size(103, 22);
           this.openMenuItem.Text = "&Open";
           this.openMenuItem.Click += new System.EventHandler(this.open_Click);
           // 
           // saveMenuItem
           // 
           this.saveMenuItem.Name = "saveMenuItem";
           this.saveMenuItem.Size = new System.Drawing.Size(103, 22);
           this.saveMenuItem.Text = "&Save";
           this.saveMenuItem.Click += new System.EventHandler(this.save_Click);
           // 
           // quitMenuItem
           // 
           this.quitMenuItem.Name = "quitMenuItem";
           this.quitMenuItem.Size = new System.Drawing.Size(103, 22);
           this.quitMenuItem.Text = "&Quit";
           this.quitMenuItem.Click += new System.EventHandler(this.quit_Click);
           // 
           // viewMenu
           // 
           this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
         this.viewDataMenuItem,
         this.viewXmlMenuItem});
           this.viewMenu.Name = "viewMenu";
           this.viewMenu.Size = new System.Drawing.Size(44, 20);
           this.viewMenu.Text = "View";
           // 
           // viewDataMenuItem
           // 
           this.viewDataMenuItem.Name = "viewDataMenuItem";
           this.viewDataMenuItem.Size = new System.Drawing.Size(98, 22);
           this.viewDataMenuItem.Text = "Data";
           this.viewDataMenuItem.Click += new System.EventHandler(this.viewData_Click);
           // 
           // viewXmlMenuItem
           // 
           this.viewXmlMenuItem.Name = "viewXmlMenuItem";
           this.viewXmlMenuItem.Size = new System.Drawing.Size(98, 22);
           this.viewXmlMenuItem.Text = "Xml";
           this.viewXmlMenuItem.Click += new System.EventHandler(this.viewXml_Click);
           // 
           // toolMenu
           // 
           this.toolMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
         this.reflectionMenuItem});
           this.toolMenu.Name = "toolMenu";
           this.toolMenu.Size = new System.Drawing.Size(46, 20);
           this.toolMenu.Text = "Tools";
           // 
           // reflectionMenuItem
           // 
           this.reflectionMenuItem.Name = "reflectionMenuItem";
           this.reflectionMenuItem.Size = new System.Drawing.Size(127, 22);
           this.reflectionMenuItem.Text = "Reflection";
           this.reflectionMenuItem.Click += new System.EventHandler(this.reflection_Click);
           // 
           // statusBar
           // 
           this.statusBar.Location = new System.Drawing.Point(0, 963);
           this.statusBar.Name = "statusBar";
           this.statusBar.Size = new System.Drawing.Size(1264, 22);
           this.statusBar.TabIndex = 1;
           this.statusBar.Text = "statusStrip1";
           // 
           // toolBar
           // 
           this.toolBar.Location = new System.Drawing.Point(0, 24);
           this.toolBar.Name = "toolBar";
           this.toolBar.Size = new System.Drawing.Size(1264, 25);
           this.toolBar.TabIndex = 2;
           this.toolBar.Text = "toolStrip1";
           // 
           // splitContainer1
           // 
           this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
           this.splitContainer1.Location = new System.Drawing.Point(0, 49);
           this.splitContainer1.Name = "splitContainer1";
           this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
           // 
           // splitContainer1.Panel1
           // 
           this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
           this.splitContainer1.Panel1.Controls.Add(this.palette);
           // 
           // splitContainer1.Panel2
           // 
           this.splitContainer1.Panel2.Controls.Add(this.info);
           this.splitContainer1.Size = new System.Drawing.Size(1264, 914);
           this.splitContainer1.SplitterDistance = 729;
           this.splitContainer1.TabIndex = 6;
           // 
           // splitContainer2
           // 
           this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
           this.splitContainer2.Location = new System.Drawing.Point(0, 77);
           this.splitContainer2.Name = "splitContainer2";
           // 
           // splitContainer2.Panel1
           // 
           this.splitContainer2.Panel1.Controls.Add(this.leftTabs);
           // 
           // splitContainer2.Panel2
           // 
           this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
           this.splitContainer2.Size = new System.Drawing.Size(1264, 652);
           this.splitContainer2.SplitterDistance = 208;
           this.splitContainer2.TabIndex = 5;
           // 
           // leftTabs
           // 
           this.leftTabs.Alignment = System.Windows.Forms.TabAlignment.Left;
           this.leftTabs.Controls.Add(this.treeTab);
           this.leftTabs.Controls.Add(this.structureTab);
           this.leftTabs.Dock = System.Windows.Forms.DockStyle.Fill;
           this.leftTabs.Location = new System.Drawing.Point(0, 0);
           this.leftTabs.Multiline = true;
           this.leftTabs.Name = "leftTabs";
           this.leftTabs.SelectedIndex = 0;
           this.leftTabs.Size = new System.Drawing.Size(208, 652);
           this.leftTabs.TabIndex = 6;
           // 
           // treeTab
           // 
           this.treeTab.Controls.Add(this.treeView);
           this.treeTab.Location = new System.Drawing.Point(23, 4);
           this.treeTab.Name = "treeTab";
           this.treeTab.Padding = new System.Windows.Forms.Padding(3);
           this.treeTab.Size = new System.Drawing.Size(181, 644);
           this.treeTab.TabIndex = 0;
           this.treeTab.Text = "Tree";
           this.treeTab.UseVisualStyleBackColor = true;
           // 
           // treeView
           // 
           this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
           this.treeView.Location = new System.Drawing.Point(3, 3);
           this.treeView.Name = "treeView";
           this.treeView.Size = new System.Drawing.Size(175, 638);
           this.treeView.TabIndex = 6;
           // 
           // structureTab
           // 
           this.structureTab.Controls.Add(this.structureTree);
           this.structureTab.Location = new System.Drawing.Point(23, 4);
           this.structureTab.Name = "structureTab";
           this.structureTab.Padding = new System.Windows.Forms.Padding(3);
           this.structureTab.Size = new System.Drawing.Size(181, 644);
           this.structureTab.TabIndex = 1;
           this.structureTab.Text = "Structure";
           this.structureTab.UseVisualStyleBackColor = true;
           // 
           // structureTree
           // 
           this.structureTree.Dock = System.Windows.Forms.DockStyle.Fill;
           this.structureTree.Location = new System.Drawing.Point(3, 3);
           this.structureTree.Name = "structureTree";
           this.structureTree.Size = new System.Drawing.Size(175, 638);
           this.structureTree.TabIndex = 0;
           this.structureTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.structureTree_AfterSelect);
           // 
           // splitContainer3
           // 
           this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
           this.splitContainer3.Location = new System.Drawing.Point(0, 0);
           this.splitContainer3.Name = "splitContainer3";
           // 
           // splitContainer3.Panel1
           // 
           this.splitContainer3.Panel1.Controls.Add(this.designer);
           // 
           // splitContainer3.Panel2
           // 
           this.splitContainer3.Panel2.Controls.Add(this.rightTabs);
           this.splitContainer3.Size = new System.Drawing.Size(1052, 652);
           this.splitContainer3.SplitterDistance = 833;
           this.splitContainer3.TabIndex = 0;
           // 
           // designer
           // 
           this.designer.Dock = System.Windows.Forms.DockStyle.Fill;
           this.designer.Location = new System.Drawing.Point(0, 0);
           this.designer.Name = "designer";
           this.designer.Size = new System.Drawing.Size(833, 652);
           this.designer.TabIndex = 0;
           this.designer.DragDrop += new System.Windows.Forms.DragEventHandler(this.designer_DragDrop);
           this.designer.DragEnter += new System.Windows.Forms.DragEventHandler(this.designer_DragEnter);
           // 
           // rightTabs
           // 
           this.rightTabs.Alignment = System.Windows.Forms.TabAlignment.Right;
           this.rightTabs.Controls.Add(this.propertyTab);
           this.rightTabs.Controls.Add(this.reflectionTab);
           this.rightTabs.Dock = System.Windows.Forms.DockStyle.Fill;
           this.rightTabs.Location = new System.Drawing.Point(0, 0);
           this.rightTabs.Multiline = true;
           this.rightTabs.Name = "rightTabs";
           this.rightTabs.SelectedIndex = 0;
           this.rightTabs.Size = new System.Drawing.Size(215, 652);
           this.rightTabs.TabIndex = 7;
           // 
           // propertyTab
           // 
           this.propertyTab.Controls.Add(this.propGrid);
           this.propertyTab.Location = new System.Drawing.Point(4, 4);
           this.propertyTab.Name = "propertyTab";
           this.propertyTab.Padding = new System.Windows.Forms.Padding(3);
           this.propertyTab.Size = new System.Drawing.Size(188, 644);
           this.propertyTab.TabIndex = 0;
           this.propertyTab.Text = "Properties";
           this.propertyTab.UseVisualStyleBackColor = true;
           // 
           // propGrid
           // 
           this.propGrid.Dock = System.Windows.Forms.DockStyle.Fill;
           this.propGrid.Location = new System.Drawing.Point(3, 3);
           this.propGrid.Name = "propGrid";
           this.propGrid.Size = new System.Drawing.Size(182, 638);
           this.propGrid.TabIndex = 7;
           // 
           // reflectionTab
           // 
           this.reflectionTab.Controls.Add(this.reflectionGrid);
           this.reflectionTab.Location = new System.Drawing.Point(4, 4);
           this.reflectionTab.Name = "reflectionTab";
           this.reflectionTab.Padding = new System.Windows.Forms.Padding(3);
           this.reflectionTab.Size = new System.Drawing.Size(188, 644);
           this.reflectionTab.TabIndex = 1;
           this.reflectionTab.Text = "Reflection";
           this.reflectionTab.UseVisualStyleBackColor = true;
           // 
           // palette
           // 
           this.palette.Controls.Add(this.componentPage);
           this.palette.Controls.Add(this.colorPage);
           this.palette.Dock = System.Windows.Forms.DockStyle.Top;
           this.palette.Location = new System.Drawing.Point(0, 0);
           this.palette.Name = "palette";
           this.palette.SelectedIndex = 0;
           this.palette.Size = new System.Drawing.Size(1264, 77);
           this.palette.TabIndex = 4;
           // 
           // componentPage
           // 
           this.componentPage.Location = new System.Drawing.Point(4, 22);
           this.componentPage.Name = "componentPage";
           this.componentPage.Padding = new System.Windows.Forms.Padding(3);
           this.componentPage.Size = new System.Drawing.Size(1256, 51);
           this.componentPage.TabIndex = 0;
           this.componentPage.Text = "Components";
           this.componentPage.UseVisualStyleBackColor = true;
           // 
           // colorPage
           // 
           this.colorPage.Controls.Add(this.colorToolbar);
           this.colorPage.Location = new System.Drawing.Point(4, 22);
           this.colorPage.Name = "colorPage";
           this.colorPage.Padding = new System.Windows.Forms.Padding(3);
           this.colorPage.Size = new System.Drawing.Size(1256, 51);
           this.colorPage.TabIndex = 1;
           this.colorPage.Text = "Colors";
           this.colorPage.UseVisualStyleBackColor = true;
           // 
           // colorToolbar
           // 
           this.colorToolbar.Dock = System.Windows.Forms.DockStyle.Fill;
           this.colorToolbar.Location = new System.Drawing.Point(3, 3);
           this.colorToolbar.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
           this.colorToolbar.Name = "colorToolbar";
           this.colorToolbar.Padding = new System.Windows.Forms.Padding(0);
           this.colorToolbar.Size = new System.Drawing.Size(1250, 45);
           this.colorToolbar.TabIndex = 0;
           this.colorToolbar.Text = "toolStrip1";
           // 
           // info
           // 
           this.info.Dock = System.Windows.Forms.DockStyle.Fill;
           this.info.Location = new System.Drawing.Point(0, 0);
           this.info.Multiline = true;
           this.info.Name = "info";
           this.info.Size = new System.Drawing.Size(1264, 181);
           this.info.TabIndex = 0;
           // 
           // openFileDialog
           // 
           this.openFileDialog.FileName = "openFileDialog1";
           // 
           // reflectionGrid
           // 
           this.reflectionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
           this.reflectionGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
         this.NameCol,
         this.ValueCol});
           this.reflectionGrid.Dock = System.Windows.Forms.DockStyle.Fill;
           this.reflectionGrid.Location = new System.Drawing.Point(3, 3);
           this.reflectionGrid.Name = "reflectionGrid";
           this.reflectionGrid.Size = new System.Drawing.Size(182, 638);
           this.reflectionGrid.TabIndex = 0;
           // 
           // NameCol
           // 
           this.NameCol.HeaderText = "Name";
           this.NameCol.Name = "NameCol";
           // 
           // ValueCol
           // 
           this.ValueCol.HeaderText = "Value";
           this.ValueCol.Name = "ValueCol";
           // 
           // MainWindow
           // 
           this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.ClientSize = new System.Drawing.Size(1264, 985);
           this.Controls.Add(this.splitContainer1);
           this.Controls.Add(this.toolBar);
           this.Controls.Add(this.statusBar);
           this.Controls.Add(this.mainMenu);
           this.MainMenuStrip = this.mainMenu;
           this.Name = "MainWindow";
           this.Text = "Builder";
           this.mainMenu.ResumeLayout(false);
           this.mainMenu.PerformLayout();
           this.splitContainer1.Panel1.ResumeLayout(false);
           this.splitContainer1.Panel2.ResumeLayout(false);
           this.splitContainer1.Panel2.PerformLayout();
           ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
           this.splitContainer1.ResumeLayout(false);
           this.splitContainer2.Panel1.ResumeLayout(false);
           this.splitContainer2.Panel2.ResumeLayout(false);
           ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
           this.splitContainer2.ResumeLayout(false);
           this.leftTabs.ResumeLayout(false);
           this.treeTab.ResumeLayout(false);
           this.structureTab.ResumeLayout(false);
           this.splitContainer3.Panel1.ResumeLayout(false);
           this.splitContainer3.Panel2.ResumeLayout(false);
           ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
           this.splitContainer3.ResumeLayout(false);
           this.rightTabs.ResumeLayout(false);
           this.propertyTab.ResumeLayout(false);
           this.reflectionTab.ResumeLayout(false);
           this.palette.ResumeLayout(false);
           this.colorPage.ResumeLayout(false);
           this.colorPage.PerformLayout();
           ((System.ComponentModel.ISupportInitialize)(this.reflectionGrid)).EndInit();
           this.ResumeLayout(false);
           this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.TabControl palette;
        private System.Windows.Forms.TabPage componentPage;
        private System.Windows.Forms.TabPage colorPage;
        private System.Windows.Forms.TextBox info;
        private System.Windows.Forms.Panel designer;
        private System.Windows.Forms.ToolStrip colorToolbar;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem viewDataMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewXmlMenuItem;
        private System.Windows.Forms.TabControl leftTabs;
        private System.Windows.Forms.TabPage treeTab;
        private System.Windows.Forms.TabPage structureTab;
        private System.Windows.Forms.TabControl rightTabs;
        private System.Windows.Forms.TabPage propertyTab;
        private System.Windows.Forms.TabPage reflectionTab;
        private System.Windows.Forms.ToolStripMenuItem toolMenu;
        private System.Windows.Forms.ToolStripMenuItem reflectionMenuItem;
        private System.Windows.Forms.DataGridView reflectionGrid;
        private System.Windows.Forms.TreeView structureTree;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueCol;
    }
}

